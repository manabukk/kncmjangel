# -*- coding: utf-8 -*-

# import matplotlib
# matplotlib.use('Agg')
# import matplotlib.pyplot as plt
import numpy as np
from torchvision import utils as vutils
import math

from misc.configure import CONF
FIG_SIZE = CONF.FIG_SIZE
FONT_BASE_SIZE = CONF.FONT_BASE_SIZE

def get_figm0_from_2darray(a, vmin=None, vmax=None, vlogscale=False, trans_y=False):
    import matplotlib.colors as colors
    import matplotlib.pyplot as plt
    plt.switch_backend('agg')
    plt.rcParams["font.size"] = FONT_BASE_SIZE * FIG_SIZE / 10
    from matplotlib import ticker
    sns = sns_set()
    # rdbu = plt.get_cmap('RdBu')

    assert len(a.shape) == 2

    # import seaborn as sns
    # sns.set_style("whitegrid")

    a = a.transpose(1,0)  # TODO:

    short_ax = 0 if a.shape[0] < a.shape[1] else 1
    if short_ax == 0:
        y = FIG_SIZE
        x = y / a.shape[1] * a.shape[0]
        if x < FIG_SIZE / 10:
            x = FIG_SIZE / 2
    else:
        x = FIG_SIZE
        y = x / a.shape[0] * a.shape[1]
        if y < FIG_SIZE / 10:
            y = FIG_SIZE / 2
        
    fig = plt.figure(figsize=(x, y))
    ax = plt.gca(aspect=1)

    if vlogscale:
        # a[a > 0] = np.log10(a[a > 0])
        # a[a < 0] = -1 * np.log10(-1 * a[a < 0])
        # if vmin is None:
        #     max_v = np.absolute(a).max()
        #     vmin = -1 * max_v
        #     vmax = max_v
        
        # heatmap = ax.pcolor(a.transpose(1, 0), cmap=plt.get_cmap(
        #     'RdBu'), vmin=vmin, vmax=vmax)
        if vmin is None:
            max_v = np.absolute(a).max()
            vmin = -1 * max_v
            vmax = max_v
        heatmap = ax.pcolor(array2DToRdBr3D(a.transpose(1,0)), cmap=plt.get_cmap('RdBu'), vmin=vmin, vmax=vmax)
    else:
        if vmin is None:
            max_v = np.absolute(a).max()
            vmin = -1 * max_v
            vmax = max_v
        heatmap = ax.pcolor(a.transpose(1,0), cmap=plt.get_cmap('RdBu'), vmin=vmin, vmax=vmax)
        # sns.heatmap(a.transpose(1, 0), cmap=plt.get_cmap('RdBu'), vmin=vmin, vmax=vmax)
    import mpl_toolkits.axes_grid1
    divider = mpl_toolkits.axes_grid1.make_axes_locatable(ax)
    cax = divider.append_axes('right', '5%', pad='3%')
    fig.colorbar(heatmap, cax=cax)

    ax.set_xlim(0, a.shape[0])
    if trans_y:
        ax.set_ylim(a.shape[1], 0)
    else:
        ax.set_ylim(0, a.shape[1])
    
    # ax.xaxis.set_major_locator(ticker.MaxNLocator(min(int(x * 2), a.shape[0])))
    # ax.yaxis.set_major_locator(ticker.MaxNLocator(min(int(y * 2), a.shape[1])))

    # ax.set_xticks(np.arange(a.shape[0]) + 0.5, minor=False)
    # ax.set_yticks(np.arange(a.shape[1]) + 0.5, minor=False)

    # row_max_num = min(int(x * 2), a.shape[0])
    # column_max_num = min(int(y * 2), a.shape[1])
    # row_labels = [str(i) if i % math.ceil(a.shape[0] / row_max_num) == 0 else '' for i in range(a.shape[0])]
    # column_labels = [str(i) if i % math.ceil(
    #     a.shape[0] / column_max_num) == 0 else '' for i in range(a.shape[1])]

    # ax.set_xticklabels(row_labels, minor=False)
    # ax.set_yticklabels(column_labels, minor=False)

    # row_max_num = min(
    #     int(x * 2), a.shape[0]) if a.shape[0] > 10 else a.shape[0]
    # row_max_num = 
    # column_max_num = min(
    #     int(y * 2), a.shape[1]) if a.shape[1] > 10 else a.shape[1]
    # column_max_num = 4

    # row_labels = [i for i in range(a.shape[0]) if i %
    #               math.ceil(a.shape[0] / row_max_num) == 0]
    # column_labels = [i for i in range(a.shape[1]) if i % math.ceil(
    #     a.shape[0] / column_max_num) == 0]
    # row_labels = [0, 30, 60, 90, 120, 150, 180, 210, 240, 269]
    # column_labels = [0, 1, 2, 3]
    row_labels = get_labels(a.shape[0])
    column_labels = get_labels(a.shape[1])

    ax.set_xticks(np.array(row_labels) + 0.5, minor=False)
    ax.set_yticks(np.array(column_labels) + 0.5, minor=False)

    ax.set_xticklabels([str(l) for l in row_labels], minor=False)
    ax.set_yticklabels([str(l) for l in column_labels], minor=False)

    return fig


def get_figb0_from_2darray(a, vmin=None, vmax=None, vlogscale=False):
    import matplotlib.colors as colors
    import matplotlib.pyplot as plt
    plt.switch_backend('agg')
    plt.rcParams["font.size"] = FONT_BASE_SIZE * FIG_SIZE / 10
    from matplotlib import ticker
    sns_set()
    # rdbu = plt.get_cmap('RdBu')

    assert len(a.shape) == 2

    a = a.transpose(1,0)  # TODO:

    short_ax = 0 if a.shape[0] < a.shape[1] else 1
    if short_ax == 0:
        y = FIG_SIZE
        x = y / a.shape[1] * a.shape[0]
        if x < FIG_SIZE / 10:
            x = FIG_SIZE / 2
    else:
        x = FIG_SIZE
        y = x / a.shape[0] * a.shape[1]
        if y < FIG_SIZE / 10:
            y = FIG_SIZE / 2
            
    fig = plt.figure(figsize=(x, y))
    ax = plt.gca(aspect=1)

    if vlogscale:
        # a[a > 0] = np.log10(a[a > 0])
        # a[a < 0] = -1 * np.log10(-1 * a[a < 0])
        a = np.log10(a - a.min() + 0.00001)
        if vmin is None:
            vmin = a.min()
            vmax = a.max()

        heatmap = ax.pcolor(a.transpose(1,0), cmap=plt.get_cmap(
            'Blues'), vmin=vmin, vmax=vmax)
    else:
        if vmin is None:
            vmin = a.min()
            vmax = a.max()
        heatmap = ax.pcolor(a.transpose(1,0), cmap=plt.get_cmap(
            'Blues'), vmin=vmin, vmax=vmax)
    fig.colorbar(heatmap)

    ax.set_xlim(0, a.shape[0])
    ax.set_ylim(0, a.shape[1])
    
    # ax.xaxis.set_major_locator(ticker.MaxNLocator(min(int(x * 2), a.shape[0])))
    # ax.yaxis.set_major_locator(ticker.MaxNLocator(min(int(y * 2), a.shape[1])))

    # row_max_num = min(int(x * 2), a.shape[0]) if a.shape[0] > 10 else a.shape[0]
    # column_max_num = min(
        # int(y * 2), a.shape[1]) if a.shape[1] > 10 else a.shape[1]

    # row_labels = [i for i in range(a.shape[0]) if i % math.ceil(a.shape[0] / row_max_num) == 0]
    # column_labels = [i for i in range(a.shape[1]) if i % math.ceil(
        # a.shape[0] / column_max_num) == 0]

    # row_labels = [0, 30, 60, 90, 120, 150, 180, 210, 240, 269]
    # column_labels = [0, 1, 2, 3]
    row_labels = get_labels(a.shape[0])
    column_labels = get_labels(a.shape[1])

    ax.set_xticks(np.array(row_labels) + 0.5, minor=False)
    ax.set_yticks(np.array(column_labels) + 0.5, minor=False)

    ax.set_xticklabels([str(l) for l in row_labels], minor=False)
    ax.set_yticklabels([str(l) for l in column_labels], minor=False)

    return fig
    
# def get_imgm0_from_2darray(a, vmin=None, vmax=None):
#     assert len(a.shape) == 2

#     # import seaborn as sns
#     # sns.set_style("whitegrid")

    

#     if vmin is None:
#         max_v = np.absolute(a).max()
#         vmin = -1 * max_v
#         vmax = max_v

#     heatmap = plt.pcolor(a.transpose(1, 0), cmap=plt.get_cmap(
#         'RdBu'), vmin=vmin, vmax=vmax)
#     fig.colorbar(heatmap)

#     plt.xlim(0, a.shape[0])
#     plt.ylim(0, a.shape[1])

#     # plt.draw()
#     return fig


def array2DToRdBr3D(a, vmin=None, vmax=None):
    # rdbu = plt.get_cmap('RdBu')

    # a_numpy = a.numpy()
    # print(a.shape)
    normalized_a = (a - vmin) / vmax
    # print(normalized_a.shape)
    normalized_a[normalized_a > 1.0] = 1.0
    normalized_a[normalized_a < 0] = 0.0
    # print(normalized_a)

    lt_indexs = normalized_a < 0.5
    gt_indexs = normalized_a >= 0.

    a_red = normalized_a.copy()
    
    # print(lt_indexs)
    a_red[lt_indexs] = 255
    a_red[gt_indexs] = (1.0 - a_red[gt_indexs]) * 255. * 2
    # print(a_red)

    # print(a_red.shape)
    a_green = normalized_a.copy()
    a_green[lt_indexs] *=  255. * 2
    a_green[gt_indexs] = (1.0 - a_green[gt_indexs]) * 255. * 2
    # print(a_green.shape)
    a_blue = normalized_a.copy()
    a_blue[lt_indexs] *= 255. * 2
    a_blue[gt_indexs] = 255.
    # print(a_blue.shape)
    # print(a_red)
    # print(a_green)
    # print(a_blue)

    a_rdbn = np.asarray([a_red, a_green, a_blue])
    # print(a_rdbn.shape)
    return a_rdbn / 255.

def get_labels(x_length: int):
    # 1 or 2 or 5
    order = int(math.log10(x_length))
    interval = 2
    tmp = x_length / (interval * 10**(order-1)) 
    if tmp < 5:
        interval = 1
    elif tmp > 15:
        interval = 5

    print()
    row_labels = [0] + list(range(int(interval * 10**(order - 1)),
                                  x_length - 1, interval * 10**(order - 1) if interval * 10**(order - 1) > 1 else 1)) + [x_length - 1]
    if row_labels[-1] - row_labels[-2] < interval * 10**(order - 1) / 2:
        row_labels.pop(-2)

    return row_labels

def sns_set():
    import seaborn as sns
    sns.set_style("whitegrid")
    # sns.set()
    return sns


def fseq2fig(fseq, save_filepath=None, x_label=None, y_label=None, x_min=None, x_max=None, y_min=None, y_max=None, x_scale=1.0):
    assert len(fseq.shape) is 2, 'ERROR'

    import matplotlib.colors as colors
    import matplotlib.pyplot as plt
    plt.switch_backend('agg')
    plt.rcParams["font.size"] = FONT_BASE_SIZE * FIG_SIZE / 10
    from matplotlib import ticker
    sns_set()

    sequences = np.asarray(fseq).transpose()
    vec_size = sequences.shape[0]
    seq_size = sequences.shape[1]

    fig = plt.figure(figsize=(15*x_scale, vec_size * 5))
    for dim, seq in enumerate(sequences):
        plt.subplot(vec_size, 1, dim + 1)
        plt.plot(seq)

    plt.xlim(x_min if x_min is not None else 0,
             x_max if x_max is not None else len(sequences[0]))
    plt.ylim(y_min, y_max)

    if x_label is not None:
        plt.xlabel(x_label)
    if y_label is not None:
        plt.ylabel(y_label)

    return fig
