import torch
import numpy as np
from data.dataset.dataset import load_dataset


def get_window_matrix(delta_num, sequence_length, dim=1):  # TODO:
    if delta_num == 0:
        window_matrix = np.eye(dim*sequence_length)

    elif delta_num == 1:
        # sub_window_static = np.eye(dim)
        # sub_window_delta1 = np.eye(dim) * 0.5
        # sub_window_delta2 = - np.eye(dim) * 0.5


        # _window_matrix = np.zeros(
        #     shape=[dim*sequence_length, (delta_num+1)*dim*sequence_length])
        # for t in range(sequence_length):
        #     _window_matrix[t*dim:t*dim+dim,
        #                    (delta_num+1)*t*dim:(delta_num+1)*t*dim + dim] = sub_window_static
        # for t in range(1, sequence_length):
        #     _window_matrix[t*dim:t*dim+dim,
        #                    (delta_num+1)*t*dim - dim:(delta_num+1)*t*dim] = sub_window_delta1
        # for t in range(2, sequence_length+1):
        #     _window_matrix[t*dim - (delta_num+1) * dim:t*dim+dim - (delta_num+1) * dim,
        #                    (delta_num+1)*t*dim - dim:(delta_num+1)*t*dim] = sub_window_delta2
        # window_matrix = _window_matrix.T
        sub_window_static = 1.0
        sub_window_delta1 = 0.5
        sub_window_delta2 = -0.5
        _window_matrix = np.zeros(shape=[sequence_length, (delta_num+1)*sequence_length])
        _window_matrix[[t for t in range(sequence_length)], [(
            delta_num+1)*t for t in range(sequence_length)]] = sub_window_static
        _window_matrix[[t for t in range(1, sequence_length)], [(
            delta_num+1)*t - 1 for t in range(1, sequence_length)]] = sub_window_delta1
        _window_matrix[[t for t in range(sequence_length-1)], [(delta_num+1)*(
            t+2) - 1 for t in range(sequence_length-1)]] = sub_window_delta2
        window_matrix = _window_matrix.T

    else:
        raise NotImplementedError()

    return window_matrix

def static2staticdelta(x, delta_num=1, W=None):

    device = x.device

    dim = x.size(1)
    seq_len = x.size(0)

    if W is None:
        W = torch.from_numpy(get_window_matrix(delta_num=delta_num, sequence_length=seq_len))
    W = W.to(device)

    X = []
    for d in range(dim):
        mini_x = x[:, d].squeeze()
        _X = torch.matmul(W, mini_x)
        _X = torch.reshape(_X, (seq_len, delta_num+1))
        X.append(_X)

    X = torch.cat(X, dim=1)
    X = torch.cat([X[:, [i for i in range(j, dim*(delta_num+1), (delta_num+1))]] for j in range(delta_num + 1)], dim=1)

    return X

def staticdelta2static(X, variance_for_trainset, delta_num=1, W=None):
    device = X.device

    x = []
    dim = X.size(1) // (delta_num+1)
    seq_len = X.size(0)

    if W is None:
        W = torch.from_numpy(get_window_matrix(
            delta_num=delta_num, sequence_length=seq_len))
    W = W.to_sparse().to(device)

    for d in range(dim):
        mini_X = X[:, [d + dim*i for i in range(delta_num+1)]]
        mini_X_flatten = mini_X.flatten().unsqueeze(1)
        _mini_Ui_flatten = (1 / variance_for_trainset[[d + dim*j for j in range(delta_num + 1)]]).to(device)
        mini_Ui_flatten = _mini_Ui_flatten.expand(seq_len, -1).flatten()
        mini_Ui = torch.diag_embed(mini_Ui_flatten)
        right_prod = torch.matmul(mini_Ui, mini_X_flatten)
        right_prod = torch.sparse.mm(W.transpose(1, 0), right_prod)
        left_prod = torch.sparse.mm(W.transpose(1, 0), mini_Ui)
        left_prod = torch.sparse.mm(W.transpose(1,0), left_prod.transpose(1,0)).transpose(1,0)
        left_prod = torch.inverse(left_prod)
        prod = torch.matmul(left_prod, right_prod)
        x.append(prod)

    x = torch.cat(x, dim=1)

    return x


def cmpt_var_Y(train_dataset_path, delta_num=1):

    train_dataset = load_dataset(train_dataset_path)
    
    global_Y = None
    for idx in range(len(train_dataset)):
        utter = static2staticdelta(torch.from_numpy(train_dataset[idx]['y']), delta_num=delta_num)

        if global_Y is None:
            global_Y = utter
        else:
            global_Y = torch.cat([global_Y, utter], dim=0)

    var_Y = torch.var(global_Y, dim=0)

    # return torch.from_numpy(var_Y)
    return var_Y
