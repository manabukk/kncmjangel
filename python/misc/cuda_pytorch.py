# -*- coding: utf-8 -*-

import torch
from torch import nn
# import multiprocessing

class CUDATorch(object):
    """docstring for CUDATorch."""
    def __init__(self):
        super(CUDATorch, self).__init__()
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.device = device
        self.device_counts = torch.cuda.device_count()

        # print("num threads: {}".format(torch.get_num_threads()))
        # print("CPU count: {}".format(multiprocessing.cpu_count()))
        # torch.set_num_threads(multiprocessing.cpu_count() - 2)
        # print("num threads: {}".format(torch.get_num_threads()))

        if self.device_counts > 1:
            print("We can use", self.device_counts, "GPUs!")
            # dim = 0 [30, xxx] -> [10, ...], [10, ...], [10, ...] on 3 GPUs

    def parallel_model(self, model):
        if self.device_counts > 1:
            print("Let's use", self.device_counts, "GPUs!")
            # dim = 0 [30, xxx] -> [10, ...], [10, ...], [10, ...] on 3 GPUs
            model = nn.DataParallel(model)

        model.to(self.device)

        return model

    def to(self, data):
        data.to(self.device)
        
