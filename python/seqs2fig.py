# -*- coding=utf-8 -*-
"""Overview:
    Plotting sequences (e.g. waveform, F0 con., or mcep's trajectories).

Usage:
    seqs2fig <input_filepaths> [-o <save_filepath>] [-d <vector_size>] [-f <feature_indices>] [-l <legends>] [-x <x_label>] [-y <y_label>] [-s <x_scale>] [--logx] [-v...]
    seqs2fig -h

Options:
    <input_filepaths>                       Must be comma separated.
    -d --vector-size <vector_size>          Vector size [default: 25].
    -f --feature-indices <feature_indices>  Feature indices [default: 1,2,4,8,16,24].
    -l --legends <legends>                  Must be comma separated.
    -x --x-label <x_label>                  X-axis label. Commas are converted to spaces.
    -y --y-label <y_label>                  Y-axis label. Commas are converted to spaces.
    -s --x-scale <x_scale>                  Hoge [default: 1.0].
    -o --save-filepath <save_filepath>      Path to the output file.
    --logx                                  Hoge.
    -v --verbose                            Show the process in detail.
    -h --help                               Show this screen and exit.
"""

from misc.readwrite import load_fseq
import numpy as np

from docopt import docopt

from misc.util import set_level, logger

import os

def fseqs2fig(fseqs, save_filepath=None, x_label=None, y_label=None, x_min=None, x_max=None, y_min=None, y_max=None, x_scale=1.0, logx=False, subplot_labels=None, legends=None):
    assert len(fseqs.shape) == 3, 'ERROR'
    sequences = np.asarray(fseqs).transpose(2,0,1)
    vec_size = sequences.shape[0]
    seq_size = sequences.shape[2]

    import matplotlib
    if save_filepath is not None:
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import seaborn as sns
    sns.set_style("whitegrid")

    assert (legends is None) or (len(legends) == len(fseqs))

    # x = np.linspace(0, 100, 1025)

    fig = plt.figure(figsize=(15*x_scale, vec_size * 5))
    for dim, seqs in enumerate(sequences):
        plt.subplot(vec_size, 1, dim + 1)
        if logx:
            plt.xscale('log')

        for seq, legend in zip(seqs, legends):
            # plt.plot(x, seq, label=legend)
            plt.plot(seq, label=legend)
        if subplot_labels is None:
            plt.title("dim {}".format(dim))
        else:
            plt.title("dim {}".format(subplot_labels[dim]))


        plt.xlim(x_min if x_min is not None else 0,
                x_max if x_max is not None else seqs.shape[1])
        # plt.xlim(0,100)
        plt.ylim(y_min, y_max)
        plt.legend()

        if x_label is not None:
            plt.xlabel(x_label)
        if y_label is not None:
            plt.ylabel(y_label)


    plt.draw()
    if save_filepath:
        fig.savefig(save_filepath)
    else:
        plt.show()


def main(input_filepaths, vector_size, feature_indices, legends, save_filepath, x_label, y_label, verbose, logx=False, y_min=None, y_max=None, x_min=None, x_max=None, x_scale=1.0, *, logger=None):
    set_level(verbose)
    fseqs = [load_fseq(path=filepath, vec_size=vector_size, feature_range=feature_indices) for filepath in input_filepaths]
    fseqs2fig(fseqs=np.asarray(fseqs),
             x_label=" ".join(x_label) if x_label is not None else None,
             y_label=" ".join(y_label) if y_label is not None else None,
             save_filepath=save_filepath,
             y_min=y_min,
             y_max=y_max,
             x_min=x_min,
             x_max=x_max,
             x_scale=x_scale,
             subplot_labels=feature_indices,
             logx=logx,
             legends=legends)


if __name__ == "__main__":
    args = docopt(__doc__)

    main(input_filepaths=[os.path.abspath(os.path.expanduser(filepath)) for filepath in args['<input_filepaths>'].split(',')],
         x_label=args['--x-label'].split(
             ',') if args['--x-label'] is not None else None,
         y_label=args['--y-label'].split(
             ',') if args['--y-label'] is not None else None,
         legends=args['--legends'].split(
             ',') if args['--legends'] is not None else None,
         save_filepath=os.path.abspath(
             args['--save-filepath']) if args['--save-filepath'] is not None else None,
         vector_size=int(args['--vector-size']),
         feature_indices=[int(f) for f in args['--feature-indices'].split(',')],
         x_scale=float(args['--x-scale']),
         logx=args['--logx'],
         verbose=int(args['--verbose']))
