# -*- coding: utf-8 -*-
"""Overview:
    Calculating power.

Usage:
    power <raw_file> <output_file> [-l <window_length>] [-e <epsilon>] [-p] [-v...]
    power -h

Options:
    <raw_file>                                Path to raw file.
    <output_file>                             Path to output file.
    -l --window-length <window_length>        Window length (nb points) [default: 80].
    -e --epsilon <epsilon>                    Value of epsilon [default: 0.0000001].
    -p --padding                              Flag of using 0 padding.
    -v --verbose                              Show in detail.
    -h --help                                 Show this message.
"""

from docopt import docopt
import sys
import os
import numpy as np
from misc.readwrite import load_fseq, write_vector

from misc.util import set_level, logger

def main(raw_file, output_file, window_length, epsilon, padding, verbose, **kargs):
    set_level(verbose)

    data = load_fseq(path=raw_file, vec_size=1, feature_range=range(1))

    flatten = data.reshape((-1))

    flatten = np.concatenate([flatten, np.zeros(shape=[window_length])], axis=0)
    _sum = flatten.copy()[None,:]
    for i in range(window_length):
        # print(flatten.shape)
        _shifted = np.roll(flatten, i - int((window_length - 1) / 2), axis=0)
        _sum = np.concatenate([_sum, _shifted[None,:]], axis=0)
        # print(_sum.shape)

    # print(_sum[0,:10])
    # print(_sum[:10,0])
    _sum = (_sum**2).sum(axis=0)
    # epsilon = 0.00001
    power = np.log10(_sum + epsilon)
    if not padding:
        power = power[int(window_length / 2):- 1 * int((window_length - 1) / 2)]
        assert power.shape[0] == data.shape[0] + 1

    write_vector(path=output_file, b=power)


if __name__ == "__main__":
    args = docopt(__doc__)

    main(raw_file=os.path.abspath(args['<raw_file>']),
         output_file=os.path.abspath(args['<output_file>']),
         window_length=int(args['--window-length']),
         epsilon=float(args['--epsilon']),
         padding=args['--padding'],
         verbose=int(args['--verbose']))
