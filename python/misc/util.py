# -*- coding: utf-8 -*-
""" Docstring """
import errno
import os, sys

from logging import getLogger, StreamHandler, DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = getLogger(__name__)
handler = StreamHandler()
handler.setLevel(DEBUG)
logger.setLevel(DEBUG)
logger.addHandler(handler)
logger.propagate = False

import subprocess

from misc.configure import CONF


def set_level(v):
    level_list = [DEBUG, INFO, WARNING, ERROR, CRITICAL]
    if v >= len(level_list):
        v = len(level_list) - 1
    level = level_list[::-1][v]
    handler.setLevel(level)
    logger.setLevel(level)
    return level


def command(query, *, logger=None):
    logger.debug(query)
    res = subprocess.getoutput(query)
    logger.debug(res)
    return res


def mkdir_p(path):
    """
    docstring here
        :param path:
    """
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def dynamic_import(module_class_str):
    """
    docstring here
        :param module_class_str:
    """
    module_name = ".".join(module_class_str.split('.')[:-1])
    class_name = module_class_str.split('.')[-1]

    try:
        module = __import__(module_name, fromlist=[class_name])
        return getattr(module, class_name)
    except Exception as e:
        print(e)
        raise e


def get_class_path(obj):
    c = str(obj.__class__)
    return c[c.index("'") + 1:c.rindex("'")]

# 改行周り
class MyPrinting(object):
    print_count = 0

    @staticmethod
    def print(string):
        print(string)
        sys.stdout.flush()

        MyPrinting.print_count += 1

    @staticmethod
    def debug(string):
        logger.debug(string)
        sys.stdout.flush()

        if logger.level >= 10:
            MyPrinting.print_count += 1

    @staticmethod
    def clean(nb_lines=None):
        if nb_lines is None:
            count = MyPrinting.print_count
        else:
            assert isinstance(nb_lines, int)
            count = nb_lines

        for _ in range(count):
            sys.stdout.write("\033[1A\033[2K\033[G")
            sys.stdout.flush()
        MyPrinting.print_count = 0

    @staticmethod
    def reset():
        print_count = MyPrinting.print_count
        MyPrinting.print_count = 0
        return print_count

    @staticmethod
    def print_at(string: str, line: int):
        for _ in range(MyPrinting.print_count - line):
            sys.stdout.write("\033[1A")
            sys.stdout.flush()
        # sys.stdout.write("\033[1A\033[2K\033[G")
        sys.stdout.write("\033[2K\033[G")
        sys.stdout.flush()

        print(string)
        sys.stdout.flush()
        MyPrinting.print_count = line + 1

def rm(file_path, temp_only=True):
    if file_path[:len(CONF.TEMP_DIR)] != CONF.TEMP_DIR and temp_only:
        print("File: {} is not in temp dir: {}.".format(file_path, CONF.TEMP_DIR))
    else:
        os.remove(file_path)


def ms2frame(ms, fs):  # frame shift [ms]
    return int(ms / fs)


def frame2ms(frame, fs):  # frame shift [ms]
    return float(frame * fs)
