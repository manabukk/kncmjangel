# -*- coding: utf-8 -*-
"""Overview:
    Adding c0.

Usage:
    addpower <filepath_A> <output_filepath> [-B <filepath_B>] [-a <dimension_A>] [-b <dimension_B>] [-v...]
    addpower -h 

Options:
    <filepath_A>                    hoge.
    -B --filepath-B <filepath_B>    hoge.
    -a --dimension-A <dimension_A>  hoge [default: 24].
    -b --dimension-B <dimension_B>  hoge [default: 25].
    -v --verbose                    Show the process in detail.
    -h --help                       Show this message.
"""

from docopt import docopt
import os
import numpy as np

from misc.util import set_level, logger
from misc.readwrite import load_fseq, write_fseq


def main(filepath_A, output_filepath, filepath_B, dimension_A, dimension_B, verbose, **kargs):
    set_level(verbose)
    A = load_fseq(path=filepath_A, vec_size=dimension_A, feature_range=range(dimension_A))
    logger.debug(A.shape)
    
    if filepath_B is not None:
        B = load_fseq(path=filepath_B, vec_size=dimension_B,
                      feature_range=range(dimension_B))
        logger.debug(B.shape)

        C = np.concatenate([B[:, 0][:,None], A], axis=1)
    else:
        
        C = np.concatenate([np.zeros([A.shape[0], 1]), A], axis=1)
    logger.debug(C.shape)

    write_fseq(path=output_filepath, fseq=C)


if __name__ == "__main__":
    args = docopt(__doc__)

    main(filepath_A=os.path.abspath(args['<filepath_A>']),
         output_filepath=os.path.abspath(args['<output_filepath>']),
         filepath_B=os.path.abspath(args['--filepath-B']) if args['--filepath-B'] is not None else None,
         dimension_A=int(args['--dimension-A']),
         dimension_B=int(args['--dimension-B']),
         verbose=int(args['--verbose']))
