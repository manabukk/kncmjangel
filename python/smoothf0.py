# -*- coding=utf-8 -*-
"""Overview:
    Smoothing F0 con..

Usage:
    smoothf0 <input_f0_fp> <output_f0_fp> [-t <thresh>] [-n <n_frames>] [-v...]
    smoothf0 -h

Options:
    <input_f0_fp>                        Path.
    <output_f0_fp>                       Path.
    -t --threshold <thresh>              Value of threshold (e.g. 16kHz&5ms -> 0.5) [default: 0.5]
    -n --nframes <n_frames>              Nb frame as micro prosody (e.g. 16kHz&5ms -> 4) [default: 4]
    -v --verbose                         Show in detail.
    -h --help                            Show this message.
"""
from docopt import docopt
import os
import sys
sys.path.append(os.path.curdir)
from misc.readwrite import load_fseq, write_fseq

def main(input_f0_fp, output_f0_fp, threshold, n_frames, verbose):

    f0 = load_fseq(path=input_f0_fp, vec_size=1, feature_range=[0])

    import numpy as np
    from scipy import signal
    import copy
    from scipy.interpolate import interp1d

    HTS_U_SYMBOL = -1e+10

    def rm_micro_prosody(clf0, n_frame=4):
        if len(np.where(clf0 == HTS_U_SYMBOL)[0]) != 0:
            raise ValueError("unvoiced frames are included.")

        smooth_clf0 = np.zeros(len(clf0))
        for t in range(len(clf0)):
            smooth_clf0[t] = np.average(clf0[ max(t-n_frame, 0) : min(t+n_frame+1, len(clf0) - 1) ])

        return smooth_clf0

    def lf02clf0(lf0, smooth=False):
        clf0 = np.zeros(len(lf0))
        t = np.arange(len(lf0))
        t_voiced = t[np.where(lf0 != HTS_U_SYMBOL)]
        lf0_voiced = lf0[t_voiced]  
    
        # uv
        uv = np.zeros(len(lf0))
        uv[t_voiced] = 1
    
        # smoothing & interpolation
        contlf0 = interp1d(t_voiced, lf0_voiced)   # lienar interpolation
        clf0[t_voiced[0]:t_voiced[-1]+1] = contlf0(np.arange(t_voiced[0], t_voiced[-1]+1))
        clf0[t_voiced[0]:t_voiced[-1]+1] = rm_micro_prosody(clf0[t_voiced[0]:t_voiced[-1]+1], n_frames)

        # smoothing & extrapolation
        clf0[:t_voiced[0]] = lf0[t_voiced[0]]
        clf0[t_voiced[-1]+1:] = lf0[t_voiced[-1]]
        clf0 = rm_micro_prosody(clf0, n_frames)

        # restore voiced frames
        clf0[t_voiced] = lf0[t_voiced]

        # smoothing (trajectory smoothing)
        if smooth is True:
            clf0 = rm_micro_prosody(clf0, n_frames)

        return clf0, uv

    def clf02lf0(clf0, uv):
        lf0 = np.ones(len(clf0)) * HTS_U_SYMBOL
        lf0[np.where(uv == 1)] = clf0[np.where(uv == 1)]

        return lf0

    def refine_uv(uv):
        for t in range(1, len(uv) - 1):
            if (uv[t-1] == uv[t+1] == 0) and (uv[t] == 1):
                uv[t] = 0
            if (uv[t-1] == uv[t+1] == 1) and (uv[t] == 0):
                uv[t] = 1
        return uv
            
    def trajectory_smoothing(x, thresh=0.5):
        y = copy.copy(x)

        b, a = signal.butter(2, thresh)
        # for d in range(y.shape[1]):
        #     y[:, d] = signal.filtfilt(b, a, y[:, d])
        #     y[:, d] = signal.filtfilt(b, a, y[::-1, d])[::-1]
        y[::] = signal.filtfilt(b, a, y[::])
        y[::] = signal.filtfilt(b, a, y[::-1])[::-1]

        return y


    lf0 = np.asarray([-1e10 if _f0 < 0.1 else np.log2(_f0) for _f0 in f0[:, 0]])
    # lf0 = np.zeros(shape=f0.shape)
    # lf0[f0 < 0.1] = -1e10
    # lf0[f0 >= 0.1] = np.log2(f0[f0>= 0.1])
    # lf0 = [-1e10, -1e10, ..., 4.5, 4.6, ..., ...-1e10] # 無声区間に-1e10の入った対数F0

    clf0, uv = lf02clf0(lf0)
    clf0 = trajectory_smoothing(clf0, threshold)
    lf0 = clf02lf0(clf0, uv)
    new_f0 = np.zeros(shape=f0.shape)
    for i, _lf0 in enumerate(lf0):
        new_f0[i][0] = 0.0 if _lf0 <= -1e10 else 2**_lf0 
    # new_f0[np.where(lf0 > -1e10)] = 2**lf0[np.where(lf0 > -1e10)]

    write_fseq(path=output_f0_fp, fseq=new_f0)

if __name__ == "__main__":
    args = docopt(__doc__)
    main(input_f0_fp=os.path.abspath(args['<input_f0_fp>']),
         output_f0_fp=args['<output_f0_fp>'],
         threshold=float(args['--threshold']),
         n_frames=int(args['--nframes']),
         verbose=int(args['--verbose']))
