""" Docstring """
import numpy as np


def compute_melcd(pdata):
    """
    docstring here
        :param pdata:
    """
    pX = pdata[0]
    pY = pdata[1]
    mel_cd = np.mean(10 / np.log(10) * np.sqrt(2 *
                                               np.sum(np.square(pX - pY), axis=1)), axis=0)
    return mel_cd


def compute_mse(pdata):
    """
    docstring here
        :param pdata:
    """
    pX = pdata[0]
    pY = pdata[1]
    # mse = np.mean(np.sqrt(np.sum(np.square(pX - pY), axis=1)), axis=0)
    mse = np.mean(np.sum(np.square(pX - pY), axis=1), axis=0)
    return mse


def cmcd_by_frame(pdata):
    """
    docstring here
        :param pdata:
    """
    pX = pdata[0]
    pY = pdata[1]
    mel_cd = 10 / np.log(10) * np.sqrt(2 * np.sum(np.square(pX - pY), axis=1))
    return mel_cd


def cmse_by_frame(pdata):
    pX = pdata[0]
    pY = pdata[1]
    mse = np.sum(np.square(pX - pY), axis=1)
    return mse


def anomaly_score(pdata):
    pX = pdata[0]
    pY = pdata[1]
    mse = np.mean(np.square(pX - pY), axis=1)
    return mse

def compute_daligns(align1, align2):  # alignment length は除いたものを与える
    # area = 0.0
    # for x in range(align1[-2]+1):
    #     assert align1.index(x) >= 0 and align2.index(x) >= 0
    #     area += abs(align1[align1.index(x) + 1] - align2[align2.index(x)+1])

    # return area
    area = 0.0
    align1_x = align1[::2]
    align1_y = align1[1::2]
    assert len(align1_x) == len(align1_y)
    align2_x = align2[::2]
    align2_y = align2[1::2]
    assert len(align2_x) == len(align2_y)

    for x in range(align1[-2]+1):
        assert align1.index(x) >= 0 and align2.index(x) >= 0
        align1_x_i = align1_x.index(x)
        align1_y_x = align1_y[align1_x_i]
        align1_x_ri = align1_x[::-1].index(x)
        align1_y_rx = align1_y[::-1][align1_x_ri]

        align2_x_i = align2_x.index(x)
        align2_y_x = align2_y[align2_x_i]
        align2_x_ri = align2_x[::-1].index(x)
        align2_y_rx = align2_y[::-1][align2_x_ri]

        distances = [abs(a1 - a2) for a1 in [align1_y_x, align1_y_rx]
                     for a2 in [align2_y_x, align2_y_rx]]

        area += max(distances)

    return area


def compute_daligns_local(align1, align2):  # alignment length は除いたものを与える
    d_list = []
    for x in range(align1[-2]+1):
        assert align1.index(x) >= 0 and align2.index(x) >= 0
        d_list.append(abs(align1[align1.index(x) + 1] - align2[align2.index(x)+1]))

    return max(d_list)
