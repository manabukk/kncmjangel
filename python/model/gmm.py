import torch
import torch.nn.functional as F
import numpy as np

import sys
from sklearn.cluster import KMeans


def inverse_cross_cov(cov):
    A11 = cov[:, 0]  # (K, features / 2)
    A21 = cov[:, 1]
    # A12 = self.cov[:, 2]
    A12 = A21
    A22 = cov[:, 2]

    half_d = A11.size(1)
    print("half d: {}".format(half_d))

    A11_inv = 1. / A11  # (K, features / 2)
    A22_inv = 1. / A22
    C1 = A11 - A12 * A22_inv * A21  # (K, features / 2)
    C2 = A22 - A21 * A11_inv * A12
    C1_inv = 1. / C1  # (K, features / 2)
    C2_inv = 1. / C2
    covar_inv = torch.diag_embed(torch.cat([C1_inv, C2_inv], dim=1))
    covar_inv += torch.diag_embed(- C2_inv *
                                  A21 * A11_inv, offset=-half_d)
    covar_inv += torch.diag_embed(- A11_inv *
                                  A12 * C2_inv, offset=half_d)
    return covar_inv


def convert2marginal(gmm):
    assert gmm.covtype == 'full'
    d = int(gmm.d/2)
    x_gmm = GMM(gmm.k, d, gmm.covtype)
    x_gmm.mu = gmm.mu[:, :d]
    x_gmm.cov = gmm.cov[:, :d, :d]
    return x_gmm


class GMM(object):
    def __init__(self, k, d, covtype='diag', eps=1e-6, *args):
        super(GMM, self).__init__(*args)
        self.covtype = covtype
        self.eps = eps
        self.mu = torch.from_numpy(
            np.random.rand(k, d)).type(dtype=torch.float64)
        if covtype == 'diag':
            self.cov = torch.Tensor(k, d).fill_(1.).type(dtype=torch.float64)
        elif covtype == 'cross':
            assert d % 2 == 0
            self.cov = torch.Tensor(
                k, 3, int(d/2)).fill_(0.).type(dtype=torch.float64)
            self.cov[:, 0] += 1.
            self.cov[:, 2] += 1.
        elif covtype == 'full':
            self.bar = torch.eye(d).repeat(k, 1, 1)
        else:
            raise(NotImplementedError())
        self.pi = torch.Tensor(k).fill_(1.).type(dtype=torch.float64) / k
        self.k = k
        self.d = d

    def compute_likelihood_for_each_component(self, X):
        # (K, # of samples)
        return self.compute_loglikelihood_for_each_component(X).exp()

    def compute_loglikelihood_for_each_component(self, X):
        if self.covtype == 'diag':
            raise(NotImplementedError())
            # # get the trace of the inverse covar. matrix
            # covar_inv = 1. / self.cov  # (K, features)

            # # compute the coefficient
            # det = (2 * np.pi) ** (1 / X.size(1)) * \
            #     (self.cov.prod(dim=1)).sqrt()  # (K)
            # coeff = 1. / det  # (K)

            # # tile the design matrix `K` times on the batch dimension
            # K = self.mu.size(0)
            # X = X.unsqueeze(0).repeat(K, 1, 1)

            # # calculate the exponent
            # a = (X - self.mu.unsqueeze(1))  # (K, examples, features)
            # exponent = a ** 2 @ covar_inv.unsqueeze(2)
            # exponent = -0.5 * exponent

            # # compute probability density
            # P = coeff.view(K, 1, 1) * exponent.exp()

        elif self.covtype == 'cross':
            # get the trace of the inverse covar. matrix
            # 全て対角ベクトルで扱われる
            A11 = self.cov[:, 0]  # (K, features / 2)
            A21 = self.cov[:, 1]
            # A12 = self.cov[:, 2]
            A12 = A21
            A22 = self.cov[:, 2]
            A11_inv = 1. / A11  # (K, features / 2)
            A22_inv = 1. / A22
            C1 = A11 - A12 * A22_inv * A21  # (K, features / 2)
            C2 = A22 - A21 * A11_inv * A12
            C1_inv = 1. / C1  # (K, features / 2)
            C2_inv = 1. / C2
            covar_inv = torch.diag_embed(torch.cat([C1_inv, C2_inv], dim=1))
            covar_inv += torch.diag_embed(- C2_inv *
                                          A21 * A11_inv, offset=-int(self.d/2))
            covar_inv += torch.diag_embed(- A11_inv *
                                          A12 * C2_inv, offset=int(self.d/2))
            # (K, d, d)

            # compute the coefficient
            log_det = torch.log(A22).sum(dim=1) + torch.log(C1).sum(dim=1)
            # log_det /= 2.
            _log_coeff = np.log(2 * np.pi) * self.d + log_det
            _log_coeff /= 2.
            log_coeff = 0. - _log_coeff

            # tile the design matrix `K` times on the batch dimension
            X = X.unsqueeze(0).repeat(self.k, 1, 1)

            # calculate the exponent
            a = (X - self.mu.unsqueeze(1))  # (K,  # of samples, features)

            _exponent = torch.bmm(a, covar_inv)  # (K,  # of samples, features)

            # (K,  # of samples, 1)
            _exponent = (_exponent * a).sum(dim=2, keepdim=True)
            exponent = -0.5 * _exponent

            log_P = self.pi.view(self.k, 1, 1) + \
                log_coeff.view(self.k, 1, 1) + exponent
        elif self.covtype == 'full':
            covar_inv = torch.inverse(self.cov)
            log_det = torch.logdet(self.cov)
            _log_coeff = np.log(2 * np.pi) * self.d + log_det
            _log_coeff /= 2.
            log_coeff = 0. - _log_coeff

            # tile the design matrix `K` times on the batch dimension
            X = X.unsqueeze(0).repeat(self.k, 1, 1)

            # calculate the exponent
            a = (X - self.mu.unsqueeze(1))  # (K,  # of samples, features)

            _exponent = torch.bmm(a, covar_inv)  # (K,  # of samples, features)

            # (K,  # of samples, 1)
            _exponent = (_exponent * a).sum(dim=2, keepdim=True)
            exponent = -0.5 * _exponent

            log_P = self.pi.view(self.k, 1, 1) + \
                log_coeff.view(self.k, 1, 1) + exponent
        else:
            raise(NotImplementedError())

        # remove final singleton dimension and return
        return log_P.squeeze(2)  # (K, # of samples)

    def compute_likelihood(self, X):
        P = self.compute_likelihood_for_each_component(X)
        return P.sum(dim=0, keepdim=True).transpose(1, 0)  # (# of samples, 1)

    def compute_posterior(self, X):
        each_ll = self.compute_loglikelihood_for_each_component(
            X)  # (K, # of samples)
        sum_l = each_ll.exp().sum(dim=0, keepdim=True).transpose(1, 0)  # (# of samples, 1)
        log_z = each_ll.transpose(1, 0) - sum_l.log()  # (# of samples, K)
        return log_z.exp()
