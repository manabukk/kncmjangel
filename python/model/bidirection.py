# -*- coding: utf-8 -*-

import torch
from torch import nn
from torch.nn import functional as F

from model.model import KNCNNModel
from model.activations import MyActivations


class BiDirectionalModel(KNCNNModel):
    def __init__(self, model_A2B, model_B2A):
        super(BiDirectionalModel, self).__init__()
        self.model_A2B = model_A2B
        self.model_B2A = model_B2A

        if torch.cuda.device_count() >= 2:
            self.model_A2B.to('cuda:0')
            self.model_A2B.device = 'cuda:0'
            self.model_B2A.to('cuda:1')
            self.model_B2A.device = 'cuda:1'
        else:
            self.model_A2B.to('cuda:0')
            self.model_A2B.device = 'cuda:0'
            self.model_B2A.to('cuda:0')
            self.model_B2A.device = 'cuda:0'

    def forward(self, z):
        dim = z.size(1)//2
        x_A = z[:, :dim]
        x_B = z[:, dim:]
        if torch.cuda.device_count() >= 2:
            x_A = x_A.to('cuda:0')
            x_B = x_B.to('cuda:1')
        else:
            x_A = x_A.to('cuda:0')
            x_B = x_B.to('cuda:0')
        predict_A2B = self.model_A2B(x_A)
        _predict_B2A = self.model_B2A(x_B)
        predict_B2A = {}
        for k, v in _predict_B2A.items():
            predict_B2A[k] = v.to(self.model_A2B.device)

        return {"predict_A2B": predict_A2B, "predict_B2A": predict_B2A}

    def optimize(self, X, Y, optimizer, loss_fn):
        optimizer.zero_grad()
        predict_Y = self(X)
        loss = self.loss_fn(predict_Y, Y, loss_fn)
        loss.backward()
        optimizer.step()
        return loss.item()

    def loss_fn(self, predict_Y, Y, loss_fn):
        return loss_fn(predict_Y, Y)

    def cp(self, z):
        dim = z.size(1)//2
        x_A = z[:, :dim]
        x_B = z[:, dim:]
        predict_A2B = self.model_A2B(x_A)
        predict_B2A = self.model_B2A(x_B)

        return {"predict_A2B": predict_A2B, "predict_B2A": predict_B2A}
