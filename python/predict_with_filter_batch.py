# -*- coding=utf-8 -*-
"""Overview:
    Predicting target by a trained model, given batched source features with VAD.

Usage:
    predictwf <model_filepath> <config_modulepath> <scp_filepath> [-d <vector_size>] [-f <feature_range>] [-v...]
    predictwf -h

Options:
    <model_filepath>                        Path.
    <config_modulepath>                     Path.
    <scp_filepath>                          hoge.
    -d --vector-size <vector_size>          Vector size [default: 25].
    -f --feature-range <feature_range>      Feature range [default: 1,25].
    -v --verbose                            Show in detail.
    -h --help
"""
from docopt import docopt
import os, sys
sys.path.append(os.path.curdir)

from misc.util import set_level, logger, mkdir_p, dynamic_import
from misc.readwrite import load_matfile, load_fseq, write_fseq, load_alignment
from data.data.scp import SCP


from comet_ml import Optimizer
import torch

def load_model_with_config(model_filepath, config_modulepath, device):
    ExpConfig = dynamic_import(config_modulepath)()
    model_configure = ExpConfig.model_configure
    project_name = ExpConfig.project_name
    opt = Optimizer(model_configure, project_name=project_name, experiment_class="OfflineExperiment",offline_directory="/tmp")
    experiment = next(opt.get_experiments())
    model = ExpConfig.get_model(experiment)
    model.load_state_dict(torch.load(
        model_filepath, map_location=torch.device(device)))
    model.eval()
    model.double()
    model.to(device)
    return model

def main(model_filepath, config_modulepath, scp_fp, dim, frange, verbose):
    set_level(verbose)

    logger.debug("Path to the DNN model: {}".format(model_filepath))

    if torch.cuda.is_available():
        device = 'cuda'
    else:
        device = 'cpu'

    model = load_model_with_config(model_filepath, config_modulepath, device)


    scp = SCP(filepath=scp_fp)
    for sfp, idxp, ofp in scp.scp:

        logger.debug("Path to source mcep: {}".format(sfp))
        smcep = load_fseq(sfp, dim, list(range(frange[0], frange[1])))

        logger.debug("Shape of smcep: {}".format(smcep.shape))

        logger.debug("Path to idxs file: {}".format(idxp))
        idxs = load_alignment(path=idxp)
        logger.debug("Length of idxs: {}".format(len(idxs)))

        x = smcep[idxs, :]

        y = model(torch.from_numpy(x).to(device))

        if isinstance(y, dict):
            y = y['predict_Y']

        y = y.cpu().detach().numpy()
        
        tmcep = smcep
        tmcep[idxs, :] = y

        logger.debug("Shape of y: {}".format(y.shape))
        logger.debug("Shape of tmcep: {}".format(tmcep.shape))

        write_fseq(path=ofp, fseq=tmcep)

if __name__ == "__main__":
    args = docopt(__doc__)
    main(model_filepath=os.path.abspath(args['<model_filepath>']),
         config_modulepath=args['<config_modulepath>'],
         scp_fp=os.path.abspath(args['<scp_filepath>']),
         dim=int(args['--vector-size']),
         frange=[int(f) for f in args['--feature-range'].split(',')],
         verbose=int(args['--verbose']))
