# MyLIB

- USAGE が今も動くかわからない

## USAGE
### おまじない
```bash
#!/bin/bash
SCRIPT_DIR=$(cd $(dirname $0); pwd)
kncbin=$SCRIPT_DIR/../bin
# wavfile=$1
# outdir=$2
# sptkbin=$3
wavfile=$SCRIPT_DIR/../sample.wav
outdir=$SCRIPT_DIR/../hoge
sptkbin=/usr/local/bin
echo "wavfile: ${wavfile}"
echo "outdir: ${outdir}"
echo "sptkbin: ${sptkbin}"

mkdir -p $outdir
bname=`basename $wavfile .wav`

rawfile=${outdir}/${bname}.raw 
${kncbin}/wav2raw $wavfile $rawfile
```

### パワースペクトログラム（SPTK）
```bash
framel=1024  # 窓長
framep=16  # シフト長（ポイント数）：1ms
w=0  # 窓関数
bins=`expr $framel / 2 + 1`  # 周波数ビン数

cat $rawfile | \
${sptkbin}/frame -l $framel -p $framep | \
${sptkbin}/window -l $framel -w $w| \
${sptkbin}/fftr -l $framel -P -H \
> ${outdir}/${bname}.psp  # パワースペクトログラム |X(w)|^2

# 対数パワースペクトログラムの描画
${kncbin}/log ${outdir}/${bname}.psp ${outdir}/${bname}.logpsp
${kncbin}/ps2fig ${outdir}/${bname}.logpsp -f $bins -o ${outdir}/${bname}_logpsp.png
```

### ピッチ抽出（SPTK）

```bash
sampling_rate=16  # [kHz]
framep=16  # シフト長（ポイント数）： 1ms
f0floor=120
f0ceil=400

cat ${rawfile} | ${sptkbin}/pitch -a 1 -s $sampling_rate -p $framep -L $f0floor -H $f0ceil -o 1 > ${outdir}/${bname}.f0
# a: {0: RAPT, 1: SWIPE'}
# s: sampling frequency [kHz]
# p: frame shift [point]
# L: minimum fundamental frequency to search for (Hz)
# H: maximum fundamental frequency to search for (Hz) 
# o: output format: {0: pitch, 1: F0, 2: logF0}

${kncbin}/seq2fig -i ${outdir}/${bname}.f0 -vs 1 -fr 0 1 -o ${outdir}/${bname}_f0.png -x Time frames -y F0 [Hz] -vvv

cat ${outdir}/${bname}.f0 | ${sptkbin}/excite -p 80 > ${outdir}/${bname}_f0.raw
sox -e floating-point -c 1 -b 32 -r 16000 ${outdir}/${bname}_f0.raw ${outdir}/${bname}_f0.wav
${kncbin}/wav2fig ${outdir}/${bname}_f0.wav -i -vvvvv -o ${outdir}/${bname}_f0_waveform.png
```

### WORLD 分析

```bash
sampling_rate=16000  # [Hz]
framep=1  # シフト長 [ms]
f0floor=71
f0ceil=600

${kncbin}/world ${rawfile} -fs $framep -r $sampling_rate -f0f $f0floor -f0c $f0ceil -H -f0 ${outdir}/${bname}_world_H.f0 -sp ${outdir}/${bname}_world_H.sp -ap ${outdir}/${bname}_world_H.ap -y ${outdir}/${bname}_world_H.resyn -vvvvvv  # Harvest and no F0 refine

# 分析再合成
sox -e floating-point -c 1 -b 32 -r 16000 ${outdir}/${bname}_world_H.resyn ${outdir}/${bname}_world_H.wav

# F0 描画
# SPTK ベースの F0 とは何故か 1 フレームのズレがある
${kncbin}/seq2fig -i ${outdir}/${bname}_world_H.f0 -vs 1 -fr 0 1 -o ${outdir}/${bname}_world_H_f0.png -x Time frames -y F0 [Hz] -vvv

# 対数パワースペクトログラムの描画
${kncbin}/log ${outdir}/${bname}_world_H.sp ${outdir}/${bname}_world_H.logpsp
${kncbin}/ps2fig ${outdir}/${bname}_world_H.logpsp -f $vs -o ${outdir}/${bname}_world_H_logpsp.png
```

### メルケプストラム係数（SPTK）

```bash
dim=24  # パワーを除く次元数
a=0.42  # For 16kHz sampling
framel=1024  # 窓長

${sptkbin}/mcep -a $a -m $dim -l $framel -q 4 ${outdir}/${bname}.psp > ${outdir}/${bname}.mcep

# 可視化
${kncbin}/ps2fig ${outdir}/${bname}.mcep -f `expr $dim + 1` -o ${outdir}/${bname}_mcep.png
```

### スペクトル包絡の可視化

```bash
framel=1024  # 窓長
bins=`expr $framel / 2 + 1`  # 周波数ビン数
dim=24  # パワーを除くメルケプ次元数

f=1050  # 可視化したいフレーム

# 生スペクトルとメルケプからの再合成、WORLD 分析に基づくスペクトル包絡について各 $f フレームを切り出す
cat ${outdir}/${bname}.psp | ${sptkbin}/bcut +f -l $bins -s $f -e $f > ${outdir}/${bname}.psp.f$f
cat ${outdir}/${bname}.mcep | ${sptkbin}/bcut +f -n $dim -s $f -e $f > ${outdir}/${bname}.mcep.f$f
cat ${outdir}/${bname}_world.sp | ${sptkbin}/bcut +f -l $bins -s $f -e $f > ${outdir}/${bname}_world.psp.f$f

# 各スペクトルを対数化
# 生スペクトル
${kncbin}/log ${outdir}/${bname}.psp.f$f ${outdir}/${bname}.logpsp.f$f

# メルケプから構成
cat ${outdir}/${bname}.mcep.f$f | ${sptkbin}/mgc2sp -m $dim -a 0.42 -g 0 -l $framel -o 3 > ${outdir}/${bname}.mcep2psp.f$f
${kncbin}/log ${outdir}/${bname}.mcep2psp.f$f ${outdir}/${bname}.mcep2logpsp.f$f

# WORLD ベース
${kncbin}/log ${outdir}/${bname}_world.psp.f$f ${outdir}/${bname}_world.logpsp.f$f

# 可視化
${kncbin}/seqs2fig -i ${outdir}/${bname}.logpsp.f$f ${outdir}/${bname}.mcep2logpsp.f$f  ${outdir}/${bname}_world.logpsp.f$f -vs 1 -fr 0 1 -l raw mcep world -o ${outdir}/${bname}_logpspf$f.png
```