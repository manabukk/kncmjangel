#include "fastdp.h"

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <float.h>
#include <math.h>

int* run(const int x_size,
         const int y_size,
         const double* x_seq,
         const double* y_seq,
         const int dim,
         const int path_num,
         const int* path,
         const double* cost,
         const double constraint_value) {

    const int max_frame_size = x_size + y_size;

    int _max_path_step = 0;
    for (int lp = 0; lp < path_num; ++lp) {
        int path_step = path[lp*2 + 0] + path[lp*2 + 1];
        if (path_step > _max_path_step) {
            _max_path_step = path_step;
        }
    }
    const int max_path_step = _max_path_step;

    double** global_cost = (double**)malloc(sizeof(double*)*(max_path_step+1));
    for (int i = 0; i < max_path_step + 1; ++i) {
        global_cost[i] = (double*)malloc(sizeof(double)*1);
    }

    int* previous_sizes = (int*)malloc(sizeof(int) * max_path_step);
    for (int i = 0; i < max_path_step; ++i)
    {
        previous_sizes[i] = 0;
    }

    int* viterbi_path = (int*)malloc(sizeof(int)*(long)(y_size)*(long)(x_size));

    // initialize parameter
    for (int i = 0; i < x_size; ++i) {
        for (int j = 0; j < y_size; ++j) {
            viterbi_path[i*y_size+j] = -1;
        }
    }

    //    DP
    const int smaller_size = (x_size < y_size)? x_size : y_size;
    for (int t = 0; t < max_frame_size; ++t) {

        const int size = t + 1;

        if (t == 0) {

        } else {
            free(global_cost[max_path_step]);

            for (int j = max_path_step; j > 0; --j)
            {
                global_cost[j] = global_cost[j - 1];
            }
        }

        global_cost[0] = (double*)malloc(sizeof(double)*(size));

        for (int j = 0; j < size; ++j) {
            global_cost[0][j] = DBL_MAX;
        }
        if (t == 0) {
            global_cost[0][0] = 0.0;
            continue;
        }

        double min_cost = DBL_MAX;
        double max_cost = DBL_MIN;
        long double i_whole_cost = 0.0;

        for (int pos_x = 0; pos_x < x_size; ++pos_x) {
            int pos_y = t - pos_x;

            if (is_invalid_pos(pos_x, pos_y, x_size - 1, 0, y_size - 1, 0, constraint_value)) {
                continue;
            }

            for (int lp = 0; lp < path_num; ++lp)
            {
                if (is_invalid_path(pos_x, pos_y, lp, path, x_size - 1, 0, y_size - 1, 0, constraint_value))
                {
                    continue;
                }
                const int pre_x = pos_x - path[lp * 2 + 0];
                const int t_diff = path[lp * 2 + 0] + path[lp * 2 + 1];
                const double dist = cdist(pos_x, pos_y, x_seq, y_seq, dim);
                const double c = global_cost[t_diff][pre_x] + dist * cost[lp];

                if (c < global_cost[0][pos_x])
                {
                    global_cost[0][pos_x] = c;
                    viterbi_path[pos_x * y_size + pos_y] = lp;

                    if (c > max_cost) { max_cost = c; }
                    if (c < min_cost) { min_cost = c; }
                }        
            }
            i_whole_cost += global_cost[0][pos_x];
        }

        if (i_whole_cost > DBL_MAX * 0.5) {
            fprintf(stderr, "WARNING: costs are huge and subtracted.\n");
            const double mean = i_whole_cost / (float)(size);
            for (int i = 0; i < max_path_step; ++i) {
                for (int j = 0; j < previous_sizes[i]; ++j) {
                    global_cost[i+1][j] -= mean;
                }
            }
            for (int j = 0; j < size; ++j)
            {
                global_cost[0][j] -= mean;
            }
        }

        if (max_cost > DBL_MAX / 2.0)
        {
            fprintf(stderr, "WARNING: costs are huge...\n");
        }

        for (int j = max_path_step - 1; j > 0; --j)
        {
            previous_sizes[j] = previous_sizes[j-1];
        }
        previous_sizes[0] = size;

    }

    for (int i = 0; i < max_path_step + 1; ++i)
    {
        free(global_cost[i]);
    }
    free(global_cost);

    //    back trace
    int pos_x = x_size-1;
    int pos_y = y_size-1;
    int frame_count = 1;
    int lp;

    while ((lp = viterbi_path[pos_x*y_size+pos_y]) > -1) { // -1 is the end flag.
        pos_x = pos_x - path[lp*2+0];
        pos_y = pos_y - path[lp*2+1];
        frame_count++;
    }

    int* alignment = (int*)malloc(sizeof(int)*frame_count*2+1);
    pos_x = x_size-1;
    pos_y = y_size-1;
    alignment[0] = frame_count;
    for (int frame = frame_count - 1; frame > -1; --frame)
    {
        alignment[frame*2+1] = pos_x;
        alignment[frame*2+1+1] = pos_y;
        lp = viterbi_path[pos_x*y_size+pos_y];
        pos_x = pos_x - path[lp*2+0];
        pos_y = pos_y - path[lp*2+1];
    }

    free(viterbi_path);
    return alignment;
}

int is_invalid_path(const int x, const int y, const int lp, const int* path, const int x_max, const int x_min, const int y_max, const int y_min, const double constraint_value) {
    int pre_x = x - path[lp*2+0];
    int pre_y = y - path[lp*2+1];
    if (pre_x < x_min) {
        return 1;
    }
    if (pre_x > x_max) {
        return 1;
    }
    if (pre_y < y_min) {
        return 1;
    }
    if (pre_y > y_max) {
        return  1;
    }
    double bias = pre_y - (y_max / (float)(x_max)) * pre_x;
    if (fabs(bias) / (float)(y_max) > constraint_value) {
        return 1;
    }
    return 0;
}

int is_invalid_pos(const int x, const int y, const int x_max, const int x_min, const int y_max,  const int y_min, const double constraint_value) {
    if (x < x_min) {
        return 1;
    }
    if (x > x_max) {
        return 1;
    }
    if (y < y_min) {
        return 1;
    }
    if (y > y_max) {
        return 1;
    }
    double bias = y - (y_max / (float)(x_max)) * x;
    if (fabs(bias) / (float)(y_max) > constraint_value)
    {
        return 1;
    }
    return 0;
}

double cdist(const int pos_x, const int pos_y, const double* x, const double* y, const int dim) {
    double dist = 0.0;
    for (int d = 0; d < dim; ++d) {
        dist += (x[pos_x * dim + d] - y[pos_y * dim + d]) * (x[pos_x * dim + d] - y[pos_y * dim + d]);
    }
    return sqrt(dist/(float)(dim));
}