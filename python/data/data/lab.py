# -*- coding: utf-8 -*-

import os
from misc.util import set_level, logger


class Lab(object):
    """docstring for Lab."""

    def __init__(self, filepath=None):
        super(Lab, self).__init__()
        self.lab = None
        self.filepath = None
        self.lab_type = None

        if filepath is not None:
            self.read(filepath)

    def __str__(self):
        desc = "Filepath: {}".format(self.filepath)
        return desc

    def read(self, filepath):
        with open(filepath, 'r') as f:
            lab = [row.split() for row in f.readlines()]

        if len(lab[0]) == 3:
            self.lab_type = 3
            self.lab = [[float(_l[0]), float(_l[1]), _l[2]]for _l in lab]  # [start [sec], end [sec], segment name]
        elif len(lab[0]) == 1:
            self.lab_type = 1
            self.lab = [[float(_l[0])]
                        for _l in lab]  # [start [sec]]

        else:
            print(len(lab[0]))
            raise NotImplementedError()
        self.filepath = filepath
        logger.debug("Filepath: {}".format(self.filepath))
        logger.debug("Segment num: {}".format(len(self.lab)))
        logger.debug("Column size: {}".format(self.lab_type))
        return self

    def get_grid(self, t='all'):
        if t == 'all':
            _all_grid = [_l for l in self.lab for i, _l in enumerate(l) if i == 0 or i == 1]
            _all_grid_set = set(_all_grid)
            all_grid = sorted(list(_all_grid_set))
            return all_grid
        else:
            raise NotImplementedError()

    def write(self, filepath):
        raise NotImplementedError()


class HTKLab(object):
    """docstring for Lab."""

    def __init__(self, filepath=None):
        super(HTKLab, self).__init__()
        self.lab = None
        self.filepath = None

        if filepath is not None:
            self.read(filepath)

    def __str__(self):
        desc = "Filepath: {}".format(self.filepath)
        return desc

    def read(self, filepath):
        with open(filepath, 'r') as f:
            lab = [row.split() for row in f.readlines()]

        self.lab = [[float(_l[0]), float(_l[1]), _l[2]]for _l in lab]

        self.filepath = filepath
        logger.debug("Filepath: {}".format(self.filepath))
        logger.debug("Segment num: {}".format(len(self.lab)))
        return self

    def get_grid(self, t='all'):
        if t == 'all':
            _all_grid = [_l for l in self.lab for i,
                         _l in enumerate(l) if i == 0 or i == 1]
            _all_grid_set = set(_all_grid)
            all_grid = sorted(list(_all_grid_set))
            return all_grid
        else:
            raise NotImplementedError()

    def write(self, filepath):
        raise NotImplementedError()


def lab2htklab(lab):  # Implemented for Blizzard Challenge 2019.
    if lab.lab_type == 3:
        htklab = HTKLab()
        htklab.filepath = lab.filepath
        htklab.lab = lab.lab
    elif lab.lab_type == 1:
        pass
        htklab = HTKLab()
        htklab.filepath = lab.filepath
        bname = os.path.splitext(os.path.basename(lab.filepath))[0]
        htklab.lab = [[s, e, "{}_{}".format(bname, str(i+1).zfill(2) )]  for i, (s, e) in enumerate(zip([_s[0] for _s in lab.lab[:-1]], [_e[0] for _e in lab.lab[1:]]))]

    return htklab
