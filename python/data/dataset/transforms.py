# -*- coding: utf-8 -*-

import torch
from torchvision.transforms import Compose
from misc.util import dynamic_import
from misc.readwrite import load_fseq
import os

def get_transforms(transforms_list):
    return Compose([dynamic_import("data.dataset.transforms."+transform_name)() for transform_name in transforms_list])

class ToTensor(object):
    """Convert ndarrays in sample to Tensors."""

    def __call__(self, sample):
        x, y = sample['x'], sample['y']

        return {'x': torch.from_numpy(x),
                'y': torch.from_numpy(y)}


class Normalize(object):
    def __init__(self, param_dirpath="/Users/gaku/workspace/kncmjAngel/hoge_norm", xvs=4, yvs=4, epsilon=0.0001):
        mean_X = load_fseq(path=os.path.join(
            param_dirpath, "mean_X"), vec_size=xvs, feature_range=range(xvs))
        std_X = load_fseq(path=os.path.join(
            param_dirpath, "std_X"), vec_size=xvs, feature_range=range(xvs))
        mean_Y = load_fseq(path=os.path.join(
            param_dirpath, "mean_Y"), vec_size=yvs, feature_range=range(yvs))
        std_Y = load_fseq(path=os.path.join(
            param_dirpath, "std_Y"), vec_size=yvs, feature_range=range(yvs))

        self.mean_X = torch.from_numpy(mean_X)
        self.std_X = torch.from_numpy(std_X)
        self.mean_Y = torch.from_numpy(mean_Y)
        self.std_Y = torch.from_numpy(std_Y)
        self.epsilon = epsilon

    def __call__(self, sample):
        x, y = sample['x'], sample['y']

        return {'x': (x - self.mean_X) / (self.std_X + self.epsilon),
                'y': (y - self.mean_Y) / (self.std_Y + self.epsilon)}


    def __repr__(self):
        return self.__class__.__name__ + '(mean_X={0}, std_X={1}, mean_Y={2}, std_Y={3})'.format(self.mean_X, self.std_X, self.mean_Y, self.std_Y)
