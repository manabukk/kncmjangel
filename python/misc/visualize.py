# -*- coding: utf-8 -*-

import math
import argparse
import os

import matplotlib
# matplotlib.use('Agg')
import numpy as np

# from misc.util import mkdir_p
from misc import color
from misc.util import dynamic_import
# from misc.readwrite import load_fseq, load_alignment_index


def get_color(color_name):
    return getattr(color, color_name)

# def matrix2hmap(matrix, vmin, vmax, save_filepath=None):
#     matrix = np.asarray(matrix)
#     if save_filepath is not None:
#         matplotlib.use('Agg')
#     import matplotlib.pyplot as plt
#     fig = plt.figure()
#     heatmap = plt.pcolor(matrix, cmap=plt.cm.bwr, vmin=-1, vmax=1)
#     fig.colorbar(heatmap)
#     plt.xlim(0, matrix.shape[0])
#     plt.ylim(matrix.shape[1], 0)

#     plt.draw()
#     plt.show()
#     if save_filepath:
#         fig.savefig(save_filepath)


# def matrices2hmaps(matrices, vmin, vmax, save_dirpath=None, ext_list=["eps", "png"], filename_0padding=True):
#     matrices = np.asarray(matrices)
#     if save_dirpath is not None:
#         matplotlib.use('Agg')
#     import matplotlib.pyplot as plt
#     for i, matrix in enumerate(matrices):
#         fig = plt.figure()
#         heatmap = plt.pcolor(matrix, cmap=plt.cm.bwr, vmin=-1, vmax=1)
#         fig.colorbar(heatmap)
#         plt.xlim(0, matrix.shape[0])
#         plt.ylim(matrix.shape[1], 0)

#         plt.draw()
#         plt.show()
#         if save_dirpath:
#             filename_format = "%0" + \
#                 str(int(math.log10(matrices.shape[0]) + 1)
#                     ) + "d" if filename_0padding else "%d"
#             save_dirpath += "/" if save_dirpath[-1] == "/" else ""
#             for ext in ext_list:
#                 mkdir_p(save_dirpath + ext + "/")
#             for ext in ext_list:
#                 filename = filename_format % i
#                 fig.savefig(save_dirpath + ext + "/" + filename + "." + ext)


def alignments2fig(alignment_list, legends, x_label=None, y_label=None, save_filepath=None, color_palette='pastel'):
    # assert len(alignment_list) <= len(color_pallette)
    if save_filepath is not None:
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import seaborn as sns
    # sns.set("whitegrid")
    sns.set_style("whitegrid")
    tick = matplotlib.ticker
    fig = plt.figure()
    color_palette=get_color(color_palette)

    if x_label is not None:
        plt.xlabel(x_label)
    if y_label is not None:
        plt.ylabel(y_label)
    # list(map(lambda i_align: plt.plot(i_align[1][1::2], i_align[1][2::2],
    #                              label=legends[i_align[0]]), enumerate(alignment_list)))
    list(map(lambda i_align: plt.plot(i_align[1][1::2],
                                      i_align[1][2::2],
                                      label=legends[i_align[0]],
                                      color=color_palette[i_align[0]],
                                      lw=1.0), enumerate(alignment_list)))
    x_max = max([align[-2] - 1 for align in alignment_list])
    y_max = max([align[-1] - 1 for align in alignment_list])
    plt.xlim(0, x_max)
    plt.ylim(0, y_max)
    plt.gca().set_aspect('equal', adjustable='box')
    locator_step = int((max([x_max, y_max]) + 500) / 1000) * 100
    if locator_step < 100:
        locator_step = 100
    plt.tick_params(labelsize=8)
    plt.gca().xaxis.set_major_locator(tick.MultipleLocator(locator_step))
    plt.gca().yaxis.set_major_locator(tick.MultipleLocator(locator_step))
    plt.gca().xaxis.set_minor_locator(tick.MultipleLocator(locator_step / 2))
    plt.gca().yaxis.set_minor_locator(tick.MultipleLocator(locator_step / 2))
    # lab_x = [58, 79, 113, 140, 175, 199, 221, 250, 267, 302, 336, 367, 384, 421, 438, 471, 491, 547, 579, 600, 626, 655, 689, 708, 737],
    # lab_y = [71, 86, 123, 132, 156, 187, 201, 221, 254, 288, 327, 354, 374, 400, 428, 446, 470, 503, 516, 540, 566, 595, 625, 641, 674]
    # plt.scatter(lab_x, lab_y, c='purple', marker='.')
    plt.grid(which='both')
    lgd = plt.legend(bbox_to_anchor=(1, 1))
    plt.subplots_adjust(left=0.1, right=0.75)
    plt.draw()
    if save_filepath:
        fig.savefig(save_filepath, bbox_extra_artists=(
            lgd,), bbox_inches='tight')
        return save_filepath
    else:
        plt.show()


def alignments_with_ref2fig(alignment_list, x_grid, y_grid, legends, x_label=None, y_label=None, save_filepath=None, color_palette='pastel', ref_type='c'):
    # assert len(alignment_list) <= len(color_pallette)
    if save_filepath is not None:
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import matplotlib.patches as patches
    import seaborn as sns
    # sns.set("whitegrid")
    sns.set_style("whitegrid")
    tick = matplotlib.ticker
    fig = plt.figure()
    color_palette = get_color(color_palette)

    if x_label is not None:
        plt.xlabel(x_label)
    if y_label is not None:
        plt.ylabel(y_label)
    # list(map(lambda i_align: plt.plot(i_align[1][1::2], i_align[1][2::2],
    #                              label=legends[i_align[0]]), enumerate(alignment_list)))
    list(map(lambda i_align: plt.plot(i_align[1][1::2],
                                      i_align[1][2::2],
                                      label=legends[i_align[0]],
                                      color=color_palette[i_align[0]],
                                      lw=1.0), enumerate(alignment_list)))
    x_max = max([align[-2] - 1 for align in alignment_list])
    y_max = max([align[-1] - 1 for align in alignment_list])
    plt.xlim(0, x_max)
    plt.ylim(0, y_max)

    if ref_type in ('g', 'grid'):
        plt.hlines(y_grid, 0, x_max, colors="black", linestyle="solid", label="", linewidth=0.8)
        plt.vlines(x_grid, 0, y_max,
                colors="black", linestyle="solid", label="", linewidth=0.8)
    elif ref_type in ('c', 'circle', 'p', 'point'):
        assert len(x_grid) == len(
            y_grid), "Error: The length of labels must be same b/w x-axis and y-axis, in the case of ref_type = circle."

        plt.scatter(x_grid, y_grid, c='black', marker='.')
    elif ref_type in ('r', 'rectangle', 'b', 'box'):
        assert len(x_grid) == len(y_grid), "Error: The length of labels must be same b/w x-axis and y-axis, in the case of ref_type = rectangle."
        for i in range(len(x_grid)-1):
            r = patches.Rectangle(xy=(x_grid[i], y_grid[i]), width=x_grid[i+1]-x_grid[i],
                                  height=y_grid[i+1]-y_grid[i], ec='lightgrey', facecolor='lightgrey', fill=True)
            plt.gca().add_patch(r)
    else:
        raise NotImplementedError()

    plt.gca().set_aspect('equal', adjustable='box')
    locator_step = int((max([x_max, y_max]) + 500) / 1000) * 100
    if locator_step < 100:
        locator_step = 100
    plt.tick_params(labelsize=8)
    plt.gca().xaxis.set_major_locator(tick.MultipleLocator(locator_step))
    plt.gca().yaxis.set_major_locator(tick.MultipleLocator(locator_step))
    plt.gca().xaxis.set_minor_locator(tick.MultipleLocator(locator_step / 2))
    plt.gca().yaxis.set_minor_locator(tick.MultipleLocator(locator_step / 2))

    plt.grid(which='both')
    lgd = plt.legend(bbox_to_anchor=(1, 1))
    # plt.subplots_adjust(left=0.1, right=0.75)
    plt.subplots_adjust(left=0.1, right=0.75)
    plt.draw()
    if save_filepath:
        fig.savefig(save_filepath, bbox_extra_artists=(
            lgd,), bbox_inches='tight')
        return save_filepath
    else:
        plt.show()


def fseq2fig(fseq, save_filepath=None, x_label=None, y_label=None, x_min=None, x_max=None, y_min=None, y_max=None):
    assert len(fseq.shape) is 2, 'ERROR'

    sequences = np.asarray(fseq).transpose()
    vec_size = sequences.shape[0]
    seq_size = sequences.shape[1]

    # if save_filepath is not None:
        # matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import seaborn as sns
    sns.set_style("whitegrid")

    if x_label is not None:
        plt.xlabel(x_label)
    if y_label is not None:
        plt.ylabel(y_label)

    fig = plt.figure(figsize=(15, vec_size * 5))
    for dim, seq in enumerate(sequences):
        plt.subplot(vec_size, 1, dim + 1)
        plt.plot(seq)

    plt.xlim(x_min if x_min is not None else 0, x_max if x_max is not None else len(sequences[0]))
    plt.ylim(y_min, y_max)

    plt.draw()
    if save_filepath:
        fig.savefig(save_filepath)
    else:
        plt.show()



# def fseqs2fig(fseq_list, legends=None, save_filepath=None):
#     sequences_list = []
#     for fseq in fseq_list:
#         sequences = np.asarray(fseq).transpose()
#         assert len(sequences.shape) is 2, 'ERROR'
#         sequences_list.append(sequences)
#     vec_size = sequences_list[0].shape[0]
#     seq_size = sequences_list[0].shape[1]

#     if save_filepath is not None:
#         matplotlib.use('Agg')
#     import matplotlib.pyplot as plt
#     fig = plt.figure(figsize=(15, vec_size * 5))
#     for dim in range(vec_size):
#         plt.subplot(vec_size, 1, dim + 1)
#         for seqences in sequences_list:
#             plt.plot(seqences[dim])

#     plt.draw()
#     plt.show()
#     if save_filepath:
#         fig.savefig(save_filepath)
