# -*- coding: utf-8 -*-

import os
from misc.util import set_level, logger


class SCP(object):
    """docstring for SCP."""
    def __init__(self, filepath=None):
        super(SCP, self).__init__()
        self.scp = None
        self.column_len = None
        self.row_len = None
        self.filepath = None

        if filepath is not None:
            self.read(filepath)

    def __str__(self):
        desc = "Filepath: {}".format(self.filepath)
        desc += ", row length: {}".format(self.row_len)
        desc += ", column length: {}".format(self.column_len)
        return desc
            
    def read(self, filepath):
        with open(filepath, 'r') as f:
            scp = [row.split() for row in f.readlines()]
        
        self.row_len = len(scp)
        self.column_len = len(scp[0])
        self.scp = scp
        self.filepath = filepath
        logger.debug("Filepath: {}".format(self.filepath))
        logger.debug("row length: {}".format(self.row_len))
        logger.debug("column length: {}".format(self.column_len))
        return self

    def write(self, filepath):
        raise NotImplementedError()

    
