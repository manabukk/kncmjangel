# -*- coding: utf-8 -*-
"""Overview:
    Plotting an alignment.

Usage:
    aligns2fig <align_filepaths> [-o <save_filepath>] [-l <legends>] [-x <x_label>] [-y <y_label>] [-v...]
    aligns2fig -h

Options:
    <align_filepaths>                   File paths to the alignment files to be plotted. They must be comma separated.
    -l --legends <legends>              Must be Comma separated.
    -x --x-label <x_label>              X-axis label. Commas are converted to spaces.
    -y --y-label <y_label>              Y-axis label. Commas are converted to spaces.
    -o --save-filepath <save_filepath>  Path to the output file.
    -v --verbose                        Show the process in detail.
    -h --help                           Show this screen and exit.
"""

from docopt import docopt
import os
from misc.util import set_level, logger

from misc.visualize import alignments2fig
from misc.readwrite import load_alignment


def main(align_filepaths, legends, x_label, y_label, save_filepath, verbose):
    set_level(verbose)
    alignments = [load_alignment(align) for align in align_filepaths]
    if legends is None:
        legends = [os.path.basename(align) for align in align_filepaths]
    else:
        assert len(align_filepaths) == len(legends)
    alignments2fig(alignment_list=alignments,
                   legends=legends,
                   x_label=" ".join(x_label) if x_label else None,
                   y_label=" ".join(y_label) if y_label else None,
                   save_filepath=save_filepath)

if __name__ == "__main__":
    args = docopt(__doc__)
    main(align_filepaths=[os.path.abspath(os.path.expanduser(file)) for file in args['<align_filepaths>'].split(',')],
    # main(align_filepaths=args['<align_filepaths>'],
         legends=args['--legends'].split(',') if args['--legends'] is not None else None,
         x_label=args['--x-label'].split(
             ',') if args['--x-label'] is not None else None,
         y_label=args['--y-label'].split(',') if args['--y-label'] is not None else None,
         save_filepath=os.path.abspath(args['--save-filepath']) if args['--save-filepath'] is not None else None,
         verbose=int(args['--verbose']))
