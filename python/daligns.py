# -*- coding: utf-8 -*-
"""Overview:
    Calculating the distance of alignments.

Usage:
    daligns <align_filepath1> [<align_filepath2>] [-t <dtype>] [-v...]
    daligns -h

Options:
    <align_filepath1>                   File path to the alignment files to calculate the distance. 
    <align_filepath2>                   File path to the alignment files to calculate the distance. If you set nothing, the distance between alignment1 and the diagonal line is calculated. 
    -t --type <dtype>                   Type of calculating the distance (a/l) [default: a].
    -v --verbose                        Show the process in detail.
    -h --help                           Show this screen and exit.
"""

from docopt import docopt
import os, sys

from misc.util import set_level, logger
from misc.readwrite import load_alignment
from misc.cdist import compute_daligns, compute_daligns_local


def main(align_filepath1, align_filepath2, dtype, verbose):
    set_level(verbose)

    alignment1 = load_alignment(align_filepath1)

    if align_filepath2 is not None:
        alignment2 = load_alignment(align_filepath2)
    else:
        x_size = alignment1[-2]
        y_size = alignment1[-1]
        logger.debug("x size: {}".format(x_size))
        logger.debug("y size: {}".format(y_size))
        alignment2 = [[x, round((y_size / x_size) * x)] if x < x_size else [x_size, y_size] for x in range(x_size+1)]
        # TODO: lp2 に従っているとは限らない。
        alignment2 = [len(alignment2)] + [a for _a in alignment2 for a in _a]

    assert alignment1[-1] == alignment2[-1] and alignment1[-2] == alignment2[-2]

    if dtype in ('a', 'area'):
        _distance = compute_daligns(alignment1[1:], alignment2[1:])
        distance = _distance / (alignment1[-2] * alignment1[-1])
    elif dtype in ('l', 'local'):
        _distance = compute_daligns_local(alignment1[1:], alignment2[1:])
        distance = _distance
    else:
        raise NotImplementedError()

    sys.stdout.write("{:.6g}".format(distance))

if __name__ == "__main__":
    args = docopt(__doc__)
    main(align_filepath1=os.path.abspath(os.path.expanduser(args['<align_filepath1>'])),
         align_filepath2=os.path.abspath(
             os.path.expanduser(args['<align_filepath2>'])) if args['<align_filepath2>'] is not None else None,
         dtype=args['--type'],
         verbose=int(args['--verbose']))
