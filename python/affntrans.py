# -*- coding: utf-8 -*-
"""Overview:
    Transformation via affine.

Usage:
    affntrans <source_file> <output_path> -A <affine_A_path> -b <affine_b_path> [-d <vector_size>] [-f <feature_range>] [-z] [-l] [-v...]
    affntrans -h

Options:
    <source_file>                             hoge.
    <output_path>                             hoge.
    -d --vector-size <vector_size>            Vector size [default: 1].
    -f --feature-range <feature_range>        Feature range [default: 0,1].
    -A --affine-A <affine_A_path>             Path to the parameter of A (t = As +b).
    -b --affine-b <affine_b_path>             Path to the parameter of b (t = As +b).
    -z --zero-retain                          hoge.
    -l --log-domain                           hoge.
    -v --verbose                              Show in detail.
    -h --help                                 Show this message.
"""

from docopt import docopt
import sys
import os
import numpy as np

from misc.affine import affine_trans
from misc.readwrite import load_fseq, load_matrix, load_vector, write_fseq
from misc.util import logger, set_level


def main(sfp, ofp, d, frange, zero_retain, verbose, affine_A_path, affine_b_path, log_domain):

    set_level(verbose)
    sfseq = load_fseq(path=sfp, vec_size=d,
                      feature_range=range(frange[0], frange[1]))
    logger.debug("Source features shape: {}".format(str(sfseq.shape)))

    logger.debug("affine A path: {}".format(affine_A_path))
    logger.debug("affine b path: {}".format(affine_b_path))

    logger.debug("Affine transformation is applied to source features.")
    A = load_matrix(affine_A_path, len(range(frange[0], frange[1])))
    b = load_vector(affine_b_path)
    convfseq = np.zeros(shape=sfseq.shape)
    if log_domain:
        _sfseq = np.log2(sfseq[sfseq != 0])
        if len(_sfseq.shape) == 1:
            _sfseq = _sfseq[:, None]

        convfseq[sfseq != 0] = np.power(2, affine_trans(_sfseq, A, b).squeeze())
    else:
        convfseq = affine_trans(sfseq, A, b)
    
    if zero_retain:
        convfseq[sfseq == 0] = 0
    
    write_fseq(path=ofp, fseq=convfseq)

if __name__ == '__main__':
    args = docopt(__doc__)

    main(sfp=os.path.abspath(args['<source_file>']),
         ofp=os.path.abspath(args['<output_path>']),
         d=int(args['--vector-size']),
         frange=[int(f) for f in args['--feature-range'].split(',')],
         affine_A_path=os.path.abspath(args['--affine-A']) if args['--affine-A'] is not None else None,
         affine_b_path=os.path.abspath(
             args['--affine-b']) if args['--affine-b'] is not None else None,
         zero_retain=args['--zero-retain'],
         log_domain=args['--log-domain'],
         verbose=int(args['--verbose']))
