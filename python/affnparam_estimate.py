# -*- coding: utf-8 -*-
"""Overview:
    Estimating the parameters of Affine transformation: A and b between source/target features.

Usage:
    affnp <scp_filepath> <output_prefix> [-d <vector_size>] [-f <feature_range>] [-v...]
    affnp -h

Options:
    <scp_filepath>                      Path to the scp file. Each row in the file has 3 columns, e.g. "/hoge/sspk_sentence1.mcep /foo/tspk_sentence1.mcep /bar/sspk-tspk-sentence1.align".
    <output_prefix>                     Prefix used to output files. If the prefix is "/hoge/bar", the outputs files are "/hoge/bar.A" and "/hoge/bar.b" which are the params of Affine: t = As + b. 
    -d --vector-size <vector_size>      Vector size [default: 25]
    -f --feature-range <feature_range>  Feature range [default: 1,25]
    -v --verbose                        Show in detail.
    -h --help                           Show this message.
"""

from docopt import docopt
import os

import numpy as np

from misc.affine import affine_estimation
from misc.pdata import get_parallel_data
from misc.readwrite import (load_alignment, load_fseq, write_matrix,
                            write_vector)
from data.data.scp import SCP
from misc.util import set_level, logger


def main(scp_fp, output_prefix, d, frange, verbose):
    set_level(verbose)

    sfseqfp_list = []
    tfseqfp_list = []
    afp_list = []
    scp = SCP(filepath=scp_fp)
    for sfp, tfp, afp in scp.scp:
        sfseqfp_list.append(sfp)
        tfseqfp_list.append(tfp)
        afp_list.append(afp)

    sfseq_list = [load_fseq(path=fp, vec_size=d, feature_range=range(frange[0], frange[1]))
                  for fp in sfseqfp_list]
    tfseq_list = [load_fseq(path=fp, vec_size=d, feature_range=range(frange[0], frange[1]))
                  for fp in tfseqfp_list]
    align_list = [load_alignment(afp) for afp in afp_list]

    p_fseqs_list = [get_parallel_data(fseq_list=[sfseq, tfseq], alignment=align)[
        0] for sfseq, tfseq, align in zip(sfseq_list, tfseq_list, align_list)]

    concat_fseq = [[], []]
    for p_fseqs in p_fseqs_list:
        concat_fseq[0].extend(list(p_fseqs[0]))
        concat_fseq[1].extend(list(p_fseqs[1]))

    p_fseqs = np.asarray(concat_fseq)
    A, b = affine_estimation(p_fseqs[0], p_fseqs[1])

    logger.debug("A shape: {}".format(str(A.shape)))
    logger.debug("b shape: {}".format(str(b.shape)))

    write_matrix(output_prefix + ".A", A)
    write_vector(output_prefix + ".b", b)
    logger.debug(output_prefix + ".A")
    logger.debug(output_prefix + ".b")


if __name__ == '__main__':
    args = docopt(__doc__)

    main(scp_fp=os.path.abspath(args['<scp_filepath>']),
    output_prefix=os.path.abspath(args['<output_prefix>']),
    d=int(args['--vector-size']),
    frange=[int(f) for f in args['--feature-range'].split(',')],
    verbose=int(args['--verbose']))
