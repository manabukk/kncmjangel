# -*- coding=utf-8 -*-
"""Overview:
    Plotting sequence (e.g. waveform).

Usage:
    seq2fig <input_filepath> [-o <save_filepath>] [-d <vector_size>] [-f <feature_indices>] [-x <x_label>] [-y <y_label>] [-s <x_scale>] [-v...]
    seq2fig -h

Options:
    <input_filepath>                        hoge.
    -d --vector-size <vector_size>          Vector size [default: 25].
    -f --feature-indices <feature_indices>  Feature indices [default: 1,2,4,8,16,24].
    -x --x-label <x_label>                  X-axis label. Commas are converted to spaces.
    -y --y-label <y_label>                  Y-axis label. Commas are converted to spaces.
    -s --x-scale <x_scale>                  Hoge [default: 1.0].
    -o --save-filepath <save_filepath>      Path to the output file.
    -v --verbose                            Show the process in detail.
    -h --help                               Show this screen and exit.
"""

from misc.readwrite import load_fseq
import numpy as np

from docopt import docopt

from misc.util import set_level, logger

import os

def fseq2fig(fseq, save_filepath=None, x_label=None, y_label=None, x_min=None, x_max=None, y_min=None, y_max=None, x_scale=1.0, subplot_labels=None):
    assert len(fseq.shape) is 2, 'ERROR'

    sequences = np.asarray(fseq).transpose()
    vec_size = sequences.shape[0]
    seq_size = sequences.shape[1]

    import matplotlib
    if save_filepath is not None:
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import seaborn as sns
    sns.set_style("whitegrid")

    fig = plt.figure(figsize=(15*x_scale, vec_size * 5))
    for dim, seq in enumerate(sequences):
        plt.subplot(vec_size, 1, dim + 1)
        plt.plot(seq)
        if subplot_labels is None:
            plt.title("dim {}".format(dim))
        else:
            plt.title("dim {}".format(subplot_labels[dim]))

        plt.xlim(x_min if x_min is not None else 0,
                x_max if x_max is not None else fseq.shape[0])
        plt.ylim(y_min, y_max)

        if x_label is not None:
            plt.xlabel(x_label)
        if y_label is not None:
            plt.ylabel(y_label)

    plt.draw()
    if save_filepath:
        fig.savefig(save_filepath)
    else:
        plt.show()


def main(input_filepath, vector_size, feature_indices, save_filepath, x_label, y_label, verbose, y_min=None, y_max=None, x_min=None, x_max=None, x_scale=1.0, *, logger=None):
    set_level(verbose)
    fseq = load_fseq(path=input_filepath, vec_size=vector_size, feature_range=feature_indices)
    fseq2fig(fseq=fseq,
             x_label=" ".join(x_label) if x_label is not None else None,
             y_label=" ".join(y_label) if y_label is not None else None,
             save_filepath=save_filepath,
             y_min=y_min,
             y_max=y_max,
             x_min=x_min,
             x_max=x_max,
             x_scale=x_scale,
             subplot_labels=feature_indices)


if __name__ == "__main__":
    args = docopt(__doc__)

    main(input_filepath=os.path.abspath(os.path.expanduser(args['<input_filepath>'])),
         x_label=args['--x-label'].split(
             ',') if args['--x-label'] is not None else None,
         y_label=args['--y-label'].split(
             ',') if args['--y-label'] is not None else None,
         save_filepath=os.path.abspath(
             args['--save-filepath']) if args['--save-filepath'] is not None else None,
         vector_size=int(args['--vector-size']),
         feature_indices=[int(f) for f in args['--feature-indices'].split(',')],
         x_scale=float(args['--x-scale']),
         verbose=int(args['--verbose']))
