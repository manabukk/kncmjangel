# -*- coding: utf-8 -*-

import torch
from torch import nn
from torch.nn import functional as F

from model.model import KNCNNModel
from model.activations import MyActivations


def get_VTLNLayer(input_dim, nb_layers, nb_units):
    if nb_layers == 1:
        layers = [nn.Linear(input_dim, 1)]
    else:
        layers = [nn.Linear(nb_units, nb_units) if i > 0 else nn.Linear(
            input_dim, nb_units) for i in range(nb_layers - 1)]
        layers.append(nn.Linear(nb_units, 1))
    return layers


def get_batchA(batch_alpha, dim):
    import math
    batch_size = batch_alpha.size(0)
    A = []
    for i in range(dim):
        for j in range(dim):
            coeff = 1. / math.factorial(j)
            m0 = max([0, j-i])
            aij = 0.
            for m in range(m0, j+1):
                first_term = math.factorial(
                    j+1) // (math.factorial(j+1 - m) * math.factorial(m)) if j+1 >= m else 0
                second_term = math.factorial(m+i) / math.factorial(m+i-j)
                second_term *= coeff
                third_term = (-1 * batch_alpha)**(m+i-j) * batch_alpha**m
                each_elem = first_term * second_term * third_term
                aij += each_elem
            A.append(aij)
    A = torch.cat(A).reshape(dim, dim, batch_size)
    A = A.transpose(1, 2).transpose(0, 1)
    return A


class NNVTLN(KNCNNModel):

    def __init__(self,
                 nb_layers,
                 nb_units,
                 activfn,
                 input_dim,
                 output_dim,
                 lim_scale=0.2
                 ):
        super(NNVTLN, self).__init__()

        self.vtln_fc = nn.ModuleList(
            get_VTLNLayer(input_dim, nb_layers, nb_units))

        self.activfn = MyActivations(activfn)

        self.input_dim = input_dim
        self.output_dim = output_dim

        self.lim_scale = lim_scale

        self._prepare_A(self.output_dim)

    def _prepare_A(self, dim):
        import math
        import numpy as np
        coeff_matrix = torch.Tensor(
            [[1. / math.factorial(j) for j in range(dim)] for i in range(dim)]).unsqueeze(2)

        A_base = np.zeros(shape=[dim, dim, dim+1])
        alpha_power1_matrix = np.zeros(shape=[dim, dim, dim+1])
        alpha_power2_matrix = np.zeros(shape=[dim, dim, dim+1])

        for i in range(dim):
            for j in range(dim):
                m0 = max([0, j-i])
                aij = 0.
                for m in range(m0, j+1):
                    first_term = math.factorial(
                        j+1) // (math.factorial(j+1 - m) * math.factorial(m)) if j+1 >= m else 0
                    second_term = math.factorial(m+i) / math.factorial(m+i-j)
                    each_elem = first_term * second_term
                    A_base[i][j][m] = each_elem
                    alpha_power1_matrix[i][j][m] = m + i - j
                    alpha_power2_matrix[i][j][m] = m
        A_base = torch.from_numpy(A_base).unsqueeze(3)
        alpha_power1_matrix = torch.from_numpy(
            alpha_power1_matrix).unsqueeze(3)
        alpha_power2_matrix = torch.from_numpy(
            alpha_power2_matrix).unsqueeze(3)

        self.params_for_calcA = nn.ParameterDict({'coeff': nn.Parameter(coeff_matrix, requires_grad=False),
                                                  'A_base': nn.Parameter(A_base, requires_grad=False),
                                                  'alpha_power1': nn.Parameter(alpha_power1_matrix, requires_grad=False),
                                                  'alpha_power2': nn.Parameter(alpha_power2_matrix, requires_grad=False)})

    def get_batchA(self, batch_alpha):
        dim = self.output_dim
        alpha_power1_matrix = self.params_for_calcA['alpha_power1']
        alpha_power2_matrix = self.params_for_calcA['alpha_power2']
        A_base = self.params_for_calcA['A_base']
        coeff_matrix = self.params_for_calcA['coeff']

        batch_alpha_like_A = batch_alpha.squeeze(1).repeat(dim, dim, dim+1, 1)

        third_term = (-1.0 * batch_alpha_like_A) ** alpha_power1_matrix * \
            batch_alpha_like_A ** alpha_power2_matrix

        element_wise_product = A_base * third_term

        no_scale_A = element_wise_product.sum(dim=2)

        A = coeff_matrix * no_scale_A

        A = A.transpose(1, 2).transpose(0, 1)

        return A

    def forward(self, x):
        h = self.activfn(self.vtln_fc[0](x))
        for i, fc in enumerate(self.vtln_fc[1:-1]):
            h = self.activfn(fc(h))
        alpha = torch.tanh(self.vtln_fc[-1](h))

        alpha = self.lim_scale * alpha

        A = self.get_batchA(alpha)
        # A = get_batchA(alpha, self.output_dim)


        x = x.unsqueeze(2)
        predict = torch.matmul(A, x)

        predict = predict.squeeze()

        return predict

    def optimize(self, X, Y, optimizer, loss_fn):
        optimizer.zero_grad()
        predict_Y = self(X)
        loss = self.loss_fn(predict_Y, Y, loss_fn)
        loss.backward()
        optimizer.step()
        return loss.item()

    def loss_fn(self, predict_Y, Y, loss_fn):
        return loss_fn(predict_Y, Y)

    def cp(self, x):
        h = self.activfn(self.vtln_fc[0](x))
        for i, fc in enumerate(self.vtln_fc[1:-1]):
            h = self.activfn(fc(h))
        alpha = torch.tanh(self.vtln_fc[-1](h))

        alpha = self.lim_scale * alpha

        A = self.get_batchA(alpha)

        return {"A": A, "alpha": alpha}


class NNVTLNwBias(KNCNNModel):

    def __init__(self,
                 nb_shared_layers,
                 nb_shared_units,
                 nb_vtln_layers,
                 nb_vtln_units,
                 nb_bias_layers,
                 nb_bias_units,
                 activfn,
                 output_activfn,
                 input_dim,
                 output_dim,
                 latent_dim=None,
                 lim_scale=0.2
                 ):
        super(NNVTLNwBias, self).__init__()

        shared_fc = [nn.Linear(nb_shared_units, nb_shared_units) if i > 0 else nn.Linear(
            input_dim, nb_shared_units) for i in range(nb_shared_layers - 1)]
        if latent_dim is None:
            latent_dim = nb_shared_units
        shared_fc.append(nn.Linear(nb_shared_units, latent_dim))
        self.shared_fc = nn.ModuleList(shared_fc)

        self.vtln_fc = nn.ModuleList(
            get_VTLNLayer(latent_dim, nb_vtln_layers, nb_vtln_units))

        if nb_bias_layers == 1:
            bias_fc = [nn.Linear(latent_dim, output_dim)]
        else:
            bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                latent_dim, nb_bias_units) for i in range(nb_bias_layers - 1)]
            bias_fc.append(nn.Linear(nb_bias_units, output_dim))

        self.bias_fc = nn.ModuleList(bias_fc)

        self.activfn = MyActivations(activfn)
        self.output_activfn = MyActivations(output_activfn)

        self.input_dim = input_dim
        self.output_dim = output_dim
        self.latent_dim = latent_dim

        self.lim_scale = lim_scale

        self._prepare_A(self.output_dim)

    def _prepare_A(self, dim):
        import math
        import numpy as np
        coeff_matrix = torch.Tensor(
            [[1. / math.factorial(j) for j in range(dim)] for i in range(dim)]).unsqueeze(2)

        A_base = np.zeros(shape=[dim, dim, dim+1])
        alpha_power1_matrix = np.zeros(shape=[dim, dim, dim+1])
        alpha_power2_matrix = np.zeros(shape=[dim, dim, dim+1])

        for i in range(dim):
            for j in range(dim):
                m0 = max([0, j-i])
                aij = 0.
                for m in range(m0, j+1):
                    first_term = math.factorial(
                        j+1) // (math.factorial(j+1 - m) * math.factorial(m)) if j+1 >= m else 0
                    second_term = math.factorial(m+i) / math.factorial(m+i-j)
                    each_elem = first_term * second_term
                    A_base[i][j][m] = each_elem
                    alpha_power1_matrix[i][j][m] = m + i - j
                    alpha_power2_matrix[i][j][m] = m
        A_base = torch.from_numpy(A_base).unsqueeze(3)
        alpha_power1_matrix = torch.from_numpy(
            alpha_power1_matrix).unsqueeze(3)
        alpha_power2_matrix = torch.from_numpy(
            alpha_power2_matrix).unsqueeze(3)

        self.params_for_calcA = nn.ParameterDict({'coeff': nn.Parameter(coeff_matrix, requires_grad=False),
                                 'A_base': nn.Parameter(A_base, requires_grad=False),
                                 'alpha_power1': nn.Parameter(alpha_power1_matrix, requires_grad=False),
                                               'alpha_power2': nn.Parameter(alpha_power2_matrix, requires_grad=False)})

    def get_batchA(self, batch_alpha):
        dim = self.output_dim
        alpha_power1_matrix = self.params_for_calcA['alpha_power1']
        alpha_power2_matrix = self.params_for_calcA['alpha_power2']
        A_base = self.params_for_calcA['A_base']
        coeff_matrix = self.params_for_calcA['coeff']

        batch_alpha_like_A = batch_alpha.squeeze(1).repeat(dim, dim, dim+1, 1)

        third_term = (-1.0 * batch_alpha_like_A) ** alpha_power1_matrix * batch_alpha_like_A ** alpha_power2_matrix

        element_wise_product = A_base * third_term

        no_scale_A = element_wise_product.sum(dim=2)

        A = coeff_matrix * no_scale_A

        A = A.transpose(1, 2).transpose(0, 1)

        return A

    def forward(self, x):
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](x))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            z = h
        else:
            z = x

        h = self.activfn(self.vtln_fc[0](z))
        for i, fc in enumerate(self.vtln_fc[1:-1]):
            h = self.activfn(fc(h))
        alpha = torch.tanh(self.vtln_fc[-1](h))

        alpha = self.lim_scale * alpha

        # A = get_batchA(alpha, self.output_dim)
        A = self.get_batchA(alpha)

        # bias-subnetwork
        if len(self.bias_fc) == 1:
            bias = self.output_activfn(self.bias_fc[0](z))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-1]):
                h = self.activfn(fc(h))
            bias = self.output_activfn(self.bias_fc[-1](h))

        x = x.unsqueeze(2)
        predict = torch.matmul(A, x)

        bias = bias.unsqueeze(2)

        predict = predict + bias

        predict = predict.squeeze()

        return predict

    def optimize(self, X, Y, optimizer, loss_fn):
        optimizer.zero_grad()
        predict_Y = self(X)
        loss = self.loss_fn(predict_Y, Y, loss_fn)
        loss.backward()
        optimizer.step()
        return loss.item()

    def loss_fn(self, predict_Y, Y, loss_fn):
        return loss_fn(predict_Y, Y)

    def cp(self, x):
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](x))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            z = h
        else:
            z = x

        h = self.activfn(self.vtln_fc[0](z))
        for i, fc in enumerate(self.vtln_fc[1:-1]):
            h = self.activfn(fc(h))
        alpha = torch.tanh(self.vtln_fc[-1](h))

        alpha = self.lim_scale * alpha

        # A = get_batchA(alpha, self.output_dim)
        A = self.get_batchA(alpha)

        # bias-subnetwork
        if len(self.bias_fc) == 1:
            bias = self.output_activfn(self.bias_fc[0](z))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-1]):
                h = self.activfn(fc(h))
            bias = self.output_activfn(self.bias_fc[-1](h))

        x = x.unsqueeze(2)
        predict = torch.matmul(A, x)

        bias = bias.unsqueeze(2)

        return {"A": A, "b": bias, "alpha": alpha}


class NNVTLNwAux(KNCNNModel):

    def __init__(self,
                 nb_layers,
                 nb_units,
                 activfn,
                 input_dim,
                 output_dim,
                 lim_scale=0.2,
                 aux_dim=2,
                 nb_aux_layers=1,
                 nb_aux_units=None,
                 not_input=False,
                 ):
        super(NNVTLNwAux, self).__init__()

        self.not_input = not_input
        if not_input:
            self.vtln_fc = nn.ModuleList(
            get_VTLNLayer(aux_dim, nb_layers, nb_units))
        else:
            self.vtln_fc = nn.ModuleList(
                get_VTLNLayer(input_dim+aux_dim, nb_layers, nb_units))

        self.aux_dim = aux_dim

        self.activfn = MyActivations(activfn)

        self.input_dim = input_dim
        self.output_dim = output_dim

        self.lim_scale = lim_scale

        self._prepare_A(self.output_dim)

    def _prepare_A(self, dim):
        import math
        import numpy as np
        coeff_matrix = torch.Tensor(
            [[1. / math.factorial(j) for j in range(dim)] for i in range(dim)]).unsqueeze(2)

        A_base = np.zeros(shape=[dim, dim, dim+1])
        alpha_power1_matrix = np.zeros(shape=[dim, dim, dim+1])
        alpha_power2_matrix = np.zeros(shape=[dim, dim, dim+1])

        for i in range(dim):
            for j in range(dim):
                m0 = max([0, j-i])
                aij = 0.
                for m in range(m0, j+1):
                    first_term = math.factorial(
                        j+1) // (math.factorial(j+1 - m) * math.factorial(m)) if j+1 >= m else 0
                    second_term = math.factorial(m+i) / math.factorial(m+i-j)
                    each_elem = first_term * second_term
                    A_base[i][j][m] = each_elem
                    alpha_power1_matrix[i][j][m] = m + i - j
                    alpha_power2_matrix[i][j][m] = m
        A_base = torch.from_numpy(A_base).unsqueeze(3)
        alpha_power1_matrix = torch.from_numpy(
            alpha_power1_matrix).unsqueeze(3)
        alpha_power2_matrix = torch.from_numpy(
            alpha_power2_matrix).unsqueeze(3)

        self.params_for_calcA = nn.ParameterDict({'coeff': nn.Parameter(coeff_matrix, requires_grad=False),
                                                  'A_base': nn.Parameter(A_base, requires_grad=False),
                                                  'alpha_power1': nn.Parameter(alpha_power1_matrix, requires_grad=False),
                                                  'alpha_power2': nn.Parameter(alpha_power2_matrix, requires_grad=False)})

    def get_batchA(self, batch_alpha):
        dim = self.output_dim
        alpha_power1_matrix = self.params_for_calcA['alpha_power1']
        alpha_power2_matrix = self.params_for_calcA['alpha_power2']
        A_base = self.params_for_calcA['A_base']
        coeff_matrix = self.params_for_calcA['coeff']

        batch_alpha_like_A = batch_alpha.squeeze(1).repeat(dim, dim, dim+1, 1)

        third_term = (-1.0 * batch_alpha_like_A) ** alpha_power1_matrix * \
            batch_alpha_like_A ** alpha_power2_matrix

        element_wise_product = A_base * third_term

        no_scale_A = element_wise_product.sum(dim=2)

        A = coeff_matrix * no_scale_A

        A = A.transpose(1, 2).transpose(0, 1)

        return A

    def forward(self, x):
        concat_x = x
        aux = concat_x[:, -self.aux_dim:]
        x =  concat_x[:, :-self.aux_dim]
        if self.not_input:
            h = self.activfn(self.vtln_fc[0](aux))
        else:
            h = self.activfn(self.vtln_fc[0](concat_x))
        for i, fc in enumerate(self.vtln_fc[1:-1]):
            h = self.activfn(fc(h))
        alpha = torch.tanh(self.vtln_fc[-1](h))

        alpha = self.lim_scale * alpha

        A = self.get_batchA(alpha)
        # A = get_batchA(alpha, self.output_dim)


        x = x.unsqueeze(2)
        predict = torch.matmul(A, x)

        predict = predict.squeeze()

        return predict

    def optimize(self, X, Y, optimizer, loss_fn):
        optimizer.zero_grad()
        predict_Y = self(X)
        loss = self.loss_fn(predict_Y, Y, loss_fn)
        loss.backward()
        optimizer.step()
        return loss.item()

    def loss_fn(self, predict_Y, Y, loss_fn):
        return loss_fn(predict_Y, Y)

    def cp(self, x):
        concat_x = x
        aux = concat_x[:, -self.aux_dim:]
        x =  concat_x[:, :-self.aux_dim]
        if self.not_input:
            h = self.activfn(self.vtln_fc[0](aux))
        else:
            h = self.activfn(self.vtln_fc[0](concat_x))
            
        for i, fc in enumerate(self.vtln_fc[1:-1]):
            h = self.activfn(fc(h))
        alpha = torch.tanh(self.vtln_fc[-1](h))

        alpha = self.lim_scale * alpha

        A = self.get_batchA(alpha)

        return {"A": A, "alpha": alpha}


class NNVTLNwtiBias(KNCNNModel):

    def __init__(self,
                 nb_layers,
                 nb_units,
                 activfn,
                 input_dim,
                 output_dim,
                 src_bias_fp,
                 tgt_bias_fp,
                 lim_scale=0.2
                 ):
        super(NNVTLNwtiBias, self).__init__()

        self.vtln_fc = nn.ModuleList(
            get_VTLNLayer(input_dim, nb_layers, nb_units))

        self.activfn = MyActivations(activfn)

        self.input_dim = input_dim
        self.output_dim = output_dim

        self.lim_scale = lim_scale

        self._prepare_A(self.output_dim)

        from misc.readwrite import load_fseq

        if torch.cuda.is_available():
            device = 'cuda'
        else:
            device = 'cpu'

        src_bias = load_fseq(path=src_bias_fp, vec_size=output_dim, feature_range=list(range(output_dim))).squeeze()
        self.src_bias = torch.from_numpy(src_bias).to(device)
        self.src_bias.requires_grad = True
        self.src_bias = nn.Parameter(self.src_bias)
        tgt_bias = load_fseq(path=tgt_bias_fp, vec_size=output_dim, feature_range=list(range(output_dim))).squeeze()
        self.tgt_bias = torch.from_numpy(tgt_bias).to(device)
        self.tgt_bias.requires_grad = True
        self.tgt_bias = nn.Parameter(self.tgt_bias)


    def _prepare_A(self, dim):
        import math
        import numpy as np
        coeff_matrix = torch.Tensor(
            [[1. / math.factorial(j) for j in range(dim)] for i in range(dim)]).unsqueeze(2)

        A_base = np.zeros(shape=[dim, dim, dim+1])
        alpha_power1_matrix = np.zeros(shape=[dim, dim, dim+1])
        alpha_power2_matrix = np.zeros(shape=[dim, dim, dim+1])

        for i in range(dim):
            for j in range(dim):
                m0 = max([0, j-i])
                aij = 0.
                for m in range(m0, j+1):
                    first_term = math.factorial(
                        j+1) // (math.factorial(j+1 - m) * math.factorial(m)) if j+1 >= m else 0
                    second_term = math.factorial(m+i) / math.factorial(m+i-j)
                    each_elem = first_term * second_term
                    A_base[i][j][m] = each_elem
                    alpha_power1_matrix[i][j][m] = m + i - j
                    alpha_power2_matrix[i][j][m] = m
        A_base = torch.from_numpy(A_base).unsqueeze(3)
        alpha_power1_matrix = torch.from_numpy(
            alpha_power1_matrix).unsqueeze(3)
        alpha_power2_matrix = torch.from_numpy(
            alpha_power2_matrix).unsqueeze(3)

        self.params_for_calcA = nn.ParameterDict({'coeff': nn.Parameter(coeff_matrix, requires_grad=False),
                                                  'A_base': nn.Parameter(A_base, requires_grad=False),
                                                  'alpha_power1': nn.Parameter(alpha_power1_matrix, requires_grad=False),
                                                  'alpha_power2': nn.Parameter(alpha_power2_matrix, requires_grad=False)})

    def get_batchA(self, batch_alpha):
        dim = self.output_dim
        alpha_power1_matrix = self.params_for_calcA['alpha_power1']
        alpha_power2_matrix = self.params_for_calcA['alpha_power2']
        A_base = self.params_for_calcA['A_base']
        coeff_matrix = self.params_for_calcA['coeff']

        batch_alpha_like_A = batch_alpha.squeeze(1).repeat(dim, dim, dim+1, 1)

        third_term = (-1.0 * batch_alpha_like_A) ** alpha_power1_matrix * \
            batch_alpha_like_A ** alpha_power2_matrix

        element_wise_product = A_base * third_term

        no_scale_A = element_wise_product.sum(dim=2)

        A = coeff_matrix * no_scale_A

        A = A.transpose(1, 2).transpose(0, 1)

        return A

    def forward(self, x):
        h = self.activfn(self.vtln_fc[0](x))
        for i, fc in enumerate(self.vtln_fc[1:-1]):
            h = self.activfn(fc(h))
        alpha = torch.tanh(self.vtln_fc[-1](h))

        alpha = self.lim_scale * alpha

        A = self.get_batchA(alpha)
        # A = get_batchA(alpha, self.output_dim)


        x = x.unsqueeze(2)

        predict = torch.matmul(A, x - self.src_bias.unsqueeze(1)) + self.tgt_bias.unsqueeze(1)

        predict = predict.squeeze()

        return predict

    def optimize(self, X, Y, optimizer, loss_fn):
        optimizer.zero_grad()
        predict_Y = self(X)
        loss = self.loss_fn(predict_Y, Y, loss_fn)
        loss.backward()
        optimizer.step()
        return loss.item()

    def loss_fn(self, predict_Y, Y, loss_fn):
        return loss_fn(predict_Y, Y)

    def cp(self, x):
        h = self.activfn(self.vtln_fc[0](x))
        for i, fc in enumerate(self.vtln_fc[1:-1]):
            h = self.activfn(fc(h))
        alpha = torch.tanh(self.vtln_fc[-1](h))

        alpha = self.lim_scale * alpha

        A = self.get_batchA(alpha)

        return {"A": A, "alpha": alpha, "src_bias": self.src_bias.unsqueeze(0).expand(x.size(0), self.output_dim), "tgt_bias": self.tgt_bias.unsqueeze(0).expand(x.size(0), self.output_dim)}
