# -*- coding: utf-8 -*-
"""Overview:
    Hoge.

Usage:
    mergemceps <original_mcep_file> <idx_file> <mcep_file> <output_path> [-d <x_vector_size>] [-f <x_feature_range>] [-e <y_vector_size>] [-g <y_feature_range>] [-v...]
    mergemceps -h

Options:
    <original_mcep_file>                      hoge.
    <idx_file>                                hoge.
    <mcep_file>                               hoge.
    <output_path>                             Path to output idx file.
    -d --x-vector-size <x_vector_size>        Vector size [default: 25].
    -f --x-feature-range <x_feature_range>    Feature range [default: 1,25].
    -e --y-vector-size <y_vector_size>        Vector size [default: 24].
    -g --y-feature-range <y_feature_range>    Feature range [default: 0,24].
    -v --verbose                              Show in detail.
    -h --help                                 Show this message.
"""

from docopt import docopt
import sys
import os

from misc.readwrite import load_fseq, load_alignment, write_fseq
from misc.util import logger, set_level

import numpy as np


def merge_mceps_with_idx(mcep, idxs, original_mcep):
    original_mcep[idxs, :] = mcep
    return original_mcep

def main(mfp, mfp_origin, ifp, ofp, xd, yd, xfrange, yfrange, verbose):
    set_level(verbose)

    logger.debug("File path to original mcep: {}".format(mfp_origin))
    original_X = load_fseq(path=mfp_origin, vec_size=xd, feature_range=list(range(xfrange[0], xfrange[1])))
    logger.debug("Original mcep shape: {}".format(original_X.shape))

    logger.debug("File path to mcep applied VAD: {}".format(mfp))
    X = load_fseq(path=mfp, vec_size=yd, feature_range=list(range(yfrange[0], yfrange[1])))
    logger.debug("Mcep applied VAD shape: {}".format(X.shape))

    logger.debug("Idx file pah: {}".format(ifp))
    idxs = load_alignment(path=ifp)
    logger.debug("Size of idxs: {}".format(len(idxs)))

    new_X = merge_mceps_with_idx(X, idxs, original_X)
    logger.debug("Path to the new mcept: {}".format(ofp))
    write_fseq(path=ofp, fseq=new_X)
    


if __name__ == '__main__':
    args = docopt(__doc__)

    main(
        mfp=os.path.abspath(args['<mcep_file>']),
        mfp_origin=os.path.abspath(args['<original_mcep_file>']),
         ifp=os.path.abspath(args['<idx_file>']),
         ofp=os.path.abspath(args['<output_path>']),
         xd=int(args['--x-vector-size']),
         yd=int(args['--y-vector-size']),
         xfrange=[int(f) for f in args['--x-feature-range'].split(',')],
         yfrange=[int(f) for f in args['--y-feature-range'].split(',')],
         verbose=int(args['--verbose']))
