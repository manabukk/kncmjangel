# -*- coding=utf-8 -*-
"""Overview:
    Computing outputted parameters of DNN e.g. A and b in DNN-TVLT.

Usage:
    cpDNN <model_filepath> <config_modulepath> -s <smcep_filepath> -o <output_prefix> [-d <vector_size>] [-f <feature_range>] [-v...]
    cpDNN -h

Options:
    <model_filepath>                    Path.
    <config_modulepath>                 Path.
    <smcep_filepath>                    hoge.
    -o --output-prefix <output_prefix>  hoge.
    -d --vector-size <vector_size>      Vector size [default: 25].
    -f --feature-range <feature_range>  Feature range [default: 1,25].
    -v --verbose                        Show in detail.
    -h --help
"""
from docopt import docopt
import os
import sys
sys.path.append(os.path.curdir)

from comet_ml import Optimizer
import torch

from model import tvlt
from misc.readwrite import load_matfile, load_fseq, write_fseq
from misc.util import set_level, logger, mkdir_p, dynamic_import



def load_model_with_config(model_filepath, config_modulepath, device):
    ExpConfig = dynamic_import(config_modulepath)()
    model_configure = ExpConfig.model_configure
    project_name = ExpConfig.project_name
    opt = Optimizer(model_configure, project_name=project_name,
                    experiment_class="OfflineExperiment", offline_directory="./temp")
    experiment = next(opt.get_experiments())
    model = ExpConfig.get_model(experiment)
    model.load_state_dict(torch.load(model_filepath, map_location=torch.device(device)))
    model.eval()
    model.double()
    model.to(device)
    return model


def main(model_filepath, config_modulepath, smcep_filepath, output_prefix, dim, frange, verbose):
    set_level(verbose)

    logger.debug("Path to the DNN model: {}".format(model_filepath))

    if torch.cuda.is_available():
        device = 'cuda'
    else:
        device = 'cpu'

    model = load_model_with_config(model_filepath, config_modulepath, device)

    logger.debug("Path to source mcep: {}".format(smcep_filepath))

    smcep = load_fseq(smcep_filepath, dim, list(range(frange[0], frange[1])))

    logger.debug("Shape of smcep: {}".format(smcep.shape))

    # A, b = cparamTVLT(model, torch.from_numpy(smcep))
    params = model.cp(torch.from_numpy(smcep).to(device))

    mkdir_p(os.path.dirname(output_prefix))

    for key, value in params.items():
        p = value.cpu().detach().numpy()
        logger.debug("Shape of {}: {}".format(key, p.shape))
        write_fseq(path="{}.{}".format(output_prefix, key), fseq=p.reshape(p.shape[0], -1))

    return


if __name__ == "__main__":
    args = docopt(__doc__)
    main(model_filepath=os.path.abspath(args['<model_filepath>']),
         config_modulepath=args['<config_modulepath>'],
         smcep_filepath=os.path.abspath(args['<smcep_filepath>']),
         output_prefix=os.path.abspath(args['--output-prefix']),
         dim=int(args['--vector-size']),
         frange=[int(f) for f in args['--feature-range'].split(',')],
         verbose=int(args['--verbose']))
