# -*- coding=utf-8 -*-
"""Overview:
    Scaling raw file to float32 (e.g. for reaper-based f0 estimation).

Usage:
    scalefloat <input_raw_fp> <output_raw_fp> [-v...]
    scalefloat -h

Options:
    <input_raw_fp>                        Path.
    <output_raw_fp>                       Path.
    -v --verbose                          Show in detail.
    -h --help                             Show this message.
"""
from docopt import docopt
import os, sys
sys.path.append(os.path.curdir)
from misc.readwrite import load_fseq, write_fseq

def main(input_raw_fp, output_raw_fp, verbose):
    raw = load_fseq(path=input_raw_fp, vec_size=1, feature_range=[0])
    assert raw.min() > -1.0 and raw.max() < 1.0
    raw_scaled_for_reaper = raw * 65535
    write_fseq(path=output_raw_fp, fseq=raw_scaled_for_reaper)

if __name__ == "__main__":
    args = docopt(__doc__)
    main(input_raw_fp=os.path.abspath(args['<input_raw_fp>']),
         output_raw_fp=args['<output_raw_fp>'],
         verbose=int(args['--verbose']))
