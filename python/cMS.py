# -*- coding: utf-8 -*-
"""Overview:
    Compute modulation spectrum [dB].

Usage:
    cMS <feature_file> <output_file> [-d <vector_size>] [-f <feature_range>] [-l <tap_length>] [-v...]
    cMS -h

Options:
    <feature_file>                            hoge.
    <output_file>                             hoge.
    -d --vector-size <vector_size>            Vector size [default: 25].
    -f --feature-range <feature_range>        Feature range [default: 1,25].
    -l --tap-length <tap_length>              Tap size [default: 2048].
    -v --verbose                              Show in detail.
    -h --help                                 Show this message.
"""

from docopt import docopt
import sys
import os

from misc.readwrite import load_fseq, load_alignment, write_fseq
from misc.util import logger, set_level

import numpy as np


def main(feature_fp, output_fp, d, frange, l, verbose):
    set_level(verbose)

    logger.debug("Feature file path: {}".format(feature_fp))
    X = load_fseq(path=feature_fp, vec_size=d, feature_range=list(range(frange[0], frange[1])))
    logger.debug("Feature sequence shape: {}".format(X.shape))

    dft = np.fft.fft(a=X, n=l, axis=0)

    ms = (dft * dft.conjugate()).real

    ms = ms[:int(l / 2) +1, :]

    ms = np.log10(ms)
    # ms = 10 / np.log(10) * np.sqrt(2) * ms

    logger.debug("Path to the MS file: {}".format(output_fp))
    logger.debug("MS shape: {}".format(ms.shape))
    write_fseq(path=output_fp, fseq=ms)


if __name__ == '__main__':
    args = docopt(__doc__)

    main(
         feature_fp=os.path.abspath(args['<feature_file>']),
         output_fp=os.path.abspath(args['<output_file>']),
         d=int(args['--vector-size']),
         l=int(args['--tap-length']),
         frange=[int(f) for f in args['--feature-range'].split(',')],
         verbose=int(args['--verbose']))
