# -*- coding=utf-8 -*-
"""Overview:
    Generating figures from a time sequence of matrices (for generating a video).

Usage:
    matrices2figs <seq_filepath> <output_prefix> [-d <matrix_shape>] [-v...]
    matrices2figs -h

Options:
    <seq_filepath>                    Path.
    <output_prefix>                   Path.
    -d --matrix-shape <matrix_shape>  Shape of a matrix [default: 24,24].
    -v --verbose                      Show in detail.
    -h --help                         Show this message.
"""

from docopt import docopt
import os
import sys

from misc.util import set_level, logger, mkdir_p
from misc.readwrite import load_fseq
from misc.figure import get_figm0_from_2darray

def get_0pad_filenames(T):
    import numpy as np
    max_order = int(np.log10(T + 1))
    filenames = []
    for t in range(T):
        order = int(np.log10(t + 1))
        filename = "{prefix}" + "0" * \
            (max_order - order) + "{}".format(t+1) + "{suffix}"
        filenames.append(filename)
    return filenames


def seq2figs(seq, dir_path, vmin=-1., vmax=1.):
    # seq: (T, D, E) should be 3-d numpy array
    T = seq.shape[0]

    _filenames = get_0pad_filenames(T)

    for frame, _filename in zip(seq, _filenames):
        fig = get_figm0_from_2darray(
            frame, vmin=vmin, vmax=vmax, vlogscale=None, trans_y=True)
        fig.savefig(_filename.format(prefix=dir_path + "/",
                                     suffix=".png"), bbox_inches="tight")


def main(seq_filepath, output_prefix, matrix_shape, verbose):
    set_level(verbose)

    logger.debug("Path to the file: {}".format(seq_filepath))

    seq_mat = load_fseq(
        seq_filepath, matrix_shape[0]*matrix_shape[1], list(range(matrix_shape[0]*matrix_shape[1])))
    seq_mat = seq_mat.reshape(
        seq_mat.shape[0], matrix_shape[0], matrix_shape[1])

    logger.debug("Shape of a sequence: {}".format(seq_mat.shape))

    dir_path = os.path.splitext(output_prefix)[0]
    mkdir_p(dir_path)
    logger.debug("A directory including figures will be '{}'".format(dir_path))

    seq2figs(seq=seq_mat, dir_path=dir_path)

if __name__ == "__main__":
    args = docopt(__doc__)

    main(seq_filepath=os.path.abspath(args['<seq_filepath>']),
         output_prefix=os.path.abspath(args['<output_prefix>']),
         matrix_shape=[int(f) for f in args['--matrix-shape'].split(',')],
         verbose=int(args['--verbose']))
