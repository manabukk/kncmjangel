from abc import ABCMeta, abstractmethod
from collections import OrderedDict
import json

class Data(object):
    __metaclass__ = ABCMeta

    def __init__(self):
        super(Data, self).__init__()
        self.source = None

    @abstractmethod
    def _load(self):
        pass

    def get(self):
        pass

    def to_json(self):
        save_dict = OrderedDict()
        for k, _v in self.__dict__.items():
            if k not in ('data',):
                save_dict[k] = _v
        save_dict['CLASS'] = self.__class__.__name__
        return save_dict

    def from_dict(self, param_dict):
        self.__dict__.update(param_dict)
        return self

    def save(self, path):
        save_dict = self.to_json()
        with open(path, 'w') as f:
            json.dump(save_dict, f)
        return path, save_dict

    def load_from_json(self, path):
        with open(path, 'r') as f:
            param_dict = json.load(f)
        self.from_dict(param_dict)
        return self

    def show(self):
        pass
