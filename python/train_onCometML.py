# -*- coding=utf-8 -*-
"""Overview:
    Training NNs on comet_ml.

Usage:
    train <config_modulepath> [-d <offline_directory>] [-v...]
    train -h

Options:
    <config_modulepath>                         Class of ExpConfig (module path) e.g. 'script.tvlt.ExpConfig'.
    -d --offline-directory <offline_directory>  hoge.
    -v --verbose                                Show in detail.
    -h --help                                   Show this message.
"""
from docopt import docopt
import os
from misc.util import set_level, logger

def main(config_modulepath, offline_directory, verbose):
    set_level(verbose)

    from comet_ml import Optimizer
    import torch
    if torch.cuda.is_available():
        device = 'cuda:0'
    else:
        device = 'cpu'
    import sys

    from misc.readwrite import load_fseq, write_fseq
    from torch import nn
    from data.dataset.dataset import load_dataset
    import ignite
    from ignite.engine import Events, create_supervised_trainer, create_supervised_evaluator
    from ignite.contrib.handlers import CustomPeriodicEvent
    from ignite.handlers import EarlyStopping
    from misc.cdist import compute_melcd
    from misc.util import dynamic_import
    from torch.optim.lr_scheduler import LambdaLR

    from model.bidirection import BiDirectionalModel  # TODO:


    sys.path.append(os.path.curdir)
    ExpConfig = dynamic_import(config_modulepath)()

    # TODO: log audio の設計
    # def sample_audio(model, experiment, step):
    #     import subprocess
    #     import numpy as np
    #     import pyworld as pw
    #     model.eval()
    #     sentences = ExpConfig.sample_id
    #     nb_bins = ExpConfig.nb_bins
    #     sampling_rate = ExpConfig.sampling_rate
    #     frame_shift = ExpConfig.frame_shift

    #     smcep_base_path = "/work/kotani/tvlt-vc/20200115/data/mht/mcep/{}.mcep"
    #     ap_base_path = "/work/kotani/tvlt-vc/20200115/data/mht/ap/{}.ap"
    #     f0_base_path = "/work/kotani/tvlt-vc/20200115/data/mht/f0/{}.f0"
    #     tmp_base_path = "/tmp/kotani-{}".format(experiment.get_key())
    #     cmcep_base_path = tmp_base_path + "-{}.mcep"
    #     csp_base_path = tmp_base_path + "-{}.sp"
    #     synth_base_path = tmp_base_path + "-{}.raw"
    #     wav_base_path = tmp_base_path + "-{}.wav"
    #     for sentence in sentences['train'] + sentences['valid']:
    #         smcep_path = smcep_base_path.format(sentence)
    #         smcep = load_fseq(path=smcep_path, vec_size=60,
    #                           feature_range=range(60))
    #         power = smcep[:, 0]
    #         power = power[:, None]
    #         smcep = smcep[:, 1:]
    #         smcep = torch.from_numpy(smcep).to(device)
    #         if ExpConfig.normalize_flag:
    #             smcep = ExpConfig.etc['norm_funcs']['x'].normalize(smcep)

    #         cmcep = model(smcep)

    #         if ExpConfig.normalize_flag:
    #             cmcep = ExpConfig.etc['norm_funcs']['y'].denormalize(cmcep)

    #         cmcep = cmcep.detach().cpu().numpy()
    #         cmcep_path = cmcep_base_path.format(sentence)
    #         write_fseq(path=cmcep_path, fseq=np.concatenate(
    #             [power, cmcep], axis=1))
    #         csp_path = csp_base_path.format(sentence)

    #         f0_filepath = f0_base_path.format(sentence)
    #         ap_filepath = ap_base_path.format(sentence)
    #         synth_path = synth_base_path.format(sentence)
    #         wav_path = wav_base_path.format(sentence)

    #         cmd = "/opt/SPTK/bin/mgc2sp -a 0.42 -m 24 -l 1024 -o 3 {} > {}".format(
    #             cmcep_path, csp_path)
    #         subprocess.run(cmd, shell=True)

    #         cmd = "/work/kotani/kncmjAngel/bin/worldsynthesis {} -f {} -a {} -s {} -S {} -r {} -b {}".format(
    #             synth_path,
    #             f0_filepath,
    #             ap_filepath,
    #             csp_path,
    #             frame_shift,
    #             sampling_rate,
    #             nb_bins)
    #         subprocess.run(cmd, shell=True)

    #         cmd = "sox -r {} -e floating-point -b 32 {} {}".format(
    #             sampling_rate, synth_path, wav_path)
    #         subprocess.run(cmd, shell=True)

    #         experiment.log_audio(audio_data=wav_path, file_name="{}".format(
    #             sentence), step=step)

    #         cmd = "rm {}*".format(tmp_base_path)
    #         subprocess.run(cmd, shell=True)

    def fit(experiment):
        exp_name = ExpConfig.get_exp_name(experiment)
        experiment.set_name(exp_name)
        exp_tags = ExpConfig.get_exp_tags(experiment)
        experiment.add_tags(exp_tags)

        log_n_iters = ExpConfig.get_log_n_iters(experiment)
        patience = ExpConfig.get_patience(experiment)
        batch_size = ExpConfig.get_batch_size(experiment)

        prepare_batch = ExpConfig.get_prepare_batch()
        score_function = ExpConfig.get_score_function()
        metrics = ExpConfig.get_metrics(experiment)

        model = ExpConfig.get_model(experiment)

        loss_fn = ExpConfig.get_loss_fn(experiment)

        learning_rate = experiment.get_parameter("learning_rate")
        optimizer = ExpConfig.optimizer(
            params=model.parameters(), lr=learning_rate)
        # scheduler_func = ExpConfig.get_scheduler_func()
        # scheduler = LambdaLR(optimizer, lr_lambda=scheduler_func)

        model.double()
        if isinstance(model, BiDirectionalModel):
            pass
        else:
            model.to(device)

        pretrained_model_path = ExpConfig.get_pretrained_model_path(experiment)
        if pretrained_model_path is not None:
            model.load_state_dict(torch.load(pretrained_model_path, map_location=torch.device(device)))

        train_dataset_path, valid_dataset_path = ExpConfig.get_dataset_path(experiment)
        train_dataset = load_dataset(train_dataset_path)
        valid_dataset = load_dataset(valid_dataset_path)

        dataset_loader = torch.utils.data.DataLoader(train_dataset,
                                                     batch_size=batch_size,
                                                     shuffle=True,
                                                     num_workers=1,
                                                     pin_memory=False)
        valid_loader = torch.utils.data.DataLoader(valid_dataset,
                                                   batch_size=batch_size,
                                                   shuffle=False,
                                                   num_workers=1,
                                                   pin_memory=False)

        # trainer = create_supervised_trainer(
        #     model, optimizer, loss_fn, device=device, prepare_batch=prepare_batch, output_transform=lambda x, y, y_pred, loss: {'x': x, 'y': y, 'y_pred': y_pred, 'loss': loss})
        trainer = create_supervised_trainer(
            model, optimizer, loss_fn, device=None, prepare_batch=prepare_batch, output_transform=lambda x, y, y_pred, loss: {'x': x.to(device), 'y': y.to(device), 'y_pred': y_pred, 'loss': loss})

        # evaluator = create_supervised_evaluator(model,
        #                                         metrics=metrics, device=device, prepare_batch=prepare_batch, output_transform=lambda x, y, y_pred: {'x': x, 'y': y, 'y_pred': y_pred})
        evaluator = create_supervised_evaluator(model,
                                                metrics=metrics, device=None, prepare_batch=prepare_batch, output_transform=lambda x, y, y_pred: {'x': x.to(device), 'y': y.to(device), 'y_pred': y_pred})

        handler = EarlyStopping(
            patience=patience, score_function=score_function, trainer=trainer)
        evaluator.add_event_handler(Events.COMPLETED, handler)

        cpe = CustomPeriodicEvent(n_iterations=log_n_iters)
        cpe.attach(trainer)

        valid_values = []
        valid_metric = []
        watching_metric, watching_minimum_step = ExpConfig.get_watching_metric()

        # trainer.add_event_handler(Events.ITERATION_STARTED, scheduler)

        @trainer.on(Events.ITERATION_STARTED)
        def hoge(trainer):
            ExpConfig.nb_iters = trainer.state.iteration

        @trainer.on(getattr(cpe.Events, "ITERATIONS_{}_COMPLETED".format(log_n_iters)))
        def log_training_loss(trainer):
            experiment.log_metric(
                "train_loss", trainer.state.output['loss'].detach().cpu().numpy(), step=trainer.state.iteration)

        @trainer.on(getattr(cpe.Events, "ITERATIONS_{}_COMPLETED".format(log_n_iters)))
        def log_validation_loss(trainer):
            with torch.no_grad():
                model.eval()
                evaluator.run(valid_loader)
                metrics = evaluator.state.metrics
                for metric in metrics.keys():
                    experiment.log_metric(
                        "valid_" + metric, metrics[metric], step=trainer.state.iteration)
                # TODO: 依存性がある
                if trainer.state.iteration > watching_minimum_step:
                    if metrics[watching_metric] < min(valid_metric):
                        save_model_path = ExpConfig.get_save_model_path(
                            experiment)
                        if save_model_path is not None:
                            torch.save(model.state_dict(), save_model_path + ".best")

                valid_values.append(metrics['mse'])
                valid_metric.append(metrics[watching_metric])
                

        @trainer.on(Events.STARTED)
        def log_on_start(trainer):
            with torch.no_grad():
                model.eval()
                evaluator.run(valid_loader)
                metrics = evaluator.state.metrics
                for metric in metrics.keys():
                    experiment.log_metric(
                        "valid_" + metric, metrics[metric], step=trainer.state.iteration)

        # @trainer.on(Events.EPOCH_COMPLETED)
        # def log_audio(trainer):
        #     with torch.no_grad():
        #         sample_audio(model, experiment, trainer.state.iteration)
        # @trainer.on(Events.EPOCH_COMPLETED)


        @trainer.on(Events.COMPLETED)
        def log_results(trainer):
            with torch.no_grad():
                model.eval()
                evaluator.run(valid_loader)
                metrics = evaluator.state.metrics
                # TODO: 依存性がある
                experiment.log_metric(
                    "best_valid_mse", min(valid_values))

                save_model_path = ExpConfig.get_save_model_path(experiment)
                if save_model_path is not None:
                    torch.save(model.state_dict(), save_model_path)

        _ = trainer.run(dataset_loader, max_epochs=1000)

        return


    model_configure = ExpConfig.model_configure
    project_name = ExpConfig.project_name
    if offline_directory is not None:
        opt = Optimizer(model_configure, project_name=project_name, experiment_class="OfflineExperiment", offline_directory=offline_directory)
        print("offline exp.")
    else:
        opt = Optimizer(model_configure, project_name=project_name)
        print("online exp.")

    for experiment in opt.get_experiments():
        fit(experiment)


if __name__ == "__main__":
    args = docopt(__doc__)
    main(config_modulepath=args['<config_modulepath>'],
         offline_directory=args['--offline-directory'] if '--offline-directory' in args else None,
         verbose=int(args['--verbose']))
