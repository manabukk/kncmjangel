# -*- coding=utf-8 -*-
"""Overview:
    Computing parameters of time-variant linear transformations based on JDGMM.
    WARNING: Currently only full covariance is supported.

Usage:
    cpGMMwVAD <jdgmm_filepath> -s <smcep_filepath> -g <file_spec_idxs> -o <output_prefix> [-d <vector_size>] [-f <feature_range>] [-v...]
    cpGMMwVAD -h

Options:
    <jdgmm_filepath>                      Path.
    -s --smcep-filepath <smcep_filepath>  hoge.
    -g --idxs-filepath <file_spec_idxs>   hoge.
    -o --output-prefix <output_prefix>    hoge.
    -d --vector-size <vector_size>        Vector size [default: 25].
    -f --feature-range <feature_range>    Feature range [default: 1,25].
    -v --verbose                          Show in detail.
    -h --help
"""

from docopt import docopt
import os
import sys

from misc.util import set_level, logger, mkdir_p
from misc.readwrite import load_matfile, load_fseq, write_fseq, load_alignment

import torch
import numpy as np


def load_matgmm(path):
    matdata = load_matfile(path)
    mat_mu = matdata['gmm']['mu'].item().astype('float64')
    logger.debug("Shape of mu of mat GMM: {}".format(mat_mu.shape))
    mu = mat_mu.transpose()
    covtype = matdata['gmm']['covtype'].item()[0]
    logger.debug("Type of covariance: {}".format(covtype))
    if covtype == 'full':
        # TODO: matlab と numpy の axis order って完全な逆？ n>2 の対応関係が不安
        mat_cov = matdata['gmm']['Sigma'].item().astype('float64')
        logger.debug("Shape of covariance of mat GMM: {}".format(mat_cov.shape))
        if len(mat_cov.shape) == 2:
            mat_cov = mat_cov[:,:,None]
        cov = mat_cov.transpose(2, 1, 0)
        # covtype = 'full'
    else:
        raise(NotImplementedError())
    mat_weight = matdata['gmm']['weight'].item().transpose().astype('float64')
    logger.debug("Shape of weight of mat GMM: {}".format(mat_weight.shape))
    weight = mat_weight.transpose()
    k = mu.shape[0]
    d = mu.shape[1]
    from model.gmm import GMM
    gmm = GMM(k=k, d=d, covtype=covtype)
    gmm.mu = torch.from_numpy(mu)
    logger.debug("Shape of mu: {}".format(gmm.mu.shape))
    gmm.cov = torch.from_numpy(cov)
    logger.debug("Shape of covariance: {}".format(gmm.cov.shape))
    gmm.weight = torch.from_numpy(weight)
    logger.debug("Shape of weight: {}".format(gmm.weight.shape))
    return gmm


def cparamTVLT(gmm, X):
    from model.gmm import convert2marginal
    xgmm = convert2marginal(gmm)
    P = xgmm.compute_posterior(X)  # (N, K)
    Sigma_yx = gmm.cov[:, xgmm.d:, :xgmm.d]
    Sigma_xx = xgmm.cov
    sigmasigma = torch.bmm(Sigma_yx, torch.inverse(Sigma_xx))
    for i in range(xgmm.d):
        _A = torch.matmul(P, sigmasigma[:, :, i]).unsqueeze(2)
        if i == 0:
            A = _A
        else:
            A = torch.cat([A, _A], dim=2)

    _b = gmm.mu[:, xgmm.d:] - \
        torch.bmm(sigmasigma, xgmm.mu.unsqueeze(2)).squeeze(2) 
    
    b = torch.matmul(P, _b)  # (T, D/2) = (T, K) * (K, D/2)
    return A, b


def main(jdgmm_filepath, smcep_filepath, idxs_filepath, output_prefix, dim, frange, verbose):
    set_level(verbose)

    logger.debug("Path to the GMM: {}".format(jdgmm_filepath))

    # if torch.cuda.is_available():
    #     device = 'cuda'
    # else:
    #     device = 'cpu'

    gmm = load_matgmm(jdgmm_filepath)

    logger.debug("Path to source mcep: {}".format(smcep_filepath))

    smcep = load_fseq(smcep_filepath, dim, list(range(frange[0], frange[1])))
    v_frames = load_alignment(path=idxs_filepath)

    logger.debug("Shape of smcep: {}".format(smcep.shape))

    # A, b = cparamTVLT(gmm, torch.from_numpy(smcep[v_frames, :]).to(device))
    _A, _b = cparamTVLT(gmm, torch.from_numpy(smcep[v_frames, :]))

    _A = _A.numpy()
    _b = _b.numpy()

    A_shape = list(_A.shape)
    A_shape[0] = smcep.shape[0]
    A = np.zeros(shape=A_shape)
    if len(A_shape) == 1:
        A[v_frames] = _A
    else:
        A[v_frames,:] = _A

    b_shape = list(_b.shape)
    b_shape[0] = smcep.shape[0]
    b = np.zeros(shape=b_shape)
    if len(b_shape) == 1:
        b[v_frames] = _b
    else:
        b[v_frames, :] = _b

    logger.debug("Shape of A: {}".format(A.shape))
    logger.debug("Shape of b: {}".format(b.shape))

    mkdir_p(os.path.dirname(output_prefix))
    write_fseq(path="{}.A".format(output_prefix), fseq=A.reshape(A.shape[0], -1))
    write_fseq(path="{}.b".format(output_prefix), fseq=b)


if __name__ == "__main__":
    args = docopt(__doc__)

    main(jdgmm_filepath=os.path.abspath(args['<jdgmm_filepath>']),
         smcep_filepath=os.path.abspath(args['--smcep-filepath']),
         idxs_filepath=os.path.abspath(args['--idxs-filepath']),
         output_prefix=os.path.abspath(args['--output-prefix']),
         dim=int(args['--vector-size']),
         frange=[int(f) for f in args['--feature-range'].split(',')],
         verbose=int(args['--verbose']))
