# -*- coding: utf-8 -*-

import torch.utils.data

import numpy as np
from docopt import docopt
import json
import os

from data.data.scp import SCP
from misc.readwrite import load_fseq, write_fseq, load_alignment
from misc.util import get_class_path, dynamic_import, mkdir_p
from data.dataset.transforms import get_transforms, ToTensor, Normalize

from torchvision.transforms import Compose


def save_dataset(dataset, args_dict, path):
    args_dict['CLASS'] = get_class_path(dataset)
    with open(path + '.args', 'w') as f:
        json.dump(args_dict, f)


def load_dataset(path):
    with open(path + ".args", 'r') as f:
        args_dict = json.load(f)
    dataset_class = args_dict.pop('CLASS')
    dataset = dynamic_import(dataset_class)(**args_dict)
    return dataset


def save_predicts(predicts, dataset, out_dir):
    assert len(predicts) == len(dataset.scp.scp)
    if out_dir is not None:
        mkdir_p(out_dir)
        for predict, scp_column in zip(predicts, dataset.scp.scp):
            out_path = os.path.join(out_dir, os.path.basename(scp_column[1]))
            write_fseq(path=out_path, fseq=predict)
    else:
        raise NotImplementedError()
        for predict, scp_column in zip(predicts, dataset.scp.scp):
            out_path = scp_column[1]
            write_fseq(path=out_path, fseq=predict)


def add_delta(x):
    x_right_roll = np.roll(x, 1, axis=0)
    x_right_roll[0, :] = 0
    x_left_roll = np.roll(x, -1, axis=0)
    x_left_roll[-1, :] = 0
    delta_x = x_right_roll * -0.5 + x_left_roll * 0.5
    _x = np.concatenate([x, delta_x], axis=1)
    return _x


class ParallelDataDatasetFromSCP(torch.utils.data.Dataset):
    def __init__(self, scp_path, x_vector_size, x_feature_range, y_vector_size, y_feature_range, inverse=False):
        super(ParallelDataDatasetFromSCP, self).__init__()
        self.predict = False
        self.scp = SCP(scp_path)
        self.xv = x_vector_size
        self.yv = y_vector_size
        self.xf = range(x_feature_range[0], x_feature_range[1])
        self.yf = range(y_feature_range[0], y_feature_range[1])
        self.X = None
        self.Y = None
        self.inverse = inverse

        for idx in range(self.scp.row_len):
            filepaths = self.scp.scp[idx]
            x = load_fseq(path=filepaths[0], vec_size=self.xv,
                          feature_range=self.xf)
            y = load_fseq(path=filepaths[1], vec_size=self.yv,
                          feature_range=self.yf)
            if len(filepaths) != 3:
                if self.X is None:
                    self.X = [x]
                else:
                    self.X.append(x)
                if self.Y is None:
                    self.Y = [y]
                else:
                    self.Y.append(y)
            else:
                align = load_alignment(path=filepaths[2])
                if self.X is None:
                    self.X = [x[align[1::2]]]
                else:
                    self.X.append(x[align[1::2]])
                if self.Y is None:
                    self.Y = [y[align[2::2]]]
                else:
                    self.Y.append(y[align[2::2]])

    def __len__(self):
        return self.scp.row_len

    def __getitem__(self, idx):

        if self.inverse:
            y = self.Y[idx]
            if self.predict:
                x = np.zeros(y.shape)
            else:
                x = self.X[idx]
            sample = {'x': np.asarray(y), 'y': np.asarray(x)}
        else:
            x = self.X[idx]
            if self.predict:
                y = np.zeros(x.shape)
            else:
                y = self.Y[idx]
            sample = {'x': np.asarray(x), 'y': np.asarray(y)}

        return sample

    def predict_mode(self, flag):
        self.predict = flag


class JointParallelDataDatasetFromSCP(torch.utils.data.Dataset):
    def __init__(self, scp_path, x_vector_size, x_feature_range, y_vector_size, y_feature_range, norm_param_path=None, on_mem=True):
        super(JointParallelDataDatasetFromSCP, self).__init__()
        self.predict = False
        self.scp = SCP(scp_path)
        self.xv = x_vector_size
        self.yv = y_vector_size
        self.xf = range(x_feature_range[0], x_feature_range[1])
        self.yf = range(y_feature_range[0], y_feature_range[1])
        self.X = None
        self.Y = None
        self.on_mem = on_mem

        if norm_param_path is not None:
            self.transforms = Compose([ToTensor(), Normalize(
                norm_param_path, x_vector_size, y_vector_size)])
        else:
            self.transforms = Compose([ToTensor()])

        if self.on_mem:
            for idx in range(self.scp.row_len):
                filepaths = self.scp.scp[idx]
                x = load_fseq(path=filepaths[0], vec_size=self.xv,
                              feature_range=self.xf)
                y = load_fseq(path=filepaths[1], vec_size=self.yv,
                              feature_range=self.yf)
                align = load_alignment(path=filepaths[2])
                if self.X is None:
                    self.X = x[align[1::2]]
                else:
                    self.X = np.concatenate([self.X, x[align[1::2]]], axis=0)
                if self.Y is None:
                    self.Y = y[align[2::2]]
                else:
                    self.Y = np.concatenate([self.Y, y[align[2::2]]], axis=0)

    def __len__(self):
        if self.on_mem:
            return len(self.X)
        else:
            return self.scp.row_len

    def __getitem__(self, idx):
        if self.on_mem:
            x = self.X[idx]

            if self.predict:
                y = np.zeros(x.shape)
            else:
                y = self.Y[idx]

            sample = {'x': np.concatenate([x, y]), 'y': np.concatenate([x, y])}
            return sample

        else:
            filepaths = self.scp.scp[idx]
            x = load_fseq(path=filepaths[0], vec_size=self.xv,
                          feature_range=self.xf)

            if self.predict:
                y = np.zeros(x.shape)
                sample = {'x': np.concatenate(
                    [x, y], axis=1), 'y': np.concatenate([x, y], axis=1)}
            else:
                raise NotImplementedError()
            return sample

    def predict_mode(self, flag):
        self.predict = flag


class ParallelDataWithDeltaDatasetFromSCP(torch.utils.data.Dataset):
    def __init__(self, scp_path, x_vector_size, x_feature_range, y_vector_size, y_feature_range, norm_param_path=None, delta_dim=1):
        super(ParallelDataWithDeltaDatasetFromSCP, self).__init__()
        self.predict = False
        self.scp = SCP(scp_path)
        self.xv = x_vector_size
        self.yv = y_vector_size
        self.xf = range(x_feature_range[0], x_feature_range[1])
        self.yf = range(y_feature_range[0], y_feature_range[1])
        self.X = None
        self.Y = None
        self.delta_dim = delta_dim

        if norm_param_path is not None:
            self.transforms = Compose([ToTensor(), Normalize(
                norm_param_path, x_vector_size, y_vector_size)])
        else:
            self.transforms = Compose([ToTensor()])

        for idx in range(self.scp.row_len):
            filepaths = self.scp.scp[idx]
            x = load_fseq(path=filepaths[0], vec_size=self.xv,
                          feature_range=self.xf)
            y = load_fseq(path=filepaths[1], vec_size=self.yv,
                          feature_range=self.yf)
            x = add_delta(x)
            y = add_delta(y)

            if len(filepaths) != 3:
                if self.X is None:
                    self.X = [x]
                else:
                    self.X.append(x)
                if self.Y is None:
                    self.Y = [y]
                else:
                    self.Y.append(y)
            else:
                align = load_alignment(path=filepaths[2])
                if self.X is None:
                    self.X = [x[align[1::2]]]
                else:
                    self.X.append(x[align[1::2]])
                if self.Y is None:
                    self.Y = [y[align[2::2]]]
                else:
                    self.Y.append(y[align[2::2]])

    def __len__(self):
        return self.scp.row_len 

    def __getitem__(self, idx):
        x = self.X[idx]

        if self.predict:
            y = np.zeros(x.shape)
        else:
            y = self.Y[idx]

        sample = {'x': np.asarray(x), 'y': np.asarray(y)}

        return sample

    def predict_mode(self, flag):
        self.predict = flag
