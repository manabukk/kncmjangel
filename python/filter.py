# -*- coding: utf-8 -*-
"""Overview:
    Processing a filter to feature sequences.
    20n dB/decade or 6n dB/octave.

Usage:
    filter <input_filepath> <output_filepath> -F <nyquist_freq> [-p <freq_pass>] [-s <freq_stop>] [-a <axis>] [-g <gain_pass>] [-G <gain_stop>] [-d <vector_size>] [-f <feature_range>] [-v...]
    filter vis -F <nyquist_freq> [-p <freq_pass>] [-s <freq_stop>] [-g <gain_pass>] [-G <gain_stop>] [-v...]
    filter -h

Options:
    <input_filepath>
    <output_filepath>
    -F --nyquist-freq <nyquist_freq>     Nyquist frequency.
    -p --passband-edge-freq <freq_pass>  Frequency of passband edge (Hz) [default: 50].
    -s --stopband-edge-freq <freq_stop>  Frequency of stopband edge (Hz) [default: 500].
    -a --axis <axis>                     Processing filter along with the axis [default: 0].
    -g --passband-edge-gain <gain_pass>  Gain of passband edge (db) [default: 3].
    -G --stopband-edge-gain <gain_stop>  Gain of stopband edge (db) [default: 100]
    -d --vector-size <vector_size>       Vector size [default: 25]
    -f --feature-range <feature_range>   Feature range [default: 1,25]
    -v --verbose                         Show in detail.
    -h --help                            Show this message.
"""

from docopt import docopt
import os
import numpy as np
from scipy import signal

from misc.util import set_level, logger
from misc.readwrite import load_fseq, write_fseq

# https://qiita.com/trami/items/9553342d970443f5e663
def filter2fig(fpass, fstop, gpass, gstop, nyquist_freq, verbose):
    set_level(verbose)

    # Normalization
    Wp = fpass / nyquist_freq
    Ws = fstop / nyquist_freq
    N, Wn = signal.buttord(Wp, Ws, gpass, gstop, fs=nyquist_freq*2)
    logger.debug("Order of butterworth filter: {}".format(N))
    logger.debug("Freq of cut-off: {:<6g} Hz".format(Wn * nyquist_freq))
    b1, a1 = signal.butter(N, Wn, "low")
    import matplotlib.pyplot as plt
    f, h = signal.freqz(b1, a1, fs=nyquist_freq*2)
    plt.semilogx(f, 20 * np.log10(abs(h)))
    plt.title('Butterworth filter frequency response')
    plt.xlabel('Frequency [Hz]')
    plt.ylabel('Amplitude [dB]')
    plt.grid(which='both', axis='both')
    plt.axvline(fpass, color='green')  # cutoff frequency
    plt.axvline(fstop, color='green')  # cutoff frequency
    plt.savefig('./filter.png')


def main(input_filepath, output_filepath, vector_size, feature_range, axis, fpass, fstop, gpass, gstop, nyquist_freq, verbose, **kargs):
    set_level(verbose)

    # Normalization
    Wp = fpass / nyquist_freq
    Ws = fstop / nyquist_freq

    data = np.asarray(load_fseq(path=input_filepath, vec_size=vector_size,
                                feature_range=range(feature_range[0], feature_range[1])))
    logger.debug(data.shape)
    # Butterworth filter
    N, Wn = signal.buttord(Wp, Ws, gpass, gstop, fs=nyquist_freq*2)
    b1, a1 = signal.butter(N, Wn, "low")
    _data = signal.filtfilt(b1, a1, data, axis=axis)

    logger.debug(_data.shape)

    write_fseq(path=output_filepath, fseq=_data)


if __name__ == "__main__":
    args = docopt(__doc__)

    if args['vis']:
        filter2fig(nyquist_freq=float(args['--nyquist-freq']),
                   fpass=float(args['--passband-edge-freq']),
                   fstop=float(args['--stopband-edge-freq']),
                   gpass=float(args['--passband-edge-gain']),
                   gstop=float(args['--stopband-edge-gain']),
                   verbose=int(args['--verbose']))

    else:
        main(input_filepath=os.path.abspath(args['<input_filepath>']),
             output_filepath=os.path.abspath(args['<output_filepath>']),
             nyquist_freq=float(args['--nyquist-freq']),
             fpass=float(args['--passband-edge-freq']),
             fstop=float(args['--stopband-edge-freq']),
             gpass=float(args['--passband-edge-gain']),
             gstop=float(args['--stopband-edge-gain']),
             axis=int(args['--axis']),
             vector_size=int(args['--vector-size']),
             feature_range=[int(f)
                            for f in args['--feature-range'].split(',')],
             verbose=int(args['--verbose']))
