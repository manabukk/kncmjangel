# -*- coding: utf-8 -*-

import torch
from torch import nn

import json, os, sys
from abc import ABCMeta, abstractmethod
from misc.util import get_class_path, dynamic_import, mkdir_p, rm
from misc.configure import CONF
from torchsummary import summary


def save_model(model, args_dict, path):
    mkdir_p(os.path.dirname(path))
    torch.save(model.state_dict(), path)
    args_dict['CLASS'] = get_class_path(model)
    with open(path + '.args', 'w') as f:
            json.dump(args_dict, f)

def load_model(path):
    with open(path + ".args", 'r') as f:
        args_dict = json.load(f)
    model_class = args_dict.pop('CLASS')
    model = dynamic_import(model_class)(**args_dict)
    model.load_state_dict(torch.load(path))
    return model, args_dict


def get_summary_str(model):
    sys.stdout = open(CONF.TEMP_DIR + "/model.summary", "w")
    model.float()
    summary(model, input_size=[1, model.input_dim])
    model.double()
    sys.stdout.close()
    sys.stdout = sys.__stdout__
    model_summary = open(CONF.TEMP_DIR + "/model.summary", "r").read()
    rm(CONF.TEMP_DIR + "/model.summary")
    return model_summary


class KNCNNModel(nn.Module):
    """docstring for KNCModel."""

    __metaclass__ = ABCMeta

    def __init__(self):
        super(KNCNNModel, self).__init__()
        torch.manual_seed(225)
        
    @abstractmethod
    def forward(self):
        pass

    # @abstractmethod
    # def kncupdate(self):
    #     pass

    def predict(self, X):
        predict = self(X)
        return predict

    def optimize(self, X, Y, optimizer, loss_fn):
        optimizer.zero_grad()
        predict = self(X)

        loss = loss_fn(predict, Y)
        loss.backward()
        optimizer.step()
        return loss.item()

# class KNCModel(object):
#     """docstring for KNCModel."""

#     __metaclass__ = ABCMeta

#     def __init__(self):
#         super(KNCModel, self).__init__()
#         torch.manual_seed(1234)

#     def state_dict(self):
#         # print(self.__dict__.keys())
#         return self.__dict__

#     def load_state_dict(self, params):
#         self.__dict__.update(params)
