# -*- coding: utf-8 -*-
"""Overview:
    Computing differentials.
    Out = A - B

Usage:
    diff <filepath_A> <filepath_B> <save_path> [-u <x_vector_size>] [-w <y_vector_size>] [-f <x_feature_range>] [-g <y_feature_range>] [-v...]
    diff -h | --help

Options:
    <filepath_A>                            hoge.
    <filepath_B>                            hoge.
    <save_path>                             Path of output file.
    -u --x-vector-size <x_vector_size>      hoge [default: 25].
    -w --y-vector-size <y_vector_size>      hoge [default: 25].
    -f --x-feature-range <x_feature_range>  hoge [default: 1,25].
    -g --y-feature-range <y_feature_range>  hoge [default: 1,25].
    -v --verbose                            Show verbose message
    -h --help                               Show this screen and exit.
"""

from docopt import docopt

import sys, os

from misc.util import set_level, logger
from misc.readwrite import load_fseq, write_fseq

def main(filepath_A, filepath_B, save_path, x_vector_size, y_vector_size, x_feature_range, y_feature_range, verbose):
    set_level(verbose)
    sfseq = load_fseq(path=filepath_A, vec_size=x_vector_size,
                      feature_range=range(x_feature_range[0], x_feature_range[1]))
    tfseq = load_fseq(path=filepath_B, vec_size=y_vector_size,
                      feature_range=range(y_feature_range[0], y_feature_range[1]))
    logger.debug("Source features shape: {}".format(str(sfseq.shape)))
    logger.debug("Target features shape: {}".format(str(tfseq.shape)))

    output = sfseq - tfseq
    logger.debug("Output features shape: {}".format(str(tfseq.shape)))

    write_fseq(fseq=output, path=save_path)

if __name__ == "__main__":
    args = docopt(__doc__)
    main(filepath_A=os.path.abspath(args['<filepath_A>']),
         filepath_B=os.path.abspath(args['<filepath_B>']),
         save_path=os.path.abspath(args['<save_path>']),
         x_vector_size=int(args['--x-vector-size']),
         y_vector_size=int(args['--y-vector-size']),
         x_feature_range=[int(f)
                          for f in args['--x-feature-range'].split(',')],
         y_feature_range=[int(f)
                          for f in args['--y-feature-range'].split(',')],
         verbose=int(args['--verbose']))