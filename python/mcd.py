# -*- coding: utf-8 -*-
"""Overview:
    Calculating mel-cepstrum distance.

Usage:
    mcd <source_file> <target_file> <alignment_file> [-d <vector_size>] [-f <feature_range>] [-v...]
    mcd -h

Options:
    <source_file> 
    <target_file>
    <alignment_file>
    -d --vector-size <vector_size>            Vector size [default: 25].
    -f --feature-range <feature_range>        Feature range [default: 1,25].
    -v --verbose                              Show in detail.
    -h --help                                 Show this message.
"""

from docopt import docopt
import sys
import os

from misc.readwrite import load_fseq, load_alignment
from misc.util import logger, set_level
from misc.pdata import get_parallel_data

from misc.cdist import compute_melcd

def main(sfp, tfp, afp, d, frange, verbose):

    set_level(verbose)
    sfseq = load_fseq(path=sfp, vec_size=d,
                      feature_range=range(frange[0], frange[1]))
    tfseq = load_fseq(path=tfp, vec_size=d,
                      feature_range=range(frange[0], frange[1]))
    alignment = load_alignment(path=afp)

    logger.debug("Source features shape: {}".format(str(sfseq.shape)))
    logger.debug("Target features shape: {}".format(str(tfseq.shape)))
    logger.debug("Alignment length: {}".format(str(alignment[0])))

    p_data = get_parallel_data(
        fseq_list=[sfseq, tfseq], alignment=alignment)[0]

    mcd = compute_melcd(p_data)

    sys.stdout.write("{:.6g}".format(mcd))

if __name__ == '__main__':
    args = docopt(__doc__)

    main(sfp=os.path.abspath(args['<source_file>']),
         tfp=os.path.abspath(args['<target_file>']),
         afp=os.path.abspath(args['<alignment_file>']),
         d=int(args['--vector-size']),
         frange=[int(f) for f in args['--feature-range'].split(',')],
         verbose=int(args['--verbose']))
