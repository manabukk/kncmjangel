#!/bin/bash

if [ "$(uname)" == 'Darwin' ]; then  # For My Mac
    OS='Mac'
    gcc -std=c99 -fPIC -Wall -c -o fastdp.o fastdp.c
    # gcc -std=c99 -fPIC -Wall -I/usr/local/Cellar/python3/3.6.2/Frameworks/Python.framework/Versions/3.6/include/python3.6m -I/usr/local/Cellar/python3/3.6.2/Frameworks/Python.framework/Versions/3.6/include/python3.6m -c -o fastdpWrapper.o fastdpWrapper.c
    gcc -std=c99 -fPIC -Wall -I/usr/local/Cellar/python/3.7.2_2/Frameworks/Python.framework/Versions/3.7/include/python3.7m -c -o fastdpWrapper.o fastdpWrapper.c 
    # gcc -std=c99 -fPIC -Wall -L/usr/local/opt/python3/Frameworks/Python.framework/Versions/3.6/lib/python3.6/config-3.6m-darwin -lpython3.6m -shared -o fastdp.so fastdp.o fastdpWrapper.o
    gcc -std=c99 -fPIC -Wall -L/usr/local/opt/python3/Frameworks/Python.framework/Versions/3.7/lib/python3.7/config-3.7m-darwin -lpython3.7m -shared -o fastdp.so fastdp.o fastdpWrapper.o
elif [ "$(expr substr $(uname -s) 1 5)" == 'Linux' ]; then  # For elf or valkyrie
    OS='Linux'
    gcc -std=c99 -fPIC -Wall -c -o fastdp.o fastdp.c
    gcc -std=c99 -fPIC -Wall -I/usr/include/python3.7 -c -o fastdpWrapper.o fastdpWrapper.c
    gcc -std=c99 -fPIC -Wall -shared -o fastdp.so fastdp.o fastdpWrapper.o
# elif [ "$(expr substr $(uname -s) 1 10)" == 'MINGW32_NT' ]; then     
    # OS='Cygwin'
else
  echo "Your platform ($(uname -a)) is not supported."
  exit 1
fi

mv ./fastdp.so ../