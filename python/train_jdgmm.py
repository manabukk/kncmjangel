# -*- coding=utf-8 -*-
"""Overview:
    Training JDGMM based on the dsk toolbox.

Usage:
    trainJDGMM <scp_filepath> [-s <script_dir>] [-t <save_dir>] [-m <nb_mixtures>] [-d <dim>] [-c <covtype>] [-e] [-f] [-l <log_filepath>] [-b <matlabbin>] [-p <toolbox_path>] [-q <misc_path>] [-v...]
    trainJDGMM -h

Options:
    <scp_filepath>                    Path.
    -s --script-dir <script_dir>      hoge [default: script].
    -t --save-dir <save_dir>          hoge [default: model].
    -m --nb-mixtures <nb_mixtures>    hoge [default: 256].
    -d --dim <dim>                    hoge [default: 25].
    -c --covtype <covtype>            'diagonal', 'full', or 'crossdiag' [default: crossdiag].
    -e --usedelta                     hoge.
    -f --usepower                     hoge.
    -l --log-filepath <log_filepath>  hoge [default: train_jdgmm.log].
    -b --matlabbin <matlabbin>        hoge [default: /opt/MATLAB/R2018b/bin/matlab].
    -p --toolbox-path <toolbox_path>  hoge [default: /work/kotani/matlab/vc-toolbox].
    -q --misc-path <misc_path>        hoge [default: /work/kotani/matlab/misc].
    -v --verbose                      Show in detail.
    -h --help                         Show this message.
"""

from docopt import docopt
import os
import sys

import subprocess
from misc.util import set_level, logger, mkdir_p

CONFIGURATION_TEMPLATE = \
    """
function train_JDGMM
newpath = '{toolbox_path}'; addpath(newpath,path);
newpath = '{misc_path}'; addpath(newpath,path);
opt = struct;
opt.savedir = '{savedir}';
opt.nmix = {nb_mixtures};
opt.dim = {dim};
opt.usepower = {usepower};
opt.usedelta = {usedelta};
opt.readtype = 'joint';
opt.fileout = false;
opt.covtype = '{covtype}';

JDMtrain('{scpfile}',opt);
"""


def main(script_dir, toolbox_path, misc_path, save_dir, nb_mixtures, dim, covtype, usedelta, usepower, scp_filepath, log_filepath, matlabbin, verbose):
    set_level(verbose)

    mkdir_p(script_dir)
    mkdir_p(save_dir)

    config = CONFIGURATION_TEMPLATE.format(toolbox_path=toolbox_path,
                                           misc_path=misc_path,
                                           savedir=save_dir,
                                           nb_mixtures=nb_mixtures,
                                           covtype=covtype,
                                           dim=dim,
                                           usedelta='true' if usedelta else 'false',
                                           usepower='true' if usepower else 'false',
                                           scpfile=scp_filepath)

    with open(os.path.join(script_dir, 'train_JDGMM.m'), 'w') as f:
        f.write(config)

    cmd = "echo \"newpath = '{script_dir}'; addpath(newpath, path); train_JDGMM; quit;\" | {matlabbin} -nojvm -nosplash -nodesktop -logfile {log_filepath}".format(
        script_dir=script_dir,
        matlabbin=matlabbin,
        log_filepath=log_filepath)

    subprocess.run(cmd, shell=True)


if __name__ == "__main__":
    args = docopt(__doc__)

    main(script_dir=os.path.abspath(args['--script-dir']),
         toolbox_path=os.path.abspath(args['--toolbox-path']),
         misc_path=os.path.abspath(args['--misc-path']),
         save_dir=os.path.abspath(args['--save-dir']),
         nb_mixtures=int(args['--nb-mixtures']),
         covtype=args['--covtype'],
         dim=int(args['--dim']),
         usedelta=args['--usedelta'],
         usepower=args['--usepower'],
         scp_filepath=os.path.abspath(args['<scp_filepath>']),
         log_filepath=os.path.abspath(args['--log-filepath']),
         matlabbin=os.path.abspath(args['--matlabbin']),
         verbose=int(args['--verbose']))
