# -*- coding: utf-8 -*-
"""Overview:
    Processing World synthesis.

Usage:
    world <synthesized_filepath> -f <f0_filepath> -a <ap_filepath> -s <sp_filepath> [-S <frame_shift>] [-r <sampling_rate>] [-b <nb_bins>] [-v...]
    world -h 

Options:
    <synthesized_filepath>              hoge.
    -f --f0-filepath <f0_filepath>      File path to the output of F0.
    -a --ap-filepath <ap_filepath>      File path to the output of Aperiodic component.
    -s --sp-filepath <sp_filepath>      File path to the output of Spectrum.
    -S --frame-shift <frame_shift>      Length of frame shift (ms, float) [default: 1.0].
    -r --sampling-rate <sampling_rate>  Sampling rate (Hz) [default: 16000.0].
    -b --nb-bins <nb_bins>              hoge [default: 513].
    -v --verbose                        Show the process in detail.
    -h --help                           Show this message.
"""

from docopt import docopt
import os

import pyworld as pw

from misc.util import set_level, logger
from misc.readwrite import load_fseq, write_fseq


def main(synthesized_filepath, frame_shift, sampling_rate, f0_filepath, ap_filepath, sp_filepath, nb_bins, verbose, **kargs):
    set_level(verbose)
    f0 = load_fseq(path=f0_filepath, vec_size=1,
                   feature_range=range(1)).reshape([-1])
    logger.debug(f0.shape)
    sp = load_fseq(path=sp_filepath, vec_size=nb_bins,
                   feature_range=range(nb_bins))
    logger.debug(sp.shape)
    ap = load_fseq(path=ap_filepath, vec_size=nb_bins,
                   feature_range=range(nb_bins))
    logger.debug(ap.shape)

    y = pw.synthesize(f0, sp, ap, sampling_rate, frame_shift)
    write_fseq(path=synthesized_filepath, fseq=y)


if __name__ == "__main__":
    args = docopt(__doc__)

    main(synthesized_filepath=os.path.abspath(args['<synthesized_filepath>']),
         f0_filepath=os.path.abspath(
             args['--f0-filepath']) if args['--f0-filepath'] is not None else None,
         ap_filepath=os.path.abspath(
             args['--ap-filepath']) if args['--ap-filepath'] is not None else None,
         sp_filepath=os.path.abspath(
             args['--sp-filepath']) if args['--sp-filepath'] is not None else None,
         frame_shift=float(args['--frame-shift']),
         sampling_rate=float(args['--sampling-rate']),
         nb_bins=int(args['--nb-bins']),
         verbose=int(args['--verbose']))
