# -*- coding: utf-8 -*-

import torch
from torch import nn
from torch.nn import functional as F
import numpy as np

from model.model import KNCNNModel
from model.activations import MyActivations
from misc.mlpg import static2staticdelta, staticdelta2static, cmpt_var_Y, get_window_matrix


class Simple(KNCNNModel):
    """docstring for Simple NNs."""

    def __init__(self, nb_layers, nb_units, activfn, output_activfn, input_dim, output_dim, dropout_prob_list=None):
        super(Simple, self).__init__()
        fc = [nn.Linear(nb_units, nb_units) if i > 0 else nn.Linear(
            input_dim, nb_units) for i in range(nb_layers - 1)]
        fc.append(nn.Linear(nb_units, output_dim))
        self.fc = nn.ModuleList(fc)

        if dropout_prob_list is None:
            dropout_prob_list = [0.0 for _ in range(nb_layers)]
        self.dropouts = nn.ModuleList(
            [nn.Dropout(p=p) for p in dropout_prob_list])

        self.activfn = MyActivations(activfn)
        self.output_activfn = MyActivations(output_activfn)

        self.input_dim = input_dim
        self.output_dim = output_dim

    def forward(self, x):
        h = self.dropouts[0](x)
        h = self.activfn(self.fc[0](h))
        for i, fc in enumerate(self.fc[1:-1]):
            h = self.dropouts[1+i](h)
            h = self.activfn(fc(h))
        h = self.dropouts[len(self.fc)-1](h)
        predict = self.output_activfn(self.fc[-1](h))
        return predict

    def optimize(self, X, Y, optimizer, loss_fn):
        optimizer.zero_grad()
        predict_Y = self(X)
        loss = self.loss_fn(predict_Y, Y, loss_fn)
        loss.backward()
        optimizer.step()
        return loss.item()

    def loss_fn(self, predict_Y, Y, loss_fn):
        return loss_fn(predict_Y, Y)


class SimpleMLPG(KNCNNModel):
    """docstring for Simple NNs."""

    def __init__(self, nb_layers, nb_units, activfn, output_activfn, input_dim, output_dim, train_dataset_path, delta_num=1, dropout_prob_list=None):
        super(SimpleMLPG, self).__init__()
        fc = [nn.Linear(nb_units, nb_units) if i > 0 else nn.Linear(
            input_dim*(delta_num+1), nb_units) for i in range(nb_layers - 1)]
        fc.append(nn.Linear(nb_units, output_dim*(delta_num+1)))
        self.fc = nn.ModuleList(fc)

        if dropout_prob_list is None:
            dropout_prob_list = [0.0 for _ in range(nb_layers)]
        self.dropouts = nn.ModuleList(
            [nn.Dropout(p=p) for p in dropout_prob_list])

        self.activfn = MyActivations(activfn)
        self.output_activfn = MyActivations(output_activfn)

        self.input_dim = input_dim
        self.output_dim = output_dim

        self.train_dataset_path = train_dataset_path
        self.var_Y = cmpt_var_Y(train_dataset_path)

        self.delta_num = delta_num

    def forward(self, x):

        W = torch.from_numpy(get_window_matrix(
            delta_num=self.delta_num, sequence_length=x.size(0)))
        X = static2staticdelta(x, delta_num=self.delta_num, W=W)

        h = self.dropouts[0](X)
        h = self.activfn(self.fc[0](h))
        for i, fc in enumerate(self.fc[1:-1]):
            h = self.dropouts[1+i](h)
            h = self.activfn(fc(h))
        h = self.dropouts[len(self.fc)-1](h)
        Y = self.output_activfn(self.fc[-1](h))

        y = staticdelta2static(Y, self.var_Y, delta_num=self.delta_num, W=W)

        return y

    def optimize(self, X, Y, optimizer, loss_fn):
        optimizer.zero_grad()
        predict_Y = self(X)
        loss = self.loss_fn(predict_Y, Y, loss_fn)
        loss.backward()
        optimizer.step()
        return loss.item()

    def loss_fn(self, predict_Y, Y, loss_fn):
        return loss_fn(predict_Y, Y)

    def cp(self, x):
        W = torch.from_numpy(get_window_matrix(
            delta_num=self.delta_num, sequence_length=x.size(0)))
        X = static2staticdelta(x, delta_num=self.delta_num, W=W)

        h = self.dropouts[0](X)
        h = self.activfn(self.fc[0](h))
        for i, fc in enumerate(self.fc[1:-1]):
            h = self.dropouts[1+i](h)
            h = self.activfn(fc(h))
        h = self.dropouts[len(self.fc)-1](h)
        Y = self.output_activfn(self.fc[-1](h))

        y = staticdelta2static(Y, self.var_Y, delta_num=self.delta_num, W=W)

        return {"Y": Y}
