#include <Python.h>  // OSX の場合は <Python/Python.h> からインストールする的な記述を見たが、Python 3 系の Python.h へパスが通っていればよい。手元の Mac では alias python='python3' で解決した。
// #include <malloc.h>
#include <stdlib.h>

// extern int *run(const int, const int, const double *, const int, const int *, const double *, const double);
extern int* run(const int, const int, const double *, const double*, const int, const int, const int *, const double *, const double);

PyObject* fastdp_run(PyObject* self, PyObject* args){
    int x_size, y_size;
    // PyObject *dist_m;
    PyObject *x_seq;
    PyObject *y_seq;
    int dim;
    int path_num;
    PyObject* p;
    PyObject* c;
    double constraint_value;
    int* alignment;

    // if(!PyArg_ParseTuple(args, "iiOiOOd", &x_size, &y_size, &dist_m, &path_num, &p, &c, &constraint_value)){
    if(!PyArg_ParseTuple(args, "iiOOiiOOd", &x_size, &y_size, &x_seq, &y_seq, &dim, &path_num, &p, &c, &constraint_value)){
        return NULL;
    }else{

        //        dist matrix
        // double* dist_matrix = (double*)malloc(sizeof(double)*y_size*x_size);
        // PyObject *iter_d = PyObject_GetIter(dist_m);
        // if (!iter_d) {
        //     // error not iterator
        // }

        // for (int i = 0; i < x_size*y_size; ++i) {
        //     PyObject *next = PyIter_Next(iter_d);
        //     if (!next) {
        //         // nothing left in the iterator
        //         break;
        //     }

        //     if (!PyFloat_Check(next)) {
        //         // error, we were expecting a floating point value
        //     }

        //     dist_matrix[i] = PyFloat_AsDouble(next);
        // }

        // x_seq
        double *x_sequence = (double *)malloc(sizeof(double) * x_size*dim);
        PyObject *iter_x = PyObject_GetIter(x_seq);
        if (!iter_x)
        {
            // error not iterator
        }

        for (int i = 0; i < x_size * dim; ++i)
        {
            PyObject *next = PyIter_Next(iter_x);
            if (!next)
            {
                // nothing left in the iterator
                break;
            }

            if (!PyFloat_Check(next))
            {
                // error, we were expecting a floating point value
            }

            x_sequence[i] = PyFloat_AsDouble(next);
        }

        // x_seq
        double *y_sequence = (double *)malloc(sizeof(double) * y_size * dim);
        PyObject *iter_y = PyObject_GetIter(y_seq);
        if (!iter_y)
        {
            // error not iterator
        }

        for (int i = 0; i < y_size * dim; ++i)
        {
            PyObject *next = PyIter_Next(iter_y);
            if (!next)
            {
                // nothing left in the iterator
                break;
            }

            if (!PyFloat_Check(next))
            {
                // error, we were expecting a floating point value
            }

            y_sequence[i] = PyFloat_AsDouble(next);
        }

        //        path
        int path[path_num*2];
        PyObject *iter_p = PyObject_GetIter(p);
        if (!iter_p) {
            // error not iterator
        }

        for (int i = 0; i < path_num*2; ++i) {
            PyObject *next = PyIter_Next(iter_p);
            if (!next) {
                // nothing left in the iterator
                break;
            }

            if (!PyLong_Check(next)) {
                // error, we were expecting a integer point value
            }

//            path[i] = PyFloat_AsDouble(next);
            path[i] = PyLong_AsSsize_t(next);
        }

//        cost
        double cost[path_num];
        PyObject *iter_c = PyObject_GetIter(c);
        if (!iter_c) {
            // error not iterator
        }

        for (int i = 0; i < path_num; ++i) {
            PyObject *next = PyIter_Next(iter_c);
            if (!next) {
                // nothing left in the iterator
                break;
            }

            if (!PyFloat_Check(next)) {
                // error, we were expecting a floating point value
            }

            cost[i] = PyFloat_AsDouble(next);
        }

        // alignment = run(x_size, y_size, dist_matrix, path_num, path, cost, constraint_value);
        alignment = run(x_size, y_size, x_sequence, y_sequence, dim, path_num, path, cost, constraint_value);
        int frame_size = alignment[0];

        PyListObject* pyalignment = (PyListObject *) PyList_New(frame_size*2+1);
        for (int i = 0; i < frame_size*2+1; ++i) {
            PyList_SetItem(pyalignment, i, Py_BuildValue("i", alignment[i]));
        }
        return Py_BuildValue("O", pyalignment);
    }
}

static PyMethodDef fastdp_methods[] = {
    {"run", (PyCFunction)fastdp_run, METH_VARARGS, NULL},
    {NULL, NULL},
};

struct module_state
{
    PyObject *error;
};

#define GETSTATE(m)((struct module_state *)PyModule_GetState(m))
static struct module_state _state;

static int fastdp_traverse(PyObject *m, visitproc visit, void *arg)
{
    Py_VISIT(GETSTATE(m)->error);
    return 0;
}

static int fastdp_clear(PyObject *m)
{
    Py_CLEAR(GETSTATE(m)->error);
    return 0;
}

static struct PyModuleDef moduledef = {
    PyModuleDef_HEAD_INIT,
    "fastdp",
    NULL,
    sizeof(struct module_state),
    fastdp_methods,
    NULL,
    fastdp_traverse,
    fastdp_clear,
    NULL};

#define INITERROR return NULL

PyMODINIT_FUNC
PyInit_fastdp(void)
{
    // Py_InitModule("dp", dp_methods);
    PyObject *module = PyModule_Create(&moduledef);

    if (module == NULL)
        INITERROR;
    struct module_state *st = GETSTATE(module);

    st->error = PyErr_NewException("fastdp.Error", NULL, NULL);
    if (st->error == NULL)
    {
        Py_DECREF(module);
        INITERROR;
    }

    return module;
}