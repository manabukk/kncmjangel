# -*- coding: utf-8 -*-
""" Docstring """
import numpy as np


def affine_trans(x, A, b):
    """
    docstring here
        :param x:
        :param A:
        :param b:
    """
    return np.dot(x, A.transpose()) + b


# x, y are sequences, such as sentences.
# http://www.infra.kochi-tech.ac.jp/takagi/Geomatics/10GeoCorrection.pdf
def affine_estimation(x, y):
    """
    docstring here
        :param x:
        :param y:
    """
    N = len(x)
    D = len(x[0])
    assert len(y) == N, '# ERROR\n affine_estimation\n frame length error'
    assert len(y[0]) == D, '# ERROR\n affine_estimation\n vec size error'
    x_array = np.concatenate((np.array(x), np.ones(N)[:, np.newaxis]), axis=1)
    y_array = np.array(y)

    # x_array = x_array[:100]

    # かしこいやり方がわからない
    # left_matrix = np.array([np.dot(x_array[n][:, np.newaxis],
    #                                x_array[n][np.newaxis, :]) for n in range(N)]).sum(axis=0)

    left_matrix = []
    for i in range(0, N, 10000):
        left_matrix.append(np.asarray([np.dot(x_array[j][:, np.newaxis], x_array[j][np.newaxis, :])
                                       for j in range(i, i+10000 if i+10000 < N else N)]).sum(axis=0))
    left_matrix = np.asarray(left_matrix).sum(axis=0)
    right_matrix = np.dot(x_array.transpose(), y_array).transpose()

    Ab = np.array(
        list(map(lambda x: np.linalg.solve(left_matrix, x), right_matrix)))
    A = Ab[:, range(D)]
    b = Ab[:, D]

    # print np.dot(x_array[:, range(D)], A.transpose()) + b
    return A, b
