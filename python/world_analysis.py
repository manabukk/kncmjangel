# -*- coding: utf-8 -*-
"""Overview:
    Processing World analysis.

Usage:
    worldanalysis <raw> [-f <f0_filepath>] [-a <ap_filepath>] [-s <sp_filepath>] [-y <resynthesized_filepath>] [-S <frame_shift>] [-r <sampling_rate>] [-g <f0_floor>] [-c <f0_ceil>] [-R] [-H] [-C <channels_in_octave>] [-v]
    worldanalysis -h 

Options:
    -f --f0-filepath <f0_filepath>                        File path to the output of F0.
    -a --ap-filepath <ap_filepath>                        File path to the output of Aperiodic component.
    -s --sp-filepath <sp_filepath>                        File path to the output of Spectrum.
    -y --resynthesized-filepath <resynthesized_filepath>  File path to the output of re-synthesized speech.
    -S --frame-shift <frame_shift>                        Period between consecutive frames in milliseconds [default: 1.0].
    -r --sampling-rate <sampling_rate>                    Sampling rate (Hz) [default: 16000.0].
    -g --f0-floor <f0_floor>                              Floor of F0 estimation (Hz) [default: 50.0].
    -c --f0-ceil <f0_ceil>                                Ceil of F0 estimation (Hz) [default: 600.0].
    -R --refine-f0                                        F0 refinement.
    -H --harvest                                          Using Harvest to F0 estimation.
    -C --channels-in-octave <channels_in_octave>          I forgot how this option works  [default: 2].
    -v --verbose                                          Show the process in detail.
    -h --help                                             Show this message.
"""

from docopt import docopt
import sys
import os

import pyworld as pw

from misc.util import set_level, logger
from misc.readwrite import load_fseq, write_fseq


def main(raw, frame_shift, sampling_rate, f0_floor, f0_ceil, channels_in_octave, harvest, refine_f0, f0_filepath, ap_filepath, sp_filepath, synthesize_filepath, verbose, **kargs):
    set_level(verbose)

    x = load_fseq(path=raw, vec_size=1, feature_range=range(1)).reshape([-1])
    logger.debug(x.shape)

    if harvest:
        _f0, t = pw.harvest(x,
                            sampling_rate,
                            f0_floor=f0_floor,
                            f0_ceil=f0_ceil,
                            frame_period=frame_shift)
    else:
        _f0, t = pw.dio(x,
                        sampling_rate,
                        f0_floor=f0_floor,
                        f0_ceil=f0_ceil,
                        channels_in_octave=channels_in_octave,
                        frame_period=frame_shift)
    if refine_f0:
        f0 = pw.stonemask(x, _f0, t, sampling_rate)
    else:
        f0 = _f0
    sp = pw.cheaptrick(x, f0, t, sampling_rate)
    ap = pw.d4c(x, f0, t, sampling_rate)

    bname = os.path.splitext(raw)
    if f0_filepath is not None:
        write_fseq(path=f0_filepath, fseq=f0)

    if sp_filepath is not None:
        write_fseq(path=sp_filepath, fseq=sp)

    if ap_filepath is not None:
        write_fseq(path=ap_filepath, fseq=ap)

    if synthesize_filepath is not None:
        y = pw.synthesize(f0, sp, ap, sampling_rate, frame_shift)
        write_fseq(path=synthesize_filepath, fseq=y)


if __name__ == "__main__":
    args = docopt(__doc__)

    main(raw=os.path.abspath(args['<raw>']),
         f0_filepath=os.path.abspath(
             args['--f0-filepath']) if args['--f0-filepath'] is not None else None,
         ap_filepath=os.path.abspath(
             args['--ap-filepath']) if args['--ap-filepath'] is not None else None,
         sp_filepath=os.path.abspath(
             args['--sp-filepath']) if args['--sp-filepath'] is not None else None,
         synthesize_filepath=os.path.abspath(
             args['--resynthesized-filepath']) if args['--resynthesized-filepath'] is not None else None,
         frame_shift=float(args['--frame-shift']),
         sampling_rate=float(args['--sampling-rate']),
         f0_floor=float(args['--f0-floor']),
         f0_ceil=float(args['--f0-ceil']),
         refine_f0=args['--refine-f0'],
         harvest=args['--harvest'],
         channels_in_octave=int(args['--channels-in-octave']),
         verbose=100 if args['--verbose'] else 0)
