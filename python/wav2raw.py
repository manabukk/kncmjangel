# -*- coding: utf-8 -*-
"""Overview:
    Wav2raw.

Usage:
    wav2raw <wav> <raw> [-i] [-v...]
    wav2raw -h 

Options:
    <wav>         hoge.
    <raw>         hoge.
    -i --info     hoge.
    -v --verbose  Show the process in detail.
    -h --help     Show this message.
"""
from docopt import docopt
import sys
import os

import wave
import subprocess

from misc.util import set_level, logger

def main(file, out, info, verbose, **kargs):
    set_level(verbose)

    wav = wave.open(file, mode='rb')
    if info:
        nb_channels = wav.getnchannels()
        sample_size = wav.getsampwidth()
        sampling_rate = wav.getframerate()
        nb_frames = wav.getnframes()
        compress_type = wav.getcompname()

        info_string =  "{:<20}{:>20}\n".format("File name:", file) \
                    + "{:<20}{:>20}\n".format("nb channels:", nb_channels) \
                    + "{:<20}{:>20}\n".format("sample size (bits):", sample_size*8) \
                    + "{:<20}{:>20}\n".format("sampling rate:", sampling_rate) \
                    + "{:<20}{:>20}\n".format("nb frames:", nb_frames) \
                    + "{:<20}{:>20}\n".format("compress type:", compress_type)
        
        print(info_string)
    wav.close()

    query = "sox {} -c 1 -b 32 -e floating-point {}".format(file, out)
    logger.debug(query)
    res = subprocess.getoutput(query)
    logger.debug(res)
    return out

if __name__ == "__main__":
    args = docopt(__doc__)

    main(file=os.path.abspath(args['<wav>']),
         out=os.path.abspath(args['<raw>']),
         info=args['--info'],
         verbose=int(args['--verbose'])
    )