# -*- coding: utf-8 -*-
""" Docstring """
import numpy as np
from misc.util import logger

def get_parallel_data(fseq_list, alignment, num_seq=2):
    """
    docstring here
        :param fseq_list:
        :param alignment:
        :param num_seq=2:
    """
    logger.debug("Alignment length: {}".format(alignment[0]))
    if alignment is None:
        assert len(fseq_list[0]) == len(fseq_list[1])
        aligned_fseqs = np.asarray(
            [[x, y] for x, y in zip(fseq_list[0], fseq_list[1])])
        alignment_len = aligned_fseqs.shape[0]
    else:
        aligned_fseqs = np.asarray([np.asarray(
            [f[frame - 1] for frame in alignment[1 + seq_id::num_seq]]) for seq_id, f in enumerate(fseq_list)])
        alignment_len = alignment[0]
        assert len(
            aligned_fseqs[0]) == alignment_len, "parallel data length error"
        assert len(
            aligned_fseqs[1]) == alignment_len, "parallel data length error"
    return aligned_fseqs, alignment_len


def get_labeled_data(fseq, lseq, label_list=None):
    """
    docstring here
        :param fseq:
        :param lseq:
        :param label_list=None:
    """
    lseq.extend([lseq[-1] for _ in range(len(fseq) - len(lseq))]
                )  # TODO: frame mismatch occur with force alignment by Julius.
    assert len(fseq) == len(
        lseq), "labeled data length error fseq : %d, lseq : %d" % (len(fseq), len(lseq))
    if label_list is not None:
        extracted_fseq = []
        extracted_lseq = []
        for f, l in zip(fseq, lseq):
            if l not in label_list:
                continue
            extracted_fseq.append(f)
            extracted_lseq.append(l)
        fseq = extracted_fseq
        lseq = extracted_lseq
        assert len(fseq) == len(
            lseq), "labeled data length error fseq : %d, lseq : %d" % (len(fseq), len(lseq))
    return [np.asarray(fseq), lseq]
