import matplotlib
# default = list(matplotlib.colors.cnames.keys())
default = ['blue', 'gold', 'darkorange', 'maroon',
           'fuchsia', 'tan', 'deepskyblue', 'silver', 'saddlebrown', 'aqua'] + list(matplotlib.colors.cnames.keys())

pastel = ['#ff7f7f',
          '#7f7fff',
          '#7fff7f',
          '#ffbf7f',
          '#ffff7f',
          '#ff7fff',
          '#bf7fff',
          '#7fffbf',
          '#7fbfff',
          '#bfff7f',
          '#ff7fbf',
          '#7fffff'
]

ro_kyu_bu = [(232 / 255, 151 / 255, 187 / 255),
             (169 / 255, 186 / 255, 222 / 255),
             (222 / 255, 205 / 255, 168 / 255),
             (242 / 255, 189 / 255, 188 / 255),
             (206 / 255, 155 / 255, 134 / 255)]
