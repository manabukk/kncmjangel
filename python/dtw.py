# -*- coding: utf-8 -*-
"""Overview:
    Processing DTW.

Usage:
    dtw <source_file> <target_file> <output_path> [-d <vector_size>] [-f <feature_range>] [-l <local_path>] [-c <constraint_value>] [-A <affine_A_path>] [-b <affine_b_path>] [-v...]
    dtw -h

Options:
    -d --vector-size <vector_size>            Vector size [default: 25].
    -f --feature-range <feature_range>        Feature range [default: 1,25].
    -l --local-path <local_path>              Specifying local path (basically based on SPTK, additional options are implemented.) [default: 2]. 
    -c --constraint-value <constraint_value>  Default is non-constraint. If set a value (int/float), alignment paths which reach its cost to the value in the way are ignored.
    -A --affine-A <affine_A_path>             Path to the parameter of A (t = As +b).
    -b --affine-b <affine_b_path>             Path to the parameter of b (t = As +b).
    -v --verbose                              Show in detail.
    -h --help                                 Show this message.
"""

from docopt import docopt
import sys
import os

from misc.dtw import dtw
from misc.affine import affine_trans
from misc.readwrite import load_fseq, write_alignment, load_matrix, load_vector
from misc.util import logger, set_level


def main(sfp, tfp, ofp, d, frange, verbose, lp=2, constraint=None, affine_A_path=None, affine_b_path=None):

    set_level(verbose)
    sfseq = load_fseq(path=sfp, vec_size=d,
                      feature_range=range(frange[0], frange[1]))
    tfseq = load_fseq(path=tfp, vec_size=d,
                      feature_range=range(frange[0], frange[1]))
    logger.debug("Source features shape: {}".format(str(sfseq.shape)))
    logger.debug("Target features shape: {}".format(str(tfseq.shape)))

    logger.debug("affine A path: {}".format(affine_A_path))
    logger.debug("affine b path: {}".format(affine_b_path))

    if affine_A_path is None or affine_b_path is None:
        pass
    else:
        logger.debug("Affine transformation is applied to source features.")
        A = load_matrix(affine_A_path, len(range(frange[0], frange[1])))
        b = load_vector(affine_b_path)
        sfseq = affine_trans(sfseq, A, b)

    logger.debug("Staring DTW (lp: {})".format(lp))
    align = dtw(x=sfseq, y=tfseq, lp=lp, constraint_value=constraint)
    logger.debug("Alignment length: {}".format(align[0]))

    write_alignment(path=ofp, alignment=align)


if __name__ == '__main__':
    args = docopt(__doc__)

    main(sfp=os.path.abspath(args['<source_file>']),
         tfp=os.path.abspath(args['<target_file>']),
         ofp=os.path.abspath(args['<output_path>']),
         d=int(args['--vector-size']),
         frange=[int(f) for f in args['--feature-range'].split(',')],
         lp=int(args['--local-path']),
         constraint=float(
             args['--constraint-value']) if args['--constraint-value'] is not None else None,
         affine_A_path=os.path.abspath(args['--affine-A']) if args['--affine-A'] is not None else None,
         affine_b_path=os.path.abspath(
             args['--affine-b']) if args['--affine-b'] is not None else None,
         verbose=int(args['--verbose']))

