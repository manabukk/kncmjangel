import sys
sys.path.append("/work/kotani/kncmjAngel/python/")
from misc.readwrite import load_fseq, write_fseq

input_raw_fp = sys.argv[1]
input_f0_fp = sys.argv[2]
output_f0_fp = sys.argv[3]
fs = int(sys.argv[4])
shiftlen = float(sys.argv[5])


import numpy as np

def zero_padding(x_len, fs, shiftlen, f0seq):
    f0_length = int(1000.0 * x_len / fs / shiftlen) + 1
    padded_f0 = np.zeros(shape=(f0_length, 1))
    start_offset = (f0_length - f0seq.shape[0]) // 2
    end_offset = f0_length - f0seq.shape[0] - start_offset
    padded_f0[start_offset:-end_offset] = f0seq
    return padded_f0

raw = load_fseq(path=input_raw_fp, vec_size=1, feature_range=[0])
x_len = raw.shape[0]
# nb_frames = int(1000.0 * x_len / fs / shiftlen) + 1
reaper_f0 = load_fseq(path=input_f0_fp, vec_size=1, feature_range=[0])
padded_f0 = zero_padding(x_len=x_len, fs=fs, shiftlen=shiftlen, f0seq=reaper_f0)

write_fseq(path=output_f0_fp, fseq=padded_f0)