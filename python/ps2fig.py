# -*- coding: utf-8 -*-
# TODO: Not refactored yet

import argparse
import sys
import os
import subprocess
import struct
import numpy as np
from misc.readwrite import load_fseq

import matplotlib


from misc.util import set_level, logger

def main(file, out, fbin, info, verbose, **kargs):
    set_level(verbose)

    if out:
        matplotlib.use('Agg')

    data = np.asarray(load_fseq(path=file, vec_size=fbin, feature_range=range(fbin))).transpose()
    nb_frames = data.shape[1]

    import matplotlib.pyplot as plt
    # import seaborn as sns
    # sns.set_style("whitegrid")

    fig = plt.figure()
    # fig = plt.figure(figsize=(10, 5))

    # heatmap = plt.pcolor(data, cmap=plt.cm.copper)
    heatmap = plt.pcolor(data, cmap=plt.cm.copper)
    fig.colorbar(heatmap)

    # plt.plot(data[100])
    plt.xlabel("Nb frames")
    plt.ylabel("Frequency bins")
    plt.title("File: '{}'".format(file))
    plt.xlim(0, nb_frames)
    plt.ylim(0, fbin)

    plt.draw()
    if out:
        fig.savefig(out)
    else:
        plt.show()

    return ""

if __name__ == "__main__":
    psr = argparse.ArgumentParser(prog=sys.argv[0],
                                  description="Plot power spectrum.",
                                  epilog="Epilog.",
                                  formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                  prefix_chars='-+',
                                  fromfile_prefix_chars='@')
    psr.add_argument('file', type=os.path.abspath)
    psr.add_argument('-o', '--out', type=os.path.abspath, default=None)
    psr.add_argument('-i', '--info', action='store_true')
    psr.add_argument('-f', '--fbin', type=int, required=True)
    psr.add_argument('--verbose', '-v', action='count', default=0)

    psr.set_defaults(func=main)
    args = psr.parse_args()
    message = args.func(**vars(args))
    psr.exit(status=0, message=None)
