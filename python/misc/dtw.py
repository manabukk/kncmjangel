# -*- coding: utf-8 -*-
""" Docstring """
import numpy as np
from scipy.spatial import distance

import misc.fastdp as dp
from misc.util import logger

local_path = {
    1: {'p': 1,
        'path_num': 2,
        'path': [[1, 0],
                 [0, 1]],
        'cost': [1.0,
                 1.0]},
    2: {'p': 2,
        'path_num': 3,
        'path': [[1, 0],
                 [0, 1],
                 [1, 1]],
        'cost': [1.0,
                 1.0,
                 2.0]},
    3: {'p': 3,
        'path_num': 2,
        'path': [[1, 0],
                 [1, 1]],
        'cost': [1.0,
                 2.0]},
    8: {'p': 8,
        'path_num': 2,
        'path': [[0, 1],
                 [1, 1]],
        'cost': [1.0,
                 2.0]}
}


# def dtw_core(x_size, y_size, dist_matrix, lp, constraint_value=None):
#     path_num = local_path[lp]['path_num']
#     path_flatten = [flatten for inner in local_path[lp]['path'] for flatten in inner]
#     cost = local_path[lp]['cost']
#     dist_matrix_flatten = [flatten for inner in dist_matrix for flatten in inner]
#     if constraint_value is None:
#         constraint_value = x_size + y_size
#     alignment = dp.run(x_size, y_size, dist_matrix_flatten, path_num, path_flatten, cost, constraint_value)
#     del dist_matrix_flatten
#     gc.collect()
#     return alignment

def dtw_core(x_size, y_size, x_seq, y_seq, dim, lp, constraint_value=None):
    """
    docstring here
        :param x_size:
        :param y_size:
        :param x_seq:
        :param y_seq:
        :param dim:
        :param lp:
        :param constraint_value=None:
    """
    logger.debug("dtw core")
    assert lp in local_path.keys(), "The local path (p={}). is not implemented. Sorry!".format(lp)
    path_num = local_path[lp]['path_num']
    path_flatten = [flatten for inner in local_path[lp]['path']
                    for flatten in inner]
    cost = local_path[lp]['cost']
    x_seq_flatten = [flatten for inner in x_seq for flatten in inner]
    y_seq_flatten = [flatten for inner in y_seq for flatten in inner]
    if constraint_value is None:
        constraint_value = x_size + y_size  # TODO:
    logger.debug("fastdp will start")
    alignment = dp.run(x_size, y_size, x_seq_flatten, y_seq_flatten, dim,
                       path_num, path_flatten, cost, constraint_value)
    return alignment

def calc_dist_matrix(x, y):
    """
    docstring here
        :param x:
        :param y:
    """
    x_array = np.asarray(x)
    y_array = np.asarray(y)
    # x_tile = np.tile(x_array, (y_array.shape[0], 1, 1))
    # y_tile = np.tile(y_array, (x_array.shape[0], 1, 1))
    # dist_matrix = np.sqrt(
    #     np.square(x_tile.transpose((1, 0, 2)) - y_tile).sum(axis=2))
    dist_matrix = distance.cdist(x_array, y_array)

    # del x_tile
    # del y_tile
    return dist_matrix

def dtw(x, y, lp, constraint_value=None):
    """
    docstring here
        :param x:
        :param y:
        :param lp:
        :param constraint_value=None:
    """
    x_array = np.asarray(x)
    y_array = np.asarray(y)

    # dist_matrix = calc_dist_matrix(x=x_array, y=y_array)

    # alignment = dtw_core(x_size=x_array.shape[0], y_size=y_array.shape[0],
    #                      dist_matrix=dist_matrix, lp=lp, constraint_value=constraint_value)
    alignment = dtw_core(x_size=x_array.shape[0], y_size=y_array.shape[0],
                         x_seq=x_array, y_seq=y_array, dim=x_array.shape[1], lp=lp, constraint_value=constraint_value)

    return alignment
