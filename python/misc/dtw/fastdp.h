#ifndef DTW_CORE_LIBRARY_H
#define DTW_CORE_LIBRARY_H

int* run(const int, const int, const double*, const double*, const int, const int, const int*, const double*, const double);

int is_invalid_path(const int, const int, const int, const int*, const int, const int, const int, const int, const double);

int is_invalid_pos(const int, const int, const int, const int, const int, const int, const double);

double cdist(const int, const int, const double*, const double*, const int);
#endif