# -*- coding=utf-8 -*-
"""Overview:
    Computing outputted parameters of DNN, given source features with VAD.

Usage:
    cpDNNwVAD <model_filepath> <config_modulepath> -s <smcep_filepath> -g <file_spec_idxs> -o <output_prefix> [-d <vector_size>] [-f <feature_range>] [-v...]
    cpDNNwVAD -h

Options:
    <model_filepath>                      Path.
    <config_modulepath>                   Path.
    -s --smcep-filepath <smcep_filepath>  hoge.
    -g --idxs-filepath <file_spec_idxs>   hoge.
    -o --output-prefix <output_prefix>    hoge.
    -d --vector-size <vector_size>        Vector size [default: 25].
    -f --feature-range <feature_range>    Feature range [default: 1,25].
    -v --verbose                          Show in detail.
    -h --help
"""
from docopt import docopt
import os
import sys
sys.path.append(os.path.curdir)

from comet_ml import Optimizer
import torch
import numpy as np

from model import tvlt
from misc.readwrite import load_matfile, load_fseq, write_fseq, load_alignment
from misc.util import set_level, logger, mkdir_p, dynamic_import



def load_model_with_config(model_filepath, config_modulepath, device):
    ExpConfig = dynamic_import(config_modulepath)()
    model_configure = ExpConfig.model_configure
    project_name = ExpConfig.project_name
    opt = Optimizer(model_configure, project_name=project_name,
                    experiment_class="OfflineExperiment", offline_directory="./temp")
    experiment = next(opt.get_experiments())
    model = ExpConfig.get_model(experiment)
    model.load_state_dict(torch.load(model_filepath, map_location=torch.device(device)))
    model.eval()
    model.double()
    model.to(device)
    return model


def main(model_filepath, config_modulepath, smcep_filepath, idxs_filepath, output_prefix, dim, frange, verbose):
    set_level(verbose)

    logger.debug("Path to the DNN model: {}".format(model_filepath))

    if torch.cuda.is_available():
        device = 'cuda'
    else:
        device = 'cpu'

    model = load_model_with_config(model_filepath, config_modulepath, device)

    logger.debug("Path to source mcep: {}".format(smcep_filepath))

    smcep = load_fseq(smcep_filepath, dim, list(range(frange[0], frange[1])))
    v_frames = load_alignment(path=idxs_filepath)

    logger.debug("Shape of smcep: {}".format(smcep.shape))

    params = model.cp(torch.from_numpy(smcep[v_frames, :]).to(device))

    mkdir_p(os.path.dirname(output_prefix))

    for key, value in params.items():
        _p = value.cpu().detach().numpy()
        shape = list(_p.shape)
        shape[0] = smcep.shape[0]
        p = np.zeros(shape=shape)
        print(key)
        print(shape)
        if len(shape) == 1:
            p[v_frames] = _p
        else:
            p[v_frames, :] = _p
        logger.debug("Shape of {}: {}".format(key, p.shape))
        write_fseq(path="{}.{}".format(output_prefix, key), fseq=p.reshape(p.shape[0], -1))

    return


if __name__ == "__main__":
    args = docopt(__doc__)
    main(model_filepath=os.path.abspath(args['<model_filepath>']),
         config_modulepath=args['<config_modulepath>'],
         smcep_filepath=os.path.abspath(args['--smcep-filepath']),
         idxs_filepath=os.path.abspath(args['--idxs-filepath']),
         output_prefix=os.path.abspath(args['--output-prefix']),
         dim=int(args['--vector-size']),
         frange=[int(f) for f in args['--feature-range'].split(',')],
         verbose=int(args['--verbose']))
