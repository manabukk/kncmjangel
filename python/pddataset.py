# -*- coding: utf-8 -*-
"""Overview:
    Building dataset.
    Available dataset classes: TODO. 

Usage:
    pddataset <scp_path> [-d <dataset>] [-o <save_path>] [-u <x_vector_size>] [-w <y_vector_size>] [-f <x_feature_range>] [-g <y_feature_range>] [-i] [-v...]
    pddataset -h | --help

Options:
    <scp_path>                              SCP file path.
    -d --dataset <dataset>                  hoge [default: data.dataset.dataset.ParallelDataDatasetFromSCP].
    -o --save-path <save_path>              Path of output file.
    -u --x-vector-size <x_vector_size>      hoge [default: 25].
    -w --y-vector-size <y_vector_size>      hoge [default: 25].
    -f --x-feature-range <x_feature_range>  hoge [default: 1,25].
    -g --y-feature-range <y_feature_range>  hoge [default: 1,25].
    -i --inverse                            inverse option.
    -v --verbose                            Show verbose message
    -h --help                               Show this screen and exit.
"""

from docopt import docopt

import sys, os

from misc.util import dynamic_import
from data.dataset.dataset import save_dataset

from misc.util import set_level, logger


def main(scp_path, dataset_name, dataset_path, x_vector_size, y_vector_size, x_feature_range, y_feature_range, inverse, verbose):
    set_level(verbose)
    datasetclass = dynamic_import(dataset_name)
    dataset_args = {'scp_path': scp_path,
                    'x_vector_size': x_vector_size,
                    'x_feature_range': x_feature_range,
                    'y_vector_size': y_vector_size, 
                    'y_feature_range': y_feature_range,
                    'inverse': inverse}
    dataset = datasetclass(**dataset_args)
    if dataset_path is not None:
        save_dataset(dataset, dataset_args, dataset_path)

if __name__ == "__main__":
    args = docopt(__doc__)
    main(scp_path=os.path.abspath(args['<scp_path>']) if args['<scp_path>'] is not None else None,
         dataset_name=args['--dataset'],
         dataset_path=os.path.abspath(args['--save-path']) if args['--save-path'] is not None else None,
         x_vector_size=int(args['--x-vector-size']),
         y_vector_size=int(args['--y-vector-size']),
         x_feature_range=[int(f)
                          for f in args['--x-feature-range'].split(',')],
         y_feature_range=[int(f)
                          for f in args['--y-feature-range'].split(',')],
         inverse=args['--inverse'],
         verbose=int(args['--verbose']))
