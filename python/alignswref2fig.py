# -*- coding: utf-8 -*-
"""Overview:
    Plotting an alignment with grid.

Usage:
    alignswref2fig <align_filepaths> -g <x_lab_filepath> -f <y_lab_filepath> -s <frame_shift> [-r <ref_type>] [-o <save_filepath>] [-l <legends>] [-x <x_label>] [-y <y_label>] [-v...]
    alignswref2fig -h

Options:
    <align_filepath>                      File path to the alignment file to be plotted.
    -g --x-lab-filepath <x_lab_filepath>  File path to the label filepath of x-axis.
    -f --y-lab-filepath <y_lab_filepath>  File path to the label filepath of x-axis.
    -s --frame-shift <frame_shift>        Frame shift length [ms].
    -r --ref-type <ref_type>              How to show reference, e.g. 'c'/'circle', 'r'/'rectangle' and 'g'/'grid' [default: r].
    -l --legends <legends>                Must be Comma separated.
    -x --x-label <x_label>                X-axis label. Commas are converted to spaces.
    -y --y-label <y_label>                Y-axis label. Commas are converted to spaces.
    -o --save-filepath <save_filepath>    Path to the output file.
    -v --verbose                          Show the process in detail.
    -h --help                             Show this screen and exit.
"""

from docopt import docopt
import os
from misc.util import set_level, logger, ms2frame
from data.data.lab import Lab

from misc.visualize import alignments_with_ref2fig
from misc.readwrite import load_alignment


def main(align_filepaths, x_lab_filepath, y_lab_filepath, frame_shift, ref_type, legends, x_label, y_label, save_filepath, verbose):
    set_level(verbose)
    logger.debug("Ref type: {}".format(ref_type))
    alignments = [load_alignment(align) for align in align_filepaths]
    if legends is None:
        legends = [os.path.basename(align) for align in align_filepaths]
    else:
        assert len(align_filepaths) == len(legends)
    x_lab = Lab(x_lab_filepath)
    y_lab = Lab(y_lab_filepath)
    logger.debug([ms2frame(1000*l, frame_shift) for l in x_lab.get_grid()])
    logger.debug([ms2frame(1000*l, frame_shift)
                  for l in y_lab.get_grid()])
    alignments_with_ref2fig(alignment_list=alignments,
                   legends=legends,
                   x_label=" ".join(x_label) if x_label else None,
                   y_label=" ".join(y_label) if y_label else None,
                   x_grid=[ms2frame(1000*l, frame_shift) for l in x_lab.get_grid()],
                   y_grid=[ms2frame(1000*l, frame_shift)
                           for l in y_lab.get_grid()],
                   save_filepath=save_filepath,
                   ref_type=ref_type)

if __name__ == "__main__":
    args = docopt(__doc__)
    main(align_filepaths=[os.path.abspath(os.path.expanduser(file)) for file in args['<align_filepaths>'].split(',')],
    # main(align_filepaths=args['<align_filepaths>'],
         x_lab_filepath=os.path.abspath(
             os.path.expanduser(args['--x-lab-filepath'])),
         y_lab_filepath=os.path.abspath(
             os.path.expanduser(args['--y-lab-filepath'])),
        frame_shift=float(args['--frame-shift']),
        ref_type=args['--ref-type'],
         legends=args['--legends'].split(',') if args['--legends'] is not None else None,
         x_label=args['--x-label'].split(
             ',') if args['--x-label'] is not None else None,
         y_label=args['--y-label'].split(',') if args['--y-label'] is not None else None,
         save_filepath=os.path.abspath(args['--save-filepath']) if args['--save-filepath'] is not None else None,
         verbose=int(args['--verbose']))
