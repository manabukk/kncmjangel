# -*- coding=utf-8 -*-
"""Overview:
    Predicting target by a trained model.

Usage:
    predict <model_filepath> <config_modulepath> -s <smcep_filepath> -o <output_filepath> [-d <vector_size>] [-f <feature_range>] [-v...]
    predict -h

Options:
    <model_filepath>                        Path.
    <config_modulepath>                     Path.
    <smcep_filepath>                        hoge.
    -o --output-filepath <output_filepath>  hoge.
    -d --vector-size <vector_size>          Vector size [default: 25].
    -f --feature-range <feature_range>      Feature range [default: 1,25].
    -v --verbose                            Show in detail.
    -h --help
"""
from docopt import docopt
import os, sys
sys.path.append(os.path.curdir)

from misc.util import set_level, logger, mkdir_p, dynamic_import
from misc.readwrite import load_matfile, load_fseq, write_fseq

from comet_ml import Optimizer
import torch

def load_model_with_config(model_filepath, config_modulepath, device):
    ExpConfig = dynamic_import(config_modulepath)()
    model_configure = ExpConfig.model_configure
    project_name = ExpConfig.project_name
    opt = Optimizer(model_configure, project_name=project_name, experiment_class="OfflineExperiment",offline_directory="./temp")
    experiment = next(opt.get_experiments())
    model = ExpConfig.get_model(experiment)
    model.load_state_dict(torch.load(
        model_filepath, map_location=torch.device(device)))
    model.eval()
    model.double()
    
    return model

def main(model_filepath, config_modulepath, smcep_filepath, output_filepath, dim, frange, verbose):
    set_level(verbose)

    logger.debug("Path to the DNN model: {}".format(model_filepath))

    if torch.cuda.is_available():
        device = 'cuda'
    else:
        device = 'cpu'

    model = load_model_with_config(model_filepath, config_modulepath, device)
   
    logger.debug("Path to source mcep: {}".format(smcep_filepath))

    smcep = load_fseq(smcep_filepath, dim, list(range(frange[0], frange[1])))

    logger.debug("Shape of smcep: {}".format(smcep.shape))

    y = model(torch.from_numpy(smcep).to(device))

    y = y.detach().numpy()

    logger.debug("Shape of y: {}".format(y.shape))

    write_fseq(path=output_filepath, fseq=y)

if __name__ == "__main__":
    args = docopt(__doc__)
    main(model_filepath=os.path.abspath(args['<model_filepath>']),
         config_modulepath=args['<config_modulepath>'],
         smcep_filepath=os.path.abspath(args['<smcep_filepath>']),
         output_filepath=os.path.abspath(args['--output-filepath']),
         dim=int(args['--vector-size']),
         frange=[int(f) for f in args['--feature-range'].split(',')],
         verbose=int(args['--verbose']))
