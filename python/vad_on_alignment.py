# -*- coding: utf-8 -*-
"""Overview:
    Processing VAD on alignment.

Usage:
    vad_onalign <alignment_file> <source_file> <target_file> <output_path> [-t <threshold_value>] [-d <vector_size>] [-f <power_index>] [-v...]
    vad_onalign -h

Options:
    <alignment_file>                          hoge.
    <source_file>                             hoge.
    <target_file>                             hoge.
    -t --threshold-value <threshold_value>    Threshold value [default: -8.0].
    -d --vector-size <vector_size>            Vector size [default: 25].
    -f --feature-range <power_index>          Feature range [default: 0].
    -v --verbose                              Show in detail.
    -h --help                                 Show this message.
"""

from docopt import docopt
import sys
import os

from misc.readwrite import load_fseq, write_alignment, load_alignment
from misc.util import logger, set_level

import numpy as np


def get_alignment_applied_VAD(alignment, src_power_seq, tgt_power_seq, threshold=-8.0):

    def VAD(power, threshold):
        return power > threshold

    idx_pairs = np.asarray(alignment[1:]).reshape(-1, 2)

    new_alignment = []
    for idx_pair in idx_pairs:
        src_power = src_power_seq[idx_pair[0]]
        tgt_power = tgt_power_seq[idx_pair[1]]
        if VAD(src_power, threshold) and VAD(tgt_power, threshold):
            new_alignment.append(idx_pair)
        else:
            pass

    len_new_alignment = len(new_alignment)
    new_alignment = np.asarray(new_alignment).reshape(-1)
    new_alignment = new_alignment.tolist()
    return [len_new_alignment] + new_alignment

def main(afp, sfp, tfp, ofp, t, d, frange, verbose):
    set_level(verbose)

    logger.debug("Source mcep file path: {}".format(sfp))
    X = load_fseq(path=sfp, vec_size=d, feature_range=frange)
    logger.debug("Souce mcep (only power) shape: {}".format(X.shape))

    logger.debug("Target mcep file path: {}".format(tfp))
    Y = load_fseq(path=tfp, vec_size=d, feature_range=frange)
    logger.debug("Target mcep (only power) shape: {}".format(Y.shape))

    logger.debug("Alignment file path: {}".format(afp))
    align = load_alignment(path=afp)
    logger.debug("Length of alignment: {}".format(align[0]))

    logger.debug("Threshold value: {}".format(t))
    new_align = get_alignment_applied_VAD(align, X, Y, t)
    logger.debug("Length of new alignment: {}".format(new_align[0]))

    logger.debug("Path to the new alignment: {}".format(ofp))
    write_alignment(path=ofp, alignment=new_align)
    


if __name__ == '__main__':
    args = docopt(__doc__)

    main(
        afp=os.path.abspath(args['<alignment_file>']),
        sfp=os.path.abspath(args['<source_file>']),
         tfp=os.path.abspath(args['<target_file>']),
         ofp=os.path.abspath(args['<output_path>']),
         d=int(args['--vector-size']),
         t=float(args['--threshold-value']),
         frange=[int(f) for f in args['--feature-range'].split(',')],
         verbose=int(args['--verbose']))
