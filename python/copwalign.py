# -*- coding: utf-8 -*-
"""Overview:
    Getting corresponding target frame indexes to the source frame indexes along an alignment.

Usage:
    copwalign <lab_filepath> -a <align_filepath> -s <frame_shift> [-o <output_filepath>] [-t] [-v...]
    copwalign -h

Options:
    <lab_filepath>                          hoge.
    -a --align-filepath <align_filepath>    File path to an alignment file. 
    -s --frame-shift <frame_shift>          Frame shift length [ms] [default: 5].
    -o --output-filepath <output_filepath>  Output filepath
    -t --transpose                          Transposing source/target.
    -v --verbose                            Show the process in detail.
    -h --help                               Show this message.
"""

from docopt import docopt
import sys
import os

from misc.readwrite import load_alignment
from misc.util import logger, set_level, ms2frame, frame2ms
from data.data.lab import Lab


def main(lab_filepath, align_filepath, frame_shift, output_filepath, transpose, verbose):
    set_level(verbose)
    align = load_alignment(path=align_filepath)
    logger.debug("Alignment length: {}".format(align[0]))

    lab = Lab(lab_filepath)
    si_list = [ms2frame(1000*l, frame_shift) for l in lab.get_grid()]
    logger.debug("Source index: {}".format(" ".join([str(si) for si in si_list])))

    ti_list = None

    if transpose:
        # TODO: 特定の source frame に複数の target frame が対応している場合は、先頭のもののみ
        ti_list = [align[1:][align[2::2].index(si)*2] for si in si_list]
    else:
        # TODO: 特定の source frame に複数の target frame が対応している場合は、先頭のもののみ
        ti_list = [align[1:][align[1::2].index(si)*2+1] for si in si_list]

    ttime_list = [frame2ms(ti, frame_shift) / 1000.0 for ti in ti_list]

    if output_filepath is None:
        sys.stdout.write("\n".join([str(tt) for tt in ttime_list]))
    else:
        with open(output_filepath, 'w') as f:
            f.write("\n".join([str(tt) for tt in ttime_list]))


if __name__ == '__main__':
    args = docopt(__doc__)

    main(lab_filepath=os.path.abspath(args['<lab_filepath>']),
         align_filepath=os.path.abspath(args['--align-filepath']),
         frame_shift=float(args['--frame-shift']),
         output_filepath=os.path.abspath(args['--output-filepath']) if args['--output-filepath'] is not None else None,
         transpose=args['--transpose'],
         verbose=int(args['--verbose']))
