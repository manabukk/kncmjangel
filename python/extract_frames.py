# -*- coding: utf-8 -*-
"""Overview:
    Extracting specified frames.

Usage:
    eframes <feature_file> <index_file> <output_file> [-d <vector_size>] [-f <feature_range>] [-v...]
    eframes -h

Options:
    <feature_file>                            hoge.
    <index_file>                              hoge.
    <output_file>                             hoge.
    -d --vector-size <vector_size>            Vector size [default: 25].
    -f --feature-range <feature_range>        Feature range [default: 1,25].
    -v --verbose                              Show in detail.
    -h --help                                 Show this message.
"""

from docopt import docopt
import sys
import os

from misc.readwrite import load_fseq, load_alignment, write_fseq
from misc.util import logger, set_level

import numpy as np


def main(feature_fp, index_fp, output_fp, d, frange, verbose):
    set_level(verbose)

    logger.debug("Feature file path: {}".format(feature_fp))
    X = load_fseq(path=feature_fp, vec_size=d, feature_range=list(range(frange[0], frange[1])))
    logger.debug("Feature sequence shape: {}".format(X.shape))

    logger.debug("Path to idx file: {}".format(index_fp))
    idxs = load_alignment(index_fp)
    logger.debug("Size of idxs: {}".format(len(idxs)))

    logger.debug("Path to the new alignment: {}".format(output_fp))
    write_fseq(path=output_fp, fseq=X[idxs, :])


if __name__ == '__main__':
    args = docopt(__doc__)

    main(
         feature_fp=os.path.abspath(args['<feature_file>']),
         index_fp=os.path.abspath(args['<index_file>']),
         output_fp=os.path.abspath(args['<output_file>']),
         d=int(args['--vector-size']),
         frange=[int(f) for f in args['--feature-range'].split(',')],
         verbose=int(args['--verbose']))
