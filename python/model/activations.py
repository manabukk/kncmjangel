# -*- coding: utf-8 -*-

from misc.util import dynamic_import

class MyActivations(object):
    """
    docstring here
        :param object:
    """
    def __new__(cls, name):

        def linear(x):
            return x
            
        if name in locals():
            return locals()[name]
        
        if name in ('sigmoid', 'tanh'):
            active_fn = dynamic_import("torch." + name)
        else:
            active_fn = dynamic_import("torch.nn." + name)()

        return active_fn
