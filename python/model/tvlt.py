# -*- coding: utf-8 -*-

import torch
from torch import nn
from torch.nn import functional as F

from model.model import KNCNNModel
from model.activations import MyActivations
from misc.mlpg import static2staticdelta, staticdelta2static, cmpt_var_Y, get_window_matrix


class NNTVLT(KNCNNModel):

    def __init__(self,
                 nb_shared_layers,
                 nb_shared_units,
                 nb_matrix_layers,
                 nb_matrix_units,
                 nb_bias_layers,
                 nb_bias_units,
                 activfn,
                 output_activfn,
                 input_dim,
                 output_dim,
                 latent_dim=None,
                 ):
        super(NNTVLT, self).__init__()

        # matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
        #     nb_shared_units, nb_matrix_units) for i in range(nb_matrix_layers)]
        # matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

        # bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
        #     nb_shared_units, nb_bias_units) for i in range(nb_bias_layers)]
        # bias_fc.append(nn.Linear(nb_bias_units, output_dim))

        if nb_shared_layers > 0:
            shared_fc = [nn.Linear(nb_shared_units, nb_shared_units) if i > 0 else nn.Linear(
                input_dim, nb_shared_units) for i in range(nb_shared_layers - 1)]
            if latent_dim is None:
                latent_dim = nb_shared_units
            shared_fc.append(nn.Linear(nb_shared_units, latent_dim))
            self.shared_fc = nn.ModuleList(shared_fc)

            if nb_matrix_layers == 1:
                matrix_fc = [nn.Linear(latent_dim, output_dim**2)]
            else:
                matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                    latent_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]
                matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            if nb_bias_layers == 1:
                bias_fc = [nn.Linear(latent_dim, output_dim)]
            else:
                bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                    latent_dim, nb_bias_units) for i in range(nb_bias_layers - 1)]
                bias_fc.append(nn.Linear(nb_bias_units, output_dim))

            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)

        else:
            self.shared_fc = None

            matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                input_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]

            matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                input_dim, nb_bias_units) for i in range(nb_bias_layers - 1)]
            bias_fc.append(nn.Linear(nb_bias_units, output_dim))


            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)

        self.activfn = MyActivations(activfn)
        self.output_activfn = MyActivations(output_activfn)

        self.input_dim = input_dim
        self.output_dim = output_dim
        self.latent_dim = latent_dim

    def forward(self, x):
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](x))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = x
            
        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](h))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # bias-subnetwork
        if len(self.bias_fc) == 1:
            bias = self.output_activfn(self.bias_fc[0](z))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-1]):
                h = self.activfn(fc(h))
            bias = self.output_activfn(self.bias_fc[-1](h))

        matrix = torch.reshape(matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))
        x = x.unsqueeze(2)
        bias = bias.unsqueeze(2)

        predict = torch.matmul(matrix, x) + bias

        predict = predict.squeeze()

        return predict
        # return bias.squeeze()
        # return matrix.reshape((matrix.shape[0], matrix.shape[1]*matrix.shape[2])).squeeze()

    def optimize(self, X, Y, optimizer, loss_fn):
        optimizer.zero_grad()
        predict_Y = self(X)
        loss = self.loss_fn(predict_Y, Y, loss_fn)
        loss.backward()
        optimizer.step()
        return loss.item()

    def loss_fn(self, predict_Y, Y, loss_fn):
        return loss_fn(predict_Y, Y)

    def cp(self, x):
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](x))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = x

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](h))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # bias-subnetwork
        if len(self.bias_fc) == 1:
            bias = self.output_activfn(self.bias_fc[0](z))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-1]):
                h = self.activfn(fc(h))
            bias = self.output_activfn(self.bias_fc[-1](h))

        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))
        bias = bias.unsqueeze(2)

        return {"A": matrix, "b": bias}


class NNSpecDiff(KNCNNModel):

    def __init__(self,
                 nb_layers,
                 nb_units,
                 activfn,
                 output_activfn,
                 input_dim,
                 output_dim,
                 ):
        super(NNSpecDiff, self).__init__()

        fc = [nn.Linear(nb_units, nb_units) if i > 0 else nn.Linear(
            input_dim, nb_units) for i in range(nb_layers - 1)]
        fc.append(nn.Linear(nb_units, output_dim))
        self.fc = nn.ModuleList(fc)

        self.activfn = MyActivations(activfn)
        self.output_activfn = MyActivations(output_activfn)

        self.input_dim = input_dim
        self.output_dim = output_dim

    def forward(self, x):
        h = self.activfn(self.fc[0](x))
        for i, fc in enumerate(self.fc[1:-1]):
            h = self.activfn(fc(h))
        bias = self.output_activfn(self.fc[-1](h))

        x = x.unsqueeze(2)
        bias = bias.unsqueeze(2)

        predict = x + bias

        predict = predict.squeeze()

        return predict

    def optimize(self, X, Y, optimizer, loss_fn):
        optimizer.zero_grad()
        predict_Y = self(X)
        loss = self.loss_fn(predict_Y, Y, loss_fn)
        loss.backward()
        optimizer.step()
        return loss.item()

    def loss_fn(self, predict_Y, Y, loss_fn):
        return loss_fn(predict_Y, Y)

    def cp(self, x):
        h = self.activfn(self.fc[0](x))
        for i, fc in enumerate(self.fc[1:-1]):
            h = self.activfn(fc(h))
        bias = self.output_activfn(self.fc[-1](h))

        bias = bias.unsqueeze(2)

        return {"b": bias}


class NNSpecDiffMLPG(KNCNNModel):

    def __init__(self,
                 nb_layers,
                 nb_units,
                 activfn,
                 output_activfn,
                 input_dim,
                 output_dim,
                 train_dataset_path,
                 delta_num=1
                 ):
        super(NNSpecDiffMLPG, self).__init__()

        fc = [nn.Linear(nb_units, nb_units) if i > 0 else nn.Linear(
            input_dim*2, nb_units) for i in range(nb_layers - 1)]
        fc.append(nn.Linear(nb_units, output_dim*2))
        self.fc = nn.ModuleList(fc)

        self.activfn = MyActivations(activfn)
        self.output_activfn = MyActivations(output_activfn)

        self.input_dim = input_dim
        self.output_dim = output_dim

        self.train_dataset_path = train_dataset_path
        self.var_Y = cmpt_var_Y(train_dataset_path)

        self.delta_num = delta_num

    def forward(self, x):
        W = torch.from_numpy(get_window_matrix(
            delta_num=self.delta_num, sequence_length=x.size(0)))
        X = static2staticdelta(x, delta_num=self.delta_num, W=W)

        h = self.activfn(self.fc[0](X))
        for i, fc in enumerate(self.fc[1:-1]):
            h = self.activfn(fc(h))
        outputs = self.output_activfn(self.fc[-1](h))

        bias = outputs[:,:self.output_dim]

        x = x.unsqueeze(2)
        bias = bias.unsqueeze(2)
        statics = x + bias

        dynamics = outputs[:, self.output_dim:]
        dynamics = dynamics.unsqueeze(2)

        predict = torch.cat([statics, dynamics], dim=1)
        predict = predict.squeeze()

        y = staticdelta2static(predict, self.var_Y,
                               delta_num=self.delta_num, W=W)
        return y

    def optimize(self, X, Y, optimizer, loss_fn):
        optimizer.zero_grad()
        predict_Y = self(X)
        loss = self.loss_fn(predict_Y, Y, loss_fn)
        loss.backward()
        optimizer.step()
        return loss.item()

    def loss_fn(self, predict_Y, Y, loss_fn):
        return loss_fn(predict_Y, Y)

    def cp(self, x):
        h = self.activfn(self.fc[0](x))
        for i, fc in enumerate(self.fc[1:-1]):
            h = self.activfn(fc(h))
        bias = self.output_activfn(self.fc[-1](h))

        bias = bias.unsqueeze(2)

        return {"b": bias}


class NNTVLTwReg(KNCNNModel):

    def __init__(self,
                 nb_shared_layers,
                 nb_shared_units,
                 nb_matrix_layers,
                 nb_matrix_units,
                 nb_bias_layers,
                 nb_bias_units,
                 activfn,
                 output_activfn,
                 input_dim,
                 output_dim,
                 latent_dim=None,
                 ):
        super(NNTVLTwReg, self).__init__()

        # matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
        #     nb_shared_units, nb_matrix_units) for i in range(nb_matrix_layers)]
        # matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

        # bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
        #     nb_shared_units, nb_bias_units) for i in range(nb_bias_layers)]
        # bias_fc.append(nn.Linear(nb_bias_units, output_dim))

        if nb_shared_layers > 0:
            shared_fc = [nn.Linear(nb_shared_units, nb_shared_units) if i > 0 else nn.Linear(
                input_dim, nb_shared_units) for i in range(nb_shared_layers - 1)]
            if latent_dim is None:
                latent_dim = nb_shared_units
            shared_fc.append(nn.Linear(nb_shared_units, latent_dim))
            self.shared_fc = nn.ModuleList(shared_fc)

            if nb_matrix_layers == 1:
                matrix_fc = [nn.Linear(latent_dim, output_dim**2)]
            else:
                matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                    latent_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]
                matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            if nb_bias_layers == 1:
                bias_fc = [nn.Linear(latent_dim, output_dim)]
            else:
                bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                    latent_dim, nb_bias_units) for i in range(nb_bias_layers - 1)]
                bias_fc.append(nn.Linear(nb_bias_units, output_dim))

            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)

        else:
            self.shared_fc = None

            matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                input_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]

            matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                input_dim, nb_bias_units) for i in range(nb_bias_layers - 1)]
            bias_fc.append(nn.Linear(nb_bias_units, output_dim))

            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)

        self.activfn = MyActivations(activfn)
        self.output_activfn = MyActivations(output_activfn)

        self.input_dim = input_dim
        self.output_dim = output_dim
        self.latent_dim = latent_dim

    def forward(self, x):
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](x))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = x

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](h))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # bias-subnetwork
        if len(self.bias_fc) == 1:
            bias = self.output_activfn(self.bias_fc[0](z))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-1]):
                h = self.activfn(fc(h))
            bias = self.output_activfn(self.bias_fc[-1](h))

        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))
        x = x.unsqueeze(2)
        bias = bias.unsqueeze(2)

        predict = torch.matmul(matrix, x) + bias

        predict = predict.squeeze()

        return {'predict_Y': predict, 'matrix': matrix}
        # return bias.squeeze()
        # return matrix.reshape((matrix.shape[0], matrix.shape[1]*matrix.shape[2])).squeeze()

    def optimize(self, X, Y, optimizer, loss_fn):
        optimizer.zero_grad()
        predict_Y = self(X)
        loss = self.loss_fn(predict_Y, Y, loss_fn)
        loss.backward()
        optimizer.step()
        return loss.item()

    def loss_fn(self, predict_Y, Y, loss_fn):
        return loss_fn(predict_Y, Y)

    def cp(self, x):
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](x))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = x

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](h))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # bias-subnetwork
        if len(self.bias_fc) == 1:
            bias = self.output_activfn(self.bias_fc[0](z))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-1]):
                h = self.activfn(fc(h))
            bias = self.output_activfn(self.bias_fc[-1](h))

        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))
        bias = bias.unsqueeze(2)

        return {"A": matrix, "b": bias}


class NNTVLTwRegTMPT(KNCNNModel):

    def __init__(self,
                 nb_shared_layers,
                 nb_shared_units,
                 nb_matrix_layers,
                 nb_matrix_units,
                 nb_bias_layers,
                 nb_bias_units,
                 activfn,
                 output_activfn,
                 input_dim,
                 output_dim,
                 latent_dim=None,
                 nb_templates=16,
                 ):
        super(NNTVLTwRegTMPT, self).__init__()

        # matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
        #     nb_shared_units, nb_matrix_units) for i in range(nb_matrix_layers)]
        # matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

        # bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
        #     nb_shared_units, nb_bias_units) for i in range(nb_bias_layers)]
        # bias_fc.append(nn.Linear(nb_bias_units, output_dim))

        if nb_shared_layers > 0:
            shared_fc = [nn.Linear(nb_shared_units, nb_shared_units) if i > 0 else nn.Linear(
                input_dim, nb_shared_units) for i in range(nb_shared_layers - 1)]
            if latent_dim is None:
                latent_dim = nb_shared_units
            shared_fc.append(nn.Linear(nb_shared_units, latent_dim))
            self.shared_fc = nn.ModuleList(shared_fc)

            if nb_matrix_layers == 1:
                matrix_fc = [nn.Linear(latent_dim, output_dim**2)]
            else:
                matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                    latent_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]
                matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            if nb_bias_layers <= 1:
                raise(NotImplementedError())
                bias_fc = [nn.Linear(latent_dim, nb_templates)]
            elif nb_bias_layers > 2:
                bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                    latent_dim, nb_bias_units) for i in range(nb_bias_layers - 2)]
                bias_fc.append(nn.Linear(nb_bias_units, nb_templates))
                bias_fc.append(nn.Linear(nb_templates, output_dim))
            else:
                bias_fc = [nn.Linear(latent_dim, nb_templates)]
                bias_fc.append(nn.Linear(nb_templates, output_dim))

            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)

        else:
            if nb_matrix_layers == 1:
                matrix_fc = [nn.Linear(input_dim, output_dim**2)]
            else:
                matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                    input_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]
                matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            if nb_bias_layers <= 1:
                raise(NotImplementedError())
                bias_fc = [nn.Linear(input_dim, nb_templates)]
            elif nb_bias_layers > 2:
                bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                    input_dim, nb_bias_units) for i in range(nb_bias_layers - 2)]
                bias_fc.append(nn.Linear(nb_bias_units, nb_templates))
                bias_fc.append(nn.Linear(nb_templates, output_dim))
            else:
                bias_fc = [nn.Linear(input_dim, nb_templates)]
                bias_fc.append(nn.Linear(nb_templates, output_dim))

            self.shared_fc = None
            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)

        self.activfn = MyActivations(activfn)
        self.output_activfn = MyActivations(output_activfn)

        self.input_dim = input_dim
        self.output_dim = output_dim
        self.latent_dim = latent_dim

        self.softmax = nn.Softmax(1)

    def forward(self, x):
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](x))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = x

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](h))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # bias-subnetwork
        if len(self.bias_fc) == 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2:
            h = self.softmax(self.bias_fc[0](z))
            bias = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-2]):
                h = self.activfn(fc(h))
            h = self.softmax(self.bias_fc[-2](h))
            bias = self.output_activfn(self.bias_fc[-1](h))

        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))
        x = x.unsqueeze(2)
        bias = bias.unsqueeze(2)

        predict = torch.matmul(matrix, x) + bias

        predict = predict.squeeze()

        return predict
        # return bias.squeeze()
        # return matrix.reshape((matrix.shape[0], matrix.shape[1]*matrix.shape[2])).squeeze()

    def optimize(self, X, Y, optimizer, loss_fn):
        optimizer.zero_grad()
        predict_Y = self(X)
        loss = self.loss_fn(predict_Y, Y, loss_fn)
        loss.backward()
        optimizer.step()
        return loss.item()

    def loss_fn(self, predict_Y, Y, loss_fn):
        return loss_fn(predict_Y, Y)

    def cp(self, x):
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](x))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = x

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](h))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # bias-subnetwork
        if len(self.bias_fc) == 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2:
            h = self.softmax(self.bias_fc[0](z))
            bias = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-2]):
                h = self.activfn(fc(h))
            h = self.softmax(self.bias_fc[-2](h))
            bias = self.output_activfn(self.bias_fc[-1](h))

        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))
        bias = bias.unsqueeze(2)

        return {"A": matrix, "b": bias}


class NNTVLTwDBias(KNCNNModel):

    def __init__(self,
                 nb_shared_layers,
                 nb_shared_units,
                 nb_matrix_layers,
                 nb_matrix_units,
                 nb_bias_layers,
                 nb_bias_units,
                 activfn,
                 output_activfn,
                 input_dim,
                 output_dim,
                 latent_dim=None,
                 nb_templates=16,
                 ):
        super(NNTVLTwDBias, self).__init__()

        if nb_shared_layers > 0:
            shared_fc = [nn.Linear(nb_shared_units, nb_shared_units) if i > 0 else nn.Linear(
                input_dim, nb_shared_units) for i in range(nb_shared_layers - 1)]
            if latent_dim is None:
                latent_dim = nb_shared_units
            shared_fc.append(nn.Linear(nb_shared_units, latent_dim))
            self.shared_fc = nn.ModuleList(shared_fc)

            if nb_matrix_layers == 1:
                matrix_fc = [nn.Linear(latent_dim, output_dim**2)]
            else:
                matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                    latent_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]
                matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            if nb_bias_layers <= 1:
                raise(NotImplementedError())
                bias_fc = [nn.Linear(latent_dim, nb_templates)]
            elif nb_bias_layers > 2:
                bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                    latent_dim, nb_bias_units) for i in range(nb_bias_layers - 2)]
                bias_fc.append(nn.Linear(nb_bias_units, nb_templates))
                bias_fc.append(nn.Linear(nb_templates, output_dim))
                bias_fc.append(nn.Linear(nb_templates, output_dim))
            else:
                bias_fc = [nn.Linear(latent_dim, nb_templates)]
                bias_fc.append(nn.Linear(nb_templates, output_dim))
                bias_fc.append(nn.Linear(nb_templates, output_dim))

            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)

        else:
            raise(NotImplementedError())
            self.shared_fc = None

            matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                input_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]

            matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                input_dim, nb_bias_units) for i in range(nb_bias_layers - 1)]
            bias_fc.append(nn.Linear(nb_bias_units, output_dim))

            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)

        self.activfn = MyActivations(activfn)
        self.output_activfn = MyActivations(output_activfn)

        self.input_dim = input_dim
        self.output_dim = output_dim
        self.latent_dim = latent_dim

        self.softmax = nn.Softmax(1)

    def forward(self, x):
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](x))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = x

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](h))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # bias-subnetwork
        if len(self.bias_fc) <= 1 + 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2 + 1:
            h = self.softmax(self.bias_fc[0](z))
            bias_src = self.output_activfn(self.bias_fc[-2](h))
            bias_tgt = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-3]):
                h = self.activfn(fc(h))
            h = self.softmax(self.bias_fc[-3](h))
            bias_src = self.output_activfn(self.bias_fc[-2](h))
            bias_tgt = self.output_activfn(self.bias_fc[-1](h))

        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))
        x = x.unsqueeze(2)
        bias_src = bias_src.unsqueeze(2)
        bias_tgt = bias_tgt.unsqueeze(2)

        predict = torch.matmul(matrix, (x - bias_src)) + bias_tgt

        predict = predict.squeeze()

        return predict
        # return bias.squeeze()
        # return matrix.reshape((matrix.shape[0], matrix.shape[1]*matrix.shape[2])).squeeze()

    def optimize(self, X, Y, optimizer, loss_fn):
        optimizer.zero_grad()
        predict_Y = self(X)
        loss = self.loss_fn(predict_Y, Y, loss_fn)
        loss.backward()
        optimizer.step()
        return loss.item()

    def loss_fn(self, predict_Y, Y, loss_fn):
        return loss_fn(predict_Y, Y)

    def cp(self, x):
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](x))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = x

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](h))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # bias-subnetwork
        if len(self.bias_fc) <= 1 + 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2 + 1:
            h = self.softmax(self.bias_fc[0](z))
            bias_src = self.output_activfn(self.bias_fc[-2](h))
            bias_tgt = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-3]):
                h = self.activfn(fc(h))
            h = self.softmax(self.bias_fc[-3](h))
            bias_src = self.output_activfn(self.bias_fc[-2](h))
            bias_tgt = self.output_activfn(self.bias_fc[-1](h))

        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))
        bias_src = bias_src.unsqueeze(2)
        bias_tgt = bias_tgt.unsqueeze(2)

        return {"A": matrix, "b_src": bias_src, "b_tgt": bias_tgt}


class NNTVLTwDBiasforBD(KNCNNModel):

    def __init__(self,
                 nb_shared_layers,
                 nb_shared_units,
                 nb_matrix_layers,
                 nb_matrix_units,
                 nb_bias_layers,
                 nb_bias_units,
                 activfn,
                 output_activfn,
                 input_dim,
                 output_dim,
                 latent_dim=None,
                 nb_templates=16,
                 ):
        super(NNTVLTwDBiasforBD, self).__init__()

        if nb_shared_layers > 0:
            shared_fc = [nn.Linear(nb_shared_units, nb_shared_units) if i > 0 else nn.Linear(
                input_dim, nb_shared_units) for i in range(nb_shared_layers - 1)]
            if latent_dim is None:
                latent_dim = nb_shared_units
            shared_fc.append(nn.Linear(nb_shared_units, latent_dim))
            self.shared_fc = nn.ModuleList(shared_fc)

            if nb_matrix_layers == 1:
                matrix_fc = [nn.Linear(latent_dim, output_dim**2)]
            else:
                matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                    latent_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]
                matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            if nb_bias_layers <= 1:
                raise(NotImplementedError())
                bias_fc = [nn.Linear(latent_dim, nb_templates)]
            elif nb_bias_layers > 2:
                bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                    latent_dim, nb_bias_units) for i in range(nb_bias_layers - 2)]
                bias_fc.append(nn.Linear(nb_bias_units, nb_templates))
                bias_fc.append(nn.Linear(nb_templates, output_dim))
                bias_fc.append(nn.Linear(nb_templates, output_dim))
            else:
                bias_fc = [nn.Linear(latent_dim, nb_templates)]
                bias_fc.append(nn.Linear(nb_templates, output_dim))
                bias_fc.append(nn.Linear(nb_templates, output_dim))

            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)

        else:
            raise(NotImplementedError())
            self.shared_fc = None

            matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                input_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]

            matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                input_dim, nb_bias_units) for i in range(nb_bias_layers - 1)]
            bias_fc.append(nn.Linear(nb_bias_units, output_dim))

            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)

        self.activfn = MyActivations(activfn)
        self.output_activfn = MyActivations(output_activfn)

        self.input_dim = input_dim
        self.output_dim = output_dim
        self.latent_dim = latent_dim

        self.softmax = nn.Softmax(1)

    def forward(self, x):
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](x))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = x

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](h))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # bias-subnetwork
        if len(self.bias_fc) <= 1 + 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2 + 1:
            h = self.softmax(self.bias_fc[0](z))
            bias_src = self.output_activfn(self.bias_fc[-2](h))
            bias_tgt = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-3]):
                h = self.activfn(fc(h))
            h = self.softmax(self.bias_fc[-3](h))
            bias_src = self.output_activfn(self.bias_fc[-2](h))
            bias_tgt = self.output_activfn(self.bias_fc[-1](h))

        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))
        x = x.unsqueeze(2)
        bias_src = bias_src.unsqueeze(2)
        bias_tgt = bias_tgt.unsqueeze(2)

        predict = torch.matmul(matrix, (x - bias_src)) + bias_tgt

        predict = predict.squeeze()

        return {"y": predict, "A": matrix, "b_src": bias_src, "b_tgt": bias_tgt}
        # return bias.squeeze()
        # return matrix.reshape((matrix.shape[0], matrix.shape[1]*matrix.shape[2])).squeeze()

    def optimize(self, X, Y, optimizer, loss_fn):
        optimizer.zero_grad()
        predict_Y = self(X)
        loss = self.loss_fn(predict_Y, Y, loss_fn)
        loss.backward()
        optimizer.step()
        return loss.item()

    def loss_fn(self, predict_Y, Y, loss_fn):
        return loss_fn(predict_Y, Y)

    def cp(self, x):
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](x))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = x

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](h))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # bias-subnetwork
        if len(self.bias_fc) <= 1 + 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2 + 1:
            h = self.softmax(self.bias_fc[0](z))
            bias_src = self.output_activfn(self.bias_fc[-2](h))
            bias_tgt = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-3]):
                h = self.activfn(fc(h))
            h = self.softmax(self.bias_fc[-3](h))
            bias_src = self.output_activfn(self.bias_fc[-2](h))
            bias_tgt = self.output_activfn(self.bias_fc[-1](h))

        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))
        bias_src = bias_src.unsqueeze(2)
        bias_tgt = bias_tgt.unsqueeze(2)

        return {"A": matrix, "b_src": bias_src, "b_tgt": bias_tgt}



def get_VTLNLayer(input_dim, nb_layers, nb_units):
    if nb_layers == 1:
        layers = [nn.Linear(input_dim, 1)]
    else:
        layers = [nn.Linear(nb_units, nb_units) if i > 0 else nn.Linear(
            input_dim, nb_units) for i in range(nb_layers - 1)]
        layers.append(nn.Linear(nb_units, 1))
    return layers


def get_batchA(batch_alpha, dim):
    import math
    batch_size = batch_alpha.size(0)
    A = []
    for i in range(dim):
        for j in range(dim):
            coeff = 1. / math.factorial(j)
            m0 = max([0, j-i])
            aij = 0.
            for m in range(m0, j+1):
                first_term = math.factorial(
                    j+1) // (math.factorial(j+1 - m) * math.factorial(m)) if j+1 >= m else 0
                second_term = math.factorial(m+i) / math.factorial(m+i-j)
                second_term *= coeff
                third_term = (-1 * batch_alpha)**(m+i-j) * batch_alpha**m
                each_elem = first_term * second_term * third_term
                aij += each_elem
            A.append(aij)
    A = torch.cat(A).reshape(dim, dim, batch_size)
    A = A.transpose(1, 2).transpose(0, 1)
    return A


class NNTVLTwDBiasforBD2(KNCNNModel):

    def __init__(self,
                 nb_shared_layers,
                 nb_shared_units,
                 nb_matrix_layers,
                 nb_matrix_units,
                 nb_bias_layers,
                 nb_bias_units,
                 activfn,
                 output_activfn,
                 input_dim,
                 output_dim,
                 latent_dim=None,
                 ):
        super(NNTVLTwDBiasforBD2, self).__init__()

        if nb_shared_layers > 0:
            shared_fc = [nn.Linear(nb_shared_units, nb_shared_units) if i > 0 else nn.Linear(
                input_dim, nb_shared_units) for i in range(nb_shared_layers - 1)]
            if latent_dim is None:
                latent_dim = nb_shared_units
            shared_fc.append(nn.Linear(nb_shared_units, latent_dim))
            self.shared_fc = nn.ModuleList(shared_fc)

            if nb_matrix_layers == 1:
                matrix_fc = [nn.Linear(latent_dim, output_dim**2)]
            else:
                matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                    latent_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]
                matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            if nb_bias_layers <= 1:
                raise(NotImplementedError())
                bias_fc = [nn.Linear(latent_dim, nb_templates)]
            elif nb_bias_layers > 2:
                bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                    latent_dim, nb_bias_units) for i in range(nb_bias_layers - 1)]
                bias_fc.append(nn.Linear(nb_bias_units, output_dim))
                bias_fc.append(nn.Linear(nb_bias_units, output_dim))
            else:
                bias_fc = [nn.Linear(latent_dim, nb_bias_units)]
                bias_fc.append(nn.Linear(nb_bias_units, output_dim))
                bias_fc.append(nn.Linear(nb_bias_units, output_dim))

            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)

        else:
            raise(NotImplementedError())
            self.shared_fc = None

            matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                input_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]

            matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                input_dim, nb_bias_units) for i in range(nb_bias_layers - 1)]
            bias_fc.append(nn.Linear(nb_bias_units, output_dim))

            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)

        self.activfn = MyActivations(activfn)
        self.output_activfn = MyActivations(output_activfn)

        self.input_dim = input_dim
        self.output_dim = output_dim
        self.latent_dim = latent_dim

    def forward(self, x):
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](x))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = x

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](h))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # bias-subnetwork
        if len(self.bias_fc) <= 1 + 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2 + 1:
            h = self.activfn(self.bias_fc[0](z))
            bias_src = self.output_activfn(self.bias_fc[-2](h))
            bias_tgt = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-3]):
                h = self.activfn(fc(h))
            h = self.activfn(self.bias_fc[-3](h))
            bias_src = self.output_activfn(self.bias_fc[-2](h))
            bias_tgt = self.output_activfn(self.bias_fc[-1](h))

        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))
        x = x.unsqueeze(2)
        bias_src = bias_src.unsqueeze(2)
        bias_tgt = bias_tgt.unsqueeze(2)

        predict = torch.matmul(matrix, (x - bias_src)) + bias_tgt

        predict = predict.squeeze()

        return {"y": predict, "A": matrix, "b_src": bias_src, "b_tgt": bias_tgt}
        # return bias.squeeze()
        # return matrix.reshape((matrix.shape[0], matrix.shape[1]*matrix.shape[2])).squeeze()

    def optimize(self, X, Y, optimizer, loss_fn):
        optimizer.zero_grad()
        predict_Y = self(X)
        loss = self.loss_fn(predict_Y, Y, loss_fn)
        loss.backward()
        optimizer.step()
        return loss.item()

    def loss_fn(self, predict_Y, Y, loss_fn):
        return loss_fn(predict_Y, Y)

    def cp(self, x):
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](x))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = x

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](h))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # bias-subnetwork
        if len(self.bias_fc) <= 1 + 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2 + 1:
            h = self.activfn(self.bias_fc[0](z))
            bias_src = self.output_activfn(self.bias_fc[-2](h))
            bias_tgt = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-3]):
                h = self.activfn(fc(h))
            h = self.activfn(self.bias_fc[-3](h))
            bias_src = self.output_activfn(self.bias_fc[-2](h))
            bias_tgt = self.output_activfn(self.bias_fc[-1](h))

        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))
        bias_src = bias_src.unsqueeze(2)
        bias_tgt = bias_tgt.unsqueeze(2)

        return {"A": matrix, "b_src": bias_src, "b_tgt": bias_tgt}


def get_VTLNLayer(input_dim, nb_layers, nb_units):
    if nb_layers == 1:
        layers = [nn.Linear(input_dim, 1)]
    else:
        layers = [nn.Linear(nb_units, nb_units) if i > 0 else nn.Linear(
            input_dim, nb_units) for i in range(nb_layers - 1)]
        layers.append(nn.Linear(nb_units, 1))
    return layers


def get_batchA(batch_alpha, dim):
    import math
    batch_size = batch_alpha.size(0)
    A = []
    for i in range(dim):
        for j in range(dim):
            coeff = 1. / math.factorial(j)
            m0 = max([0, j-i])
            aij = 0.
            for m in range(m0, j+1):
                first_term = math.factorial(
                    j+1) // (math.factorial(j+1 - m) * math.factorial(m)) if j+1 >= m else 0
                second_term = math.factorial(m+i) / math.factorial(m+i-j)
                second_term *= coeff
                third_term = (-1 * batch_alpha)**(m+i-j) * batch_alpha**m
                each_elem = first_term * second_term * third_term
                aij += each_elem
            A.append(aij)
    A = torch.cat(A).reshape(dim, dim, batch_size)
    A = A.transpose(1, 2).transpose(0, 1)
    return A



class NNTVLTAlphawDBias(KNCNNModel):

    def __init__(self,
                 nb_shared_layers,
                 nb_shared_units,
                 nb_matrix_layers,
                 nb_matrix_units,
                 nb_bias_layers,
                 nb_bias_units,
                 nb_vtln_layers,
                 nb_vtln_units,
                 activfn,
                 output_activfn,
                 input_dim,
                 output_dim,
                 latent_dim=None,
                 nb_templates=16,
                 lim_scale=0.3,
                 only_alpha=False,
                 ):
        super(NNTVLTAlphawDBias, self).__init__()

        if nb_shared_layers > 0:
            shared_fc = [nn.Linear(nb_shared_units, nb_shared_units) if i > 0 else nn.Linear(
                input_dim, nb_shared_units) for i in range(nb_shared_layers - 1)]
            if latent_dim is None:
                latent_dim = nb_shared_units
            shared_fc.append(nn.Linear(nb_shared_units, latent_dim))
            self.shared_fc = nn.ModuleList(shared_fc)

            if nb_matrix_layers == 1:
                raise(NotImplementedError())
                matrix_fc = [nn.Linear(latent_dim, output_dim**2)]
            else:
                matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                    latent_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]
                matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            if nb_bias_layers <= 1:
                raise(NotImplementedError())
                bias_fc = [nn.Linear(latent_dim, nb_templates)]
            elif nb_bias_layers > 2:
                bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                    latent_dim, nb_bias_units) for i in range(nb_bias_layers - 2)]
                bias_fc.append(nn.Linear(nb_bias_units, nb_templates))
                bias_fc.append(nn.Linear(nb_templates, output_dim))
                bias_fc.append(nn.Linear(nb_templates, output_dim))
            else:
                bias_fc = [nn.Linear(latent_dim, nb_templates)]
                bias_fc.append(nn.Linear(nb_templates, output_dim))
                bias_fc.append(nn.Linear(nb_templates, output_dim))

            vtln_fc = get_VTLNLayer(latent_dim, nb_vtln_layers, nb_vtln_units)

            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)
            self.vtln_fc = nn.ModuleList(vtln_fc)

        else:
            raise(NotImplementedError())
            self.shared_fc = None

            matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                input_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]

            matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                input_dim, nb_bias_units) for i in range(nb_bias_layers - 1)]
            bias_fc.append(nn.Linear(nb_bias_units, output_dim))

            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)

        self.activfn = MyActivations(activfn)
        self.output_activfn = MyActivations(output_activfn)

        self.input_dim = input_dim
        self.output_dim = output_dim
        self.latent_dim = latent_dim
        self.only_alpha = only_alpha

        self.softmax = nn.Softmax(1)

        self.lim_scale = lim_scale

        self._prepare_A(self.output_dim)

    def _prepare_A(self, dim):
        import math
        import numpy as np
        coeff_matrix = torch.Tensor(
            [[1. / math.factorial(j) for j in range(dim)] for i in range(dim)]).unsqueeze(2)

        A_base = np.zeros(shape=[dim, dim, dim+1])
        alpha_power1_matrix = np.zeros(shape=[dim, dim, dim+1])
        alpha_power2_matrix = np.zeros(shape=[dim, dim, dim+1])

        for i in range(dim):
            for j in range(dim):
                m0 = max([0, j-i])
                aij = 0.
                for m in range(m0, j+1):
                    first_term = math.factorial(
                        j+1) // (math.factorial(j+1 - m) * math.factorial(m)) if j+1 >= m else 0
                    second_term = math.factorial(m+i) / math.factorial(m+i-j)
                    each_elem = first_term * second_term
                    A_base[i][j][m] = each_elem
                    alpha_power1_matrix[i][j][m] = m + i - j
                    alpha_power2_matrix[i][j][m] = m
        A_base = torch.from_numpy(A_base).unsqueeze(3)
        alpha_power1_matrix = torch.from_numpy(
            alpha_power1_matrix).unsqueeze(3)
        alpha_power2_matrix = torch.from_numpy(
            alpha_power2_matrix).unsqueeze(3)

        self.params_for_calcA = nn.ParameterDict({'coeff': nn.Parameter(coeff_matrix, requires_grad=False),
                                                  'A_base': nn.Parameter(A_base, requires_grad=False),
                                                  'alpha_power1': nn.Parameter(alpha_power1_matrix, requires_grad=False),
                                                  'alpha_power2': nn.Parameter(alpha_power2_matrix, requires_grad=False)})

    def get_batchA(self, batch_alpha):
        dim = self.output_dim
        alpha_power1_matrix = self.params_for_calcA['alpha_power1']
        alpha_power2_matrix = self.params_for_calcA['alpha_power2']
        A_base = self.params_for_calcA['A_base']
        coeff_matrix = self.params_for_calcA['coeff']

        batch_alpha_like_A = batch_alpha.squeeze(1).repeat(dim, dim, dim+1, 1)

        third_term = (-1.0 * batch_alpha_like_A) ** alpha_power1_matrix * \
            batch_alpha_like_A ** alpha_power2_matrix

        element_wise_product = A_base * third_term

        no_scale_A = element_wise_product.sum(dim=2)

        A = coeff_matrix * no_scale_A

        A = A.transpose(1, 2).transpose(0, 1)

        return A

    def forward(self, x):
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](x))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = x

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](h))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # VTLN-subnetwork
        if len(self.vtln_fc) == 1:
            alpha = torch.tanh(self.vtln_fc[0](z))
        else:
            h = self.activfn(self.vtln_fc[0](z))
            for i, fc in enumerate(self.vtln_fc[1:-1]):
                h = self.activfn(fc(h))
            alpha = torch.tanh(self.vtln_fc[-1](h))
        alpha = self.lim_scale * alpha

        # bias-subnetwork
        if len(self.bias_fc) <= 1 + 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2 + 1:
            h = self.softmax(self.bias_fc[0](z))
            bias_src = self.output_activfn(self.bias_fc[-2](h))
            bias_tgt = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-3]):
                h = self.activfn(fc(h))
            h = self.softmax(self.bias_fc[-3](h))
            bias_src = self.output_activfn(self.bias_fc[-2](h))
            bias_tgt = self.output_activfn(self.bias_fc[-1](h))

        A_alpha = self.get_batchA(alpha)

        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))
        x = x.unsqueeze(2)
        bias_src = bias_src.unsqueeze(2)
        bias_tgt = bias_tgt.unsqueeze(2)

        if self.only_alpha:
            predict = torch.matmul(A_alpha, (x - bias_src)) + bias_tgt
        else:
            predict = torch.matmul(A_alpha + matrix, (x - bias_src)) + bias_tgt

        predict = predict.squeeze()

        return predict
        # return bias.squeeze()
        # return matrix.reshape((matrix.shape[0], matrix.shape[1]*matrix.shape[2])).squeeze()

    def optimize(self, X, Y, optimizer, loss_fn):
        optimizer.zero_grad()
        predict_Y = self(X)
        loss = self.loss_fn(predict_Y, Y, loss_fn)
        loss.backward()
        optimizer.step()
        return loss.item()

    def loss_fn(self, predict_Y, Y, loss_fn):
        return loss_fn(predict_Y, Y)

    def cp(self, x):
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](x))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = x

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](h))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # VTLN-subnetwork
        if len(self.vtln_fc) == 1:
            alpha = torch.tanh(self.vtln_fc[0](z))
        else:
            h = self.activfn(self.vtln_fc[0](z))
            for i, fc in enumerate(self.vtln_fc[1:-1]):
                h = self.activfn(fc(h))
            alpha = torch.tanh(self.vtln_fc[-1](h))
        alpha = self.lim_scale * alpha

        # bias-subnetwork
        if len(self.bias_fc) <= 1 + 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2 + 1:
            h = self.softmax(self.bias_fc[0](z))
            bias_src = self.output_activfn(self.bias_fc[-2](h))
            bias_tgt = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-3]):
                h = self.activfn(fc(h))
            h = self.softmax(self.bias_fc[-3](h))
            bias_src = self.output_activfn(self.bias_fc[-2](h))
            bias_tgt = self.output_activfn(self.bias_fc[-1](h))

        A_alpha = self.get_batchA(alpha)
        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))
        bias_src = bias_src.unsqueeze(2)
        bias_tgt = bias_tgt.unsqueeze(2)

        if self.only_alpha:
            A = A_alpha
        else:
            A = A_alpha+matrix

        return {"A": A, "A_alpha": A_alpha, "alpha":alpha, "A_res": matrix, "b_src": bias_src, "b_tgt": bias_tgt}


class NNTVLTAlphawTMPT(KNCNNModel):

    def __init__(self,
                 nb_shared_layers,
                 nb_shared_units,
                 nb_matrix_layers,
                 nb_matrix_units,
                 nb_bias_layers,
                 nb_bias_units,
                 nb_vtln_layers,
                 nb_vtln_units,
                 activfn,
                 output_activfn,
                 input_dim,
                 output_dim,
                 latent_dim=None,
                 nb_templates=16,
                 lim_scale=0.3,
                 only_alpha=False,
                 ):
        super(NNTVLTAlphawTMPT, self).__init__()

        if nb_shared_layers > 0:
            shared_fc = [nn.Linear(nb_shared_units, nb_shared_units) if i > 0 else nn.Linear(
                input_dim, nb_shared_units) for i in range(nb_shared_layers - 1)]
            if latent_dim is None:
                latent_dim = nb_shared_units
            shared_fc.append(nn.Linear(nb_shared_units, latent_dim))
            self.shared_fc = nn.ModuleList(shared_fc)

            if nb_matrix_layers == 1:
                matrix_fc = [nn.Linear(latent_dim, output_dim**2)]
            else:
                matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                    latent_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]
                matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            if nb_bias_layers <= 1:
                raise(NotImplementedError())
                bias_fc = [nn.Linear(latent_dim, nb_templates)]
            elif nb_bias_layers > 2:
                bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                    latent_dim, nb_bias_units) for i in range(nb_bias_layers - 2)]
                bias_fc.append(nn.Linear(nb_bias_units, nb_templates))
                bias_fc.append(nn.Linear(nb_templates, output_dim))
            else:
                bias_fc = [nn.Linear(latent_dim, nb_templates)]
                bias_fc.append(nn.Linear(nb_templates, output_dim))

            vtln_fc = get_VTLNLayer(latent_dim, nb_vtln_layers, nb_vtln_units)

            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)
            self.vtln_fc = nn.ModuleList(vtln_fc)
        else:
            if nb_matrix_layers == 1:
                matrix_fc = [nn.Linear(input_dim, output_dim**2)]
            else:
                matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                    input_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]
                matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            if nb_bias_layers <= 1:
                raise(NotImplementedError())
                bias_fc = [nn.Linear(input_dim, nb_templates)]
            elif nb_bias_layers > 2:
                bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                    input_dim, nb_bias_units) for i in range(nb_bias_layers - 2)]
                bias_fc.append(nn.Linear(nb_bias_units, nb_templates))
                bias_fc.append(nn.Linear(nb_templates, output_dim))
            else:
                bias_fc = [nn.Linear(input_dim, nb_templates)]
                bias_fc.append(nn.Linear(nb_templates, output_dim))

            vtln_fc = get_VTLNLayer(input_dim, nb_vtln_layers, nb_vtln_units)

            self.shared_fc = None
            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)
            self.vtln_fc = nn.ModuleList(vtln_fc)


        self.activfn = MyActivations(activfn)
        self.output_activfn = MyActivations(output_activfn)

        self.input_dim = input_dim
        self.output_dim = output_dim
        self.latent_dim = latent_dim
        self.only_alpha = only_alpha

        self.softmax = nn.Softmax(1)

        self.lim_scale = lim_scale

        self._prepare_A(self.output_dim)

    def _prepare_A(self, dim):
        import math
        import numpy as np
        coeff_matrix = torch.Tensor(
            [[1. / math.factorial(j) for j in range(dim)] for i in range(dim)]).unsqueeze(2)

        A_base = np.zeros(shape=[dim, dim, dim+1])
        alpha_power1_matrix = np.zeros(shape=[dim, dim, dim+1])
        alpha_power2_matrix = np.zeros(shape=[dim, dim, dim+1])

        for i in range(dim):
            for j in range(dim):
                m0 = max([0, j-i])
                aij = 0.
                for m in range(m0, j+1):
                    first_term = math.factorial(
                        j+1) // (math.factorial(j+1 - m) * math.factorial(m)) if j+1 >= m else 0
                    second_term = math.factorial(m+i) / math.factorial(m+i-j)
                    each_elem = first_term * second_term
                    A_base[i][j][m] = each_elem
                    alpha_power1_matrix[i][j][m] = m + i - j
                    alpha_power2_matrix[i][j][m] = m
        A_base = torch.from_numpy(A_base).unsqueeze(3)
        alpha_power1_matrix = torch.from_numpy(
            alpha_power1_matrix).unsqueeze(3)
        alpha_power2_matrix = torch.from_numpy(
            alpha_power2_matrix).unsqueeze(3)

        self.params_for_calcA = nn.ParameterDict({'coeff': nn.Parameter(coeff_matrix, requires_grad=False),
                                                  'A_base': nn.Parameter(A_base, requires_grad=False),
                                                  'alpha_power1': nn.Parameter(alpha_power1_matrix, requires_grad=False),
                                                  'alpha_power2': nn.Parameter(alpha_power2_matrix, requires_grad=False)})

    def get_batchA(self, batch_alpha):
        dim = self.output_dim
        alpha_power1_matrix = self.params_for_calcA['alpha_power1']
        alpha_power2_matrix = self.params_for_calcA['alpha_power2']
        A_base = self.params_for_calcA['A_base']
        coeff_matrix = self.params_for_calcA['coeff']

        batch_alpha_like_A = batch_alpha.squeeze(1).repeat(dim, dim, dim+1, 1)

        third_term = (-1.0 * batch_alpha_like_A) ** alpha_power1_matrix * \
            batch_alpha_like_A ** alpha_power2_matrix

        element_wise_product = A_base * third_term

        no_scale_A = element_wise_product.sum(dim=2)

        A = coeff_matrix * no_scale_A

        A = A.transpose(1, 2).transpose(0, 1)

        return A

    def forward(self, x):
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](x))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = x

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](h))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # VTLN-subnetwork
        if len(self.vtln_fc) == 1:
            alpha = torch.tanh(self.vtln_fc[0](z))
        else:
            h = self.activfn(self.vtln_fc[0](z))
            for i, fc in enumerate(self.vtln_fc[1:-1]):
                h = self.activfn(fc(h))
            alpha = torch.tanh(self.vtln_fc[-1](h))
        alpha = self.lim_scale * alpha

        # bias-subnetwork
        if len(self.bias_fc) == 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2:
            h = self.softmax(self.bias_fc[0](z))
            bias = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-2]):
                h = self.activfn(fc(h))
            h = self.softmax(self.bias_fc[-2](h))
            bias = self.output_activfn(self.bias_fc[-1](h))

        A_alpha = self.get_batchA(alpha)

        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))
        x = x.unsqueeze(2)
        bias = bias.unsqueeze(2)

        # predict = torch.matmul(matrix, x) + bias
        if self.only_alpha:
            predict = torch.matmul(A_alpha, x) + bias
        else:
            predict = torch.matmul(A_alpha + matrix, x) + bias

        predict = predict.squeeze()

        return predict
        # return bias.squeeze()
        # return matrix.reshape((matrix.shape[0], matrix.shape[1]*matrix.shape[2])).squeeze()

    def optimize(self, X, Y, optimizer, loss_fn):
        optimizer.zero_grad()
        predict_Y = self(X)
        loss = self.loss_fn(predict_Y, Y, loss_fn)
        loss.backward()
        optimizer.step()
        return loss.item()

    def loss_fn(self, predict_Y, Y, loss_fn):
        return loss_fn(predict_Y, Y)

    def cp(self, x):
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](x))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = x

        # VTLN-subnetwork
        if len(self.vtln_fc) == 1:
            alpha = torch.tanh(self.vtln_fc[0](z))
        else:
            h = self.activfn(self.vtln_fc[0](z))
            for i, fc in enumerate(self.vtln_fc[1:-1]):
                h = self.activfn(fc(h))
            alpha = torch.tanh(self.vtln_fc[-1](h))
        alpha = self.lim_scale * alpha

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](h))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # bias-subnetwork
        if len(self.bias_fc) == 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2:
            h = self.softmax(self.bias_fc[0](z))
            bias = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-2]):
                h = self.activfn(fc(h))
            h = self.softmax(self.bias_fc[-2](h))
            bias = self.output_activfn(self.bias_fc[-1](h))

        A_alpha = self.get_batchA(alpha)

        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))
        bias = bias.unsqueeze(2)

        if self.only_alpha:
            A = A_alpha
        else:
            A = A_alpha+matrix

        return {"A": A, "A_alpha": A_alpha, "alpha": alpha, "A_res": matrix, "b": bias}


class NNTVLTAlphawTMPTMLPG1(KNCNNModel):

    def __init__(self,
                 nb_shared_layers,
                 nb_shared_units,
                 nb_matrix_layers,
                 nb_matrix_units,
                 nb_bias_layers,
                 nb_bias_units,
                 nb_vtln_layers,
                 nb_vtln_units,
                 activfn,
                 output_activfn,
                 input_dim,
                 output_dim,
                 train_dataset_path,
                 latent_dim=None,
                 nb_templates=16,
                 lim_scale=0.3,
                 only_alpha=False,
                 delta_num=1
                 ):
        super(NNTVLTAlphawTMPTMLPG1, self).__init__()

        if nb_shared_layers > 0:
            shared_fc = [nn.Linear(nb_shared_units, nb_shared_units) if i > 0 else nn.Linear(
                input_dim, nb_shared_units) for i in range(nb_shared_layers - 1)]
            if latent_dim is None:
                latent_dim = nb_shared_units
            shared_fc.append(nn.Linear(nb_shared_units, latent_dim))
            self.shared_fc = nn.ModuleList(shared_fc)

            if nb_matrix_layers == 1:
                matrix_fc = [nn.Linear(latent_dim, output_dim**2)]
            else:
                matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                    latent_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]
                matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            if nb_bias_layers <= 1:
                raise(NotImplementedError())
                bias_fc = [nn.Linear(latent_dim, nb_templates)]
            elif nb_bias_layers > 2:
                bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                    latent_dim, nb_bias_units) for i in range(nb_bias_layers - 2)]
                bias_fc.append(nn.Linear(nb_bias_units, nb_templates))
                bias_fc.append(nn.Linear(nb_templates, output_dim))
            else:
                bias_fc = [nn.Linear(latent_dim, nb_templates)]
                bias_fc.append(nn.Linear(nb_templates, output_dim))

            vtln_fc = get_VTLNLayer(latent_dim, nb_vtln_layers, nb_vtln_units)

            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)
            self.vtln_fc = nn.ModuleList(vtln_fc)
        else:
            raise(NotImplementedError())
            self.shared_fc = None

            matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                input_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]

            matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                input_dim, nb_bias_units) for i in range(nb_bias_layers - 1)]
            bias_fc.append(nn.Linear(nb_bias_units, output_dim))

            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)

        self.activfn = MyActivations(activfn)
        self.output_activfn = MyActivations(output_activfn)

        self.input_dim = input_dim
        self.output_dim = output_dim
        self.latent_dim = latent_dim
        self.only_alpha = only_alpha

        self.softmax = nn.Softmax(1)

        self.lim_scale = lim_scale

        self._prepare_A(self.output_dim)

        self.train_dataset_path = train_dataset_path
        self.var_Y = cmpt_var_Y(train_dataset_path)

        self.delta_num = delta_num

    def _prepare_A(self, dim):
        import math
        import numpy as np
        coeff_matrix = torch.Tensor(
            [[1. / math.factorial(j) for j in range(dim)] for i in range(dim)]).unsqueeze(2)

        A_base = np.zeros(shape=[dim, dim, dim+1])
        alpha_power1_matrix = np.zeros(shape=[dim, dim, dim+1])
        alpha_power2_matrix = np.zeros(shape=[dim, dim, dim+1])

        for i in range(dim):
            for j in range(dim):
                m0 = max([0, j-i])
                aij = 0.
                for m in range(m0, j+1):
                    first_term = math.factorial(
                        j+1) // (math.factorial(j+1 - m) * math.factorial(m)) if j+1 >= m else 0
                    second_term = math.factorial(m+i) / math.factorial(m+i-j)
                    each_elem = first_term * second_term
                    A_base[i][j][m] = each_elem
                    alpha_power1_matrix[i][j][m] = m + i - j
                    alpha_power2_matrix[i][j][m] = m
        A_base = torch.from_numpy(A_base).unsqueeze(3)
        alpha_power1_matrix = torch.from_numpy(
            alpha_power1_matrix).unsqueeze(3)
        alpha_power2_matrix = torch.from_numpy(
            alpha_power2_matrix).unsqueeze(3)

        self.params_for_calcA = nn.ParameterDict({'coeff': nn.Parameter(coeff_matrix, requires_grad=False),
                                                  'A_base': nn.Parameter(A_base, requires_grad=False),
                                                  'alpha_power1': nn.Parameter(alpha_power1_matrix, requires_grad=False),
                                                  'alpha_power2': nn.Parameter(alpha_power2_matrix, requires_grad=False)})

    def get_batchA(self, batch_alpha):
        dim = self.output_dim
        alpha_power1_matrix = self.params_for_calcA['alpha_power1']
        alpha_power2_matrix = self.params_for_calcA['alpha_power2']
        A_base = self.params_for_calcA['A_base']
        coeff_matrix = self.params_for_calcA['coeff']

        batch_alpha_like_A = batch_alpha.squeeze(1).repeat(dim, dim, dim+1, 1)

        third_term = (-1.0 * batch_alpha_like_A) ** alpha_power1_matrix * \
            batch_alpha_like_A ** alpha_power2_matrix

        element_wise_product = A_base * third_term

        no_scale_A = element_wise_product.sum(dim=2)

        A = coeff_matrix * no_scale_A

        A = A.transpose(1, 2).transpose(0, 1)

        return A

    def forward(self, x):
        W = torch.from_numpy(get_window_matrix(
            delta_num=self.delta_num, sequence_length=x.size(0)))
        X = static2staticdelta(x, delta_num=self.delta_num, W=W)
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](x))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = x

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](h))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # VTLN-subnetwork
        if len(self.vtln_fc) == 1:
            alpha = torch.tanh(self.vtln_fc[0](z))
        else:
            h = self.activfn(self.vtln_fc[0](z))
            for i, fc in enumerate(self.vtln_fc[1:-1]):
                h = self.activfn(fc(h))
            alpha = torch.tanh(self.vtln_fc[-1](h))
        alpha = self.lim_scale * alpha

        # bias-subnetwork
        if len(self.bias_fc) == 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2:
            h = self.softmax(self.bias_fc[0](z))
            bias = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-2]):
                h = self.activfn(fc(h))
            h = self.softmax(self.bias_fc[-2](h))
            bias = self.output_activfn(self.bias_fc[-1](h))

        A_alpha = self.get_batchA(alpha)

        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))
        x = x.unsqueeze(2)
        bias = bias.unsqueeze(2)

        # predict = torch.matmul(matrix, x) + bias
        if self.only_alpha:
            predict = torch.matmul(A_alpha, x) + bias
        else:
            predict = torch.matmul(A_alpha + matrix, x) + bias

        predict = predict.squeeze()

        Y = torch.cat([predict, X[:, self.output_dim:]], dim=1)
        y = staticdelta2static(Y, self.var_Y, delta_num=self.delta_num, W=W)

        return y

    def optimize(self, X, Y, optimizer, loss_fn):
        optimizer.zero_grad()
        predict_Y = self(X)
        loss = self.loss_fn(predict_Y, Y, loss_fn)
        loss.backward()
        optimizer.step()
        return loss.item()

    def loss_fn(self, predict_Y, Y, loss_fn):
        return loss_fn(predict_Y, Y)

    def cp(self, x):
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](x))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = x

        # VTLN-subnetwork
        if len(self.vtln_fc) == 1:
            alpha = torch.tanh(self.vtln_fc[0](z))
        else:
            h = self.activfn(self.vtln_fc[0](z))
            for i, fc in enumerate(self.vtln_fc[1:-1]):
                h = self.activfn(fc(h))
            alpha = torch.tanh(self.vtln_fc[-1](h))
        alpha = self.lim_scale * alpha

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](h))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # bias-subnetwork
        if len(self.bias_fc) == 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2:
            h = self.softmax(self.bias_fc[0](z))
            bias = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-2]):
                h = self.activfn(fc(h))
            h = self.softmax(self.bias_fc[-2](h))
            bias = self.output_activfn(self.bias_fc[-1](h))

        A_alpha = self.get_batchA(alpha)

        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))
        bias = bias.unsqueeze(2)

        if self.only_alpha:
            A = A_alpha
        else:
            A = A_alpha+matrix

        return {"A": A, "A_alpha": A_alpha, "alpha": alpha, "A_res": matrix, "b": bias}


class NNTVLTAlphawTMPTMLPG2(KNCNNModel):

    def __init__(self,
                 nb_shared_layers,
                 nb_shared_units,
                 nb_matrix_layers,
                 nb_matrix_units,
                 nb_bias_layers,
                 nb_bias_units,
                 nb_vtln_layers,
                 nb_vtln_units,
                 activfn,
                 output_activfn,
                 input_dim,
                 output_dim,
                 train_dataset_path,
                 latent_dim=None,
                 nb_templates=16,
                 lim_scale=0.3,
                 only_alpha=False,
                 delta_num=1
                 ):
        super(NNTVLTAlphawTMPTMLPG2, self).__init__()

        if nb_shared_layers > 0:
            shared_fc = [nn.Linear(nb_shared_units, nb_shared_units) if i > 0 else nn.Linear(
                input_dim*2, nb_shared_units) for i in range(nb_shared_layers - 1)]
            if latent_dim is None:
                latent_dim = nb_shared_units
            shared_fc.append(nn.Linear(nb_shared_units, latent_dim))
            self.shared_fc = nn.ModuleList(shared_fc)

            if nb_matrix_layers == 1:
                matrix_fc = [nn.Linear(latent_dim, output_dim**2)]
            else:
                matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                    latent_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]
                matrix_fc.append(nn.Linear(nb_matrix_units, (output_dim**2)*2))

            if nb_bias_layers <= 1:
                raise(NotImplementedError())
                bias_fc = [nn.Linear(latent_dim, nb_templates)]
            elif nb_bias_layers > 2:
                bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                    latent_dim, nb_bias_units) for i in range(nb_bias_layers - 2)]
                bias_fc.append(nn.Linear(nb_bias_units, nb_templates))
                bias_fc.append(nn.Linear(nb_templates, output_dim*2))
            else:
                bias_fc = [nn.Linear(latent_dim, nb_templates)]
                bias_fc.append(nn.Linear(nb_templates, output_dim*2))

            vtln_fc = get_VTLNLayer(latent_dim, nb_vtln_layers, nb_vtln_units)

            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)
            self.vtln_fc = nn.ModuleList(vtln_fc)
        else:
            raise(NotImplementedError())
            self.shared_fc = None

            matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                input_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]

            matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                input_dim, nb_bias_units) for i in range(nb_bias_layers - 1)]
            bias_fc.append(nn.Linear(nb_bias_units, output_dim))

            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)

        self.activfn = MyActivations(activfn)
        self.output_activfn = MyActivations(output_activfn)

        self.input_dim = input_dim
        self.output_dim = output_dim
        self.latent_dim = latent_dim
        self.only_alpha = only_alpha

        self.softmax = nn.Softmax(1)

        self.lim_scale = lim_scale

        self._prepare_A(self.output_dim)

        self.train_dataset_path = train_dataset_path
        self.var_Y = cmpt_var_Y(train_dataset_path)

        self.delta_num = delta_num

    def _prepare_A(self, dim):
        import math
        import numpy as np
        coeff_matrix = torch.Tensor(
            [[1. / math.factorial(j) for j in range(dim)] for i in range(dim)]).unsqueeze(2)

        A_base = np.zeros(shape=[dim, dim, dim+1])
        alpha_power1_matrix = np.zeros(shape=[dim, dim, dim+1])
        alpha_power2_matrix = np.zeros(shape=[dim, dim, dim+1])

        for i in range(dim):
            for j in range(dim):
                m0 = max([0, j-i])
                aij = 0.
                for m in range(m0, j+1):
                    first_term = math.factorial(
                        j+1) // (math.factorial(j+1 - m) * math.factorial(m)) if j+1 >= m else 0
                    second_term = math.factorial(m+i) / math.factorial(m+i-j)
                    each_elem = first_term * second_term
                    A_base[i][j][m] = each_elem
                    alpha_power1_matrix[i][j][m] = m + i - j
                    alpha_power2_matrix[i][j][m] = m
        A_base = torch.from_numpy(A_base).unsqueeze(3)
        alpha_power1_matrix = torch.from_numpy(
            alpha_power1_matrix).unsqueeze(3)
        alpha_power2_matrix = torch.from_numpy(
            alpha_power2_matrix).unsqueeze(3)

        self.params_for_calcA = nn.ParameterDict({'coeff': nn.Parameter(coeff_matrix, requires_grad=False),
                                                  'A_base': nn.Parameter(A_base, requires_grad=False),
                                                  'alpha_power1': nn.Parameter(alpha_power1_matrix, requires_grad=False),
                                                  'alpha_power2': nn.Parameter(alpha_power2_matrix, requires_grad=False)})

    def get_batchA(self, batch_alpha):
        dim = self.output_dim
        alpha_power1_matrix = self.params_for_calcA['alpha_power1']
        alpha_power2_matrix = self.params_for_calcA['alpha_power2']
        A_base = self.params_for_calcA['A_base']
        coeff_matrix = self.params_for_calcA['coeff']

        batch_alpha_like_A = batch_alpha.squeeze(1).repeat(dim, dim, dim+1, 1)

        third_term = (-1.0 * batch_alpha_like_A) ** alpha_power1_matrix * \
            batch_alpha_like_A ** alpha_power2_matrix

        element_wise_product = A_base * third_term

        no_scale_A = element_wise_product.sum(dim=2)

        A = coeff_matrix * no_scale_A

        A = A.transpose(1, 2).transpose(0, 1)

        return A

    def forward(self, x):
        W = torch.from_numpy(get_window_matrix(
            delta_num=self.delta_num, sequence_length=x.size(0)))
        X = static2staticdelta(x, delta_num=self.delta_num, W=W)
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](X))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = X

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](h))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # VTLN-subnetwork
        if len(self.vtln_fc) == 1:
            alpha = torch.tanh(self.vtln_fc[0](z))
        else:
            h = self.activfn(self.vtln_fc[0](z))
            for i, fc in enumerate(self.vtln_fc[1:-1]):
                h = self.activfn(fc(h))
            alpha = torch.tanh(self.vtln_fc[-1](h))
        alpha = self.lim_scale * alpha

        # bias-subnetwork
        if len(self.bias_fc) == 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2:
            h = self.softmax(self.bias_fc[0](z))
            bias = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-2]):
                h = self.activfn(fc(h))
            h = self.softmax(self.bias_fc[-2](h))
            bias = self.output_activfn(self.bias_fc[-1](h))

        A_alpha = self.get_batchA(alpha)

        matrix = torch.reshape(
            matrix_flatten[:, :self.output_dim**2], (matrix_flatten.shape[0], self.output_dim, self.output_dim))
        matrix_delta = torch.reshape(
            matrix_flatten[:, self.output_dim**2:], (matrix_flatten.shape[0], self.output_dim, self.output_dim))
        x = x.unsqueeze(2)
        x_delta = X[:, self.input_dim:].unsqueeze(2)
        bias = bias.unsqueeze(2)

        # predict = torch.matmul(matrix, x) + bias
        if self.only_alpha:
            raise(NotImplementedError())
            predict = torch.matmul(A_alpha, x) + bias
        else:
            statics = torch.matmul(A_alpha + matrix, x)
            dynamics = torch.matmul(matrix_delta, x_delta)
            predict = torch.cat([statics, dynamics], dim=1) + bias

        predict = predict.squeeze()

        y = staticdelta2static(predict, self.var_Y, delta_num=self.delta_num, W=W)

        return y

    def optimize(self, X, Y, optimizer, loss_fn):
        optimizer.zero_grad()
        predict_Y = self(X)
        loss = self.loss_fn(predict_Y, Y, loss_fn)
        loss.backward()
        optimizer.step()
        return loss.item()

    def loss_fn(self, predict_Y, Y, loss_fn):
        return loss_fn(predict_Y, Y)

    def cp(self, x):
        W = torch.from_numpy(get_window_matrix(
            delta_num=self.delta_num, sequence_length=x.size(0)))
        X = static2staticdelta(x, delta_num=self.delta_num, W=W)
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](X))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = X

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](h))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # VTLN-subnetwork
        if len(self.vtln_fc) == 1:
            alpha = torch.tanh(self.vtln_fc[0](z))
        else:
            h = self.activfn(self.vtln_fc[0](z))
            for i, fc in enumerate(self.vtln_fc[1:-1]):
                h = self.activfn(fc(h))
            alpha = torch.tanh(self.vtln_fc[-1](h))
        alpha = self.lim_scale * alpha

        # bias-subnetwork
        if len(self.bias_fc) == 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2:
            h = self.softmax(self.bias_fc[0](z))
            bias = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-2]):
                h = self.activfn(fc(h))
            h = self.softmax(self.bias_fc[-2](h))
            bias = self.output_activfn(self.bias_fc[-1](h))

        A_alpha = self.get_batchA(alpha)

        matrix = torch.reshape(
            matrix_flatten[:, :self.output_dim**2], (matrix_flatten.shape[0], self.output_dim, self.output_dim))
        matrix_delta = torch.reshape(
            matrix_flatten[:, self.output_dim**2:], (matrix_flatten.shape[0], self.output_dim, self.output_dim))
        x = x.unsqueeze(2)
        x_delta = X[:, self.input_dim:].unsqueeze(2)
        bias = bias.unsqueeze(2)

        if self.only_alpha:
            A = A_alpha
        else:
            A = A_alpha+matrix
            statics = torch.matmul(A_alpha + matrix, x)
            dynamics = torch.matmul(matrix_delta, x_delta)
            predict = torch.cat([statics, dynamics], dim=1) + bias

        predict = predict.squeeze()

        return {"A": A, "A_alpha": A_alpha, "alpha": alpha, "A_res": matrix, "b": bias, "A_delta": matrix_delta, "Y": predict}


class NNTVLTAlphawTMPTMLPG3(KNCNNModel):

    def __init__(self,
                 nb_shared_layers,
                 nb_shared_units,
                 nb_matrix_layers,
                 nb_matrix_units,
                 nb_bias_layers,
                 nb_bias_units,
                 nb_vtln_layers,
                 nb_vtln_units,
                 nb_delta_layers,
                 nb_delta_units,
                 activfn,
                 output_activfn,
                 input_dim,
                 output_dim,
                 train_dataset_path,
                 latent_dim=None,
                 nb_templates=16,
                 lim_scale=0.3,
                 only_alpha=False,
                 delta_num=1
                 ):
        super(NNTVLTAlphawTMPTMLPG3, self).__init__()

        if nb_shared_layers > 0:
            shared_fc = [nn.Linear(nb_shared_units, nb_shared_units) if i > 0 else nn.Linear(
                input_dim*2, nb_shared_units) for i in range(nb_shared_layers - 1)]
            if latent_dim is None:
                latent_dim = nb_shared_units
            shared_fc.append(nn.Linear(nb_shared_units, latent_dim))
            self.shared_fc = nn.ModuleList(shared_fc)

            if nb_matrix_layers == 1:
                matrix_fc = [nn.Linear(latent_dim, output_dim**2)]
            else:
                matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                    latent_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]
                matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            if nb_bias_layers <= 1:
                raise(NotImplementedError())
                bias_fc = [nn.Linear(latent_dim, nb_templates)]
            elif nb_bias_layers > 2:
                bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                    latent_dim, nb_bias_units) for i in range(nb_bias_layers - 2)]
                bias_fc.append(nn.Linear(nb_bias_units, nb_templates))
                bias_fc.append(nn.Linear(nb_templates, output_dim))
            else:
                bias_fc = [nn.Linear(latent_dim, nb_templates)]
                bias_fc.append(nn.Linear(nb_templates, output_dim))

            vtln_fc = get_VTLNLayer(latent_dim, nb_vtln_layers, nb_vtln_units)

            if nb_delta_layers == 1:
                raise(NotImplementedError())
                delta_fc = [nn.Linear(latent_dim, output_dim)]
            else:
                delta_fc = [nn.Linear(nb_delta_units, nb_delta_units) if i > 0 else nn.Linear(
                    latent_dim, nb_delta_units) for i in range(nb_delta_layers - 1)]
                delta_fc.append(nn.Linear(nb_delta_units, output_dim))

            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)
            self.vtln_fc = nn.ModuleList(vtln_fc)
            self.delta_fc = nn.ModuleList(delta_fc)

        else:
            if nb_matrix_layers == 1:
                matrix_fc = [nn.Linear(input_dim*2, output_dim**2)]
            else:
                matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                    input_dim*2, nb_matrix_units) for i in range(nb_matrix_layers - 1)]
                matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            if nb_bias_layers <= 1:
                raise(NotImplementedError())
                bias_fc = [nn.Linear(input_dim*2, nb_templates)]
            elif nb_bias_layers > 2:
                bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                    input_dim*2, nb_bias_units) for i in range(nb_bias_layers - 2)]
                bias_fc.append(nn.Linear(nb_bias_units, nb_templates))
                bias_fc.append(nn.Linear(nb_templates, output_dim))
            else:
                bias_fc = [nn.Linear(input_dim*2, nb_templates)]
                bias_fc.append(nn.Linear(nb_templates, output_dim))

            vtln_fc = get_VTLNLayer(input_dim*2, nb_vtln_layers, nb_vtln_units)

            if nb_delta_layers == 1:
                raise(NotImplementedError())
                delta_fc = [nn.Linear(input_dim*2, output_dim)]
            else:
                delta_fc = [nn.Linear(nb_delta_units, nb_delta_units) if i > 0 else nn.Linear(
                    input_dim*2, nb_delta_units) for i in range(nb_delta_layers - 1)]
                delta_fc.append(nn.Linear(nb_delta_units, output_dim))

            self.shared_fc = None
            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)
            self.vtln_fc = nn.ModuleList(vtln_fc)
            self.delta_fc = nn.ModuleList(delta_fc)

        self.activfn = MyActivations(activfn)
        self.output_activfn = MyActivations(output_activfn)

        self.input_dim = input_dim
        self.output_dim = output_dim
        self.latent_dim = latent_dim
        self.only_alpha = only_alpha

        self.softmax = nn.Softmax(1)

        self.lim_scale = lim_scale

        self._prepare_A(self.output_dim)

        self.train_dataset_path = train_dataset_path
        self.var_Y = cmpt_var_Y(train_dataset_path)

        self.delta_num = delta_num

    def _prepare_A(self, dim):
        import math
        import numpy as np
        coeff_matrix = torch.Tensor(
            [[1. / math.factorial(j) for j in range(dim)] for i in range(dim)]).unsqueeze(2)

        A_base = np.zeros(shape=[dim, dim, dim+1])
        alpha_power1_matrix = np.zeros(shape=[dim, dim, dim+1])
        alpha_power2_matrix = np.zeros(shape=[dim, dim, dim+1])

        for i in range(dim):
            for j in range(dim):
                m0 = max([0, j-i])
                aij = 0.
                for m in range(m0, j+1):
                    first_term = math.factorial(
                        j+1) // (math.factorial(j+1 - m) * math.factorial(m)) if j+1 >= m else 0
                    second_term = math.factorial(m+i) / math.factorial(m+i-j)
                    each_elem = first_term * second_term
                    A_base[i][j][m] = each_elem
                    alpha_power1_matrix[i][j][m] = m + i - j
                    alpha_power2_matrix[i][j][m] = m
        A_base = torch.from_numpy(A_base).unsqueeze(3)
        alpha_power1_matrix = torch.from_numpy(
            alpha_power1_matrix).unsqueeze(3)
        alpha_power2_matrix = torch.from_numpy(
            alpha_power2_matrix).unsqueeze(3)

        self.params_for_calcA = nn.ParameterDict({'coeff': nn.Parameter(coeff_matrix, requires_grad=False),
                                                  'A_base': nn.Parameter(A_base, requires_grad=False),
                                                  'alpha_power1': nn.Parameter(alpha_power1_matrix, requires_grad=False),
                                                  'alpha_power2': nn.Parameter(alpha_power2_matrix, requires_grad=False)})

    def get_batchA(self, batch_alpha):
        dim = self.output_dim
        alpha_power1_matrix = self.params_for_calcA['alpha_power1']
        alpha_power2_matrix = self.params_for_calcA['alpha_power2']
        A_base = self.params_for_calcA['A_base']
        coeff_matrix = self.params_for_calcA['coeff']

        batch_alpha_like_A = batch_alpha.squeeze(1).repeat(dim, dim, dim+1, 1)

        third_term = (-1.0 * batch_alpha_like_A) ** alpha_power1_matrix * \
            batch_alpha_like_A ** alpha_power2_matrix

        element_wise_product = A_base * third_term

        no_scale_A = element_wise_product.sum(dim=2)

        A = coeff_matrix * no_scale_A

        A = A.transpose(1, 2).transpose(0, 1)

        return A

    def forward(self, x):
        W = torch.from_numpy(get_window_matrix(
            delta_num=self.delta_num, sequence_length=x.size(0)))
        X = static2staticdelta(x, delta_num=self.delta_num, W=W)
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](X))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = X

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](z))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # VTLN-subnetwork
        if len(self.vtln_fc) == 1:
            alpha = torch.tanh(self.vtln_fc[0](z))
        else:
            h = self.activfn(self.vtln_fc[0](z))
            for i, fc in enumerate(self.vtln_fc[1:-1]):
                h = self.activfn(fc(h))
            alpha = torch.tanh(self.vtln_fc[-1](h))
        alpha = self.lim_scale * alpha

        # bias-subnetwork
        if len(self.bias_fc) == 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2:
            h = self.softmax(self.bias_fc[0](z))
            bias = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-2]):
                h = self.activfn(fc(h))
            h = self.softmax(self.bias_fc[-2](h))
            bias = self.output_activfn(self.bias_fc[-1](h))


        # delta sub-network
        if len(self.delta_fc) == 1:
            delta = self.output_activfn(self.delta_fc[0](z))
        else:
            h = self.activfn(self.delta_fc[0](z))
            for i, fc in enumerate(self.delta_fc[1:-1]):
                h = self.activfn(fc(h))
            delta = self.output_activfn(self.delta_fc[-1](h))

        A_alpha = self.get_batchA(alpha)

        matrix = torch.reshape(matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))

        x = x.unsqueeze(2)
        delta = delta.unsqueeze(2)
        bias = bias.unsqueeze(2)

        # predict = torch.matmul(matrix, x) + bias
        if self.only_alpha:
            raise(NotImplementedError())
            predict = torch.matmul(A_alpha, x) + bias
        else:
            statics = torch.matmul(A_alpha + matrix, x) + bias
            dynamics = delta
            predict = torch.cat([statics, dynamics], dim=1)

        predict = predict.squeeze()

        y = staticdelta2static(predict, self.var_Y,
                               delta_num=self.delta_num, W=W)

        return y

    def optimize(self, X, Y, optimizer, loss_fn):
        optimizer.zero_grad()
        predict_Y = self(X)
        loss = self.loss_fn(predict_Y, Y, loss_fn)
        loss.backward()
        optimizer.step()
        return loss.item()

    def loss_fn(self, predict_Y, Y, loss_fn):
        return loss_fn(predict_Y, Y)

    def cp(self, x):
        W = torch.from_numpy(get_window_matrix(
            delta_num=self.delta_num, sequence_length=x.size(0)))
        X = static2staticdelta(x, delta_num=self.delta_num, W=W)
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](X))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = X

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](z))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # VTLN-subnetwork
        if len(self.vtln_fc) == 1:
            alpha = torch.tanh(self.vtln_fc[0](z))
        else:
            h = self.activfn(self.vtln_fc[0](z))
            for i, fc in enumerate(self.vtln_fc[1:-1]):
                h = self.activfn(fc(h))
            alpha = torch.tanh(self.vtln_fc[-1](h))
        alpha = self.lim_scale * alpha

        # bias-subnetwork
        if len(self.bias_fc) == 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2:
            h = self.softmax(self.bias_fc[0](z))
            bias = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-2]):
                h = self.activfn(fc(h))
            h = self.softmax(self.bias_fc[-2](h))
            bias = self.output_activfn(self.bias_fc[-1](h))

        # delta sub-network
        if len(self.delta_fc) == 1:
            delta = self.output_activfn(self.delta_fc[0](z))
        else:
            h = self.activfn(self.delta_fc[0](z))
            for i, fc in enumerate(self.delta_fc[1:-1]):
                h = self.activfn(fc(h))
            delta = self.output_activfn(self.delta_fc[-1](h))

        A_alpha = self.get_batchA(alpha)

        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))

        x = x.unsqueeze(2)
        delta = delta.unsqueeze(2)
        bias = bias.unsqueeze(2)

        # predict = torch.matmul(matrix, x) + bias
        if self.only_alpha:
            A = A_alpha
            predict = torch.matmul(A_alpha, x) + bias
        else:
            A = A_alpha+matrix
            statics = torch.matmul(A_alpha + matrix, x) + bias
            dynamics = delta
            predict = torch.cat([statics, dynamics], dim=1)

        predict = predict.squeeze()

        return {"A": A, "A_alpha": A_alpha, "alpha": alpha, "A_res": matrix, "b": bias, "delta": dynamics, "Y": predict}


class NNTVLTTMPTMLPG1(KNCNNModel):

    def __init__(self,
                 nb_shared_layers,
                 nb_shared_units,
                 nb_matrix_layers,
                 nb_matrix_units,
                 nb_bias_layers,
                 nb_bias_units,
                 nb_delta_layers,
                 nb_delta_units,
                 activfn,
                 output_activfn,
                 input_dim,
                 output_dim,
                 train_dataset_path,
                 latent_dim=None,
                 nb_templates=16,
                 delta_num=1
                 ):
        super(NNTVLTTMPTMLPG1, self).__init__()

        if nb_shared_layers > 0:
            shared_fc = [nn.Linear(nb_shared_units, nb_shared_units) if i > 0 else nn.Linear(
                input_dim*2, nb_shared_units) for i in range(nb_shared_layers - 1)]
            if latent_dim is None:
                latent_dim = nb_shared_units
            shared_fc.append(nn.Linear(nb_shared_units, latent_dim))
            self.shared_fc = nn.ModuleList(shared_fc)

            if nb_matrix_layers == 1:
                matrix_fc = [nn.Linear(latent_dim, output_dim**2)]
            else:
                matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                    latent_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]
                matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            if nb_bias_layers <= 1:
                raise(NotImplementedError())
                bias_fc = [nn.Linear(latent_dim, nb_templates)]
            elif nb_bias_layers > 2:
                bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                    latent_dim, nb_bias_units) for i in range(nb_bias_layers - 2)]
                bias_fc.append(nn.Linear(nb_bias_units, nb_templates))
                bias_fc.append(nn.Linear(nb_templates, output_dim))
            else:
                bias_fc = [nn.Linear(latent_dim, nb_templates)]
                bias_fc.append(nn.Linear(nb_templates, output_dim))

            if nb_delta_layers == 1:
                raise(NotImplementedError())
                delta_fc = [nn.Linear(latent_dim, output_dim)]
            else:
                delta_fc = [nn.Linear(nb_delta_units, nb_delta_units) if i > 0 else nn.Linear(
                    latent_dim, nb_delta_units) for i in range(nb_delta_layers - 1)]
                delta_fc.append(nn.Linear(nb_delta_units, output_dim))

            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)
            self.delta_fc = nn.ModuleList(delta_fc)

        else:
            raise(NotImplementedError())
            self.shared_fc = None

            matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                input_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]

            matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                input_dim, nb_bias_units) for i in range(nb_bias_layers - 1)]
            bias_fc.append(nn.Linear(nb_bias_units, output_dim))

            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)

        self.activfn = MyActivations(activfn)
        self.output_activfn = MyActivations(output_activfn)

        self.input_dim = input_dim
        self.output_dim = output_dim
        self.latent_dim = latent_dim

        self.softmax = nn.Softmax(1)

        self.train_dataset_path = train_dataset_path
        self.var_Y = cmpt_var_Y(train_dataset_path)

        self.delta_num = delta_num

    def forward(self, x):
        W = torch.from_numpy(get_window_matrix(
            delta_num=self.delta_num, sequence_length=x.size(0)))
        X = static2staticdelta(x, delta_num=self.delta_num, W=W)
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](X))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = X

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](z))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # bias-subnetwork
        if len(self.bias_fc) == 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2:
            h = self.softmax(self.bias_fc[0](z))
            bias = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-2]):
                h = self.activfn(fc(h))
            h = self.softmax(self.bias_fc[-2](h))
            bias = self.output_activfn(self.bias_fc[-1](h))

        # delta sub-network
        if len(self.delta_fc) == 1:
            delta = self.output_activfn(self.delta_fc[0](z))
        else:
            h = self.activfn(self.delta_fc[0](z))
            for i, fc in enumerate(self.delta_fc[1:-1]):
                h = self.activfn(fc(h))
            delta = self.output_activfn(self.delta_fc[-1](h))

        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))

        x = x.unsqueeze(2)
        delta = delta.unsqueeze(2)
        bias = bias.unsqueeze(2)

        # predict = torch.matmul(matrix, x) + bias
        statics = torch.matmul(matrix, x) + bias
        dynamics = delta
        predict = torch.cat([statics, dynamics], dim=1)

        predict = predict.squeeze()

        y = staticdelta2static(predict, self.var_Y,
                               delta_num=self.delta_num, W=W)

        return y

    def optimize(self, X, Y, optimizer, loss_fn):
        optimizer.zero_grad()
        predict_Y = self(X)
        loss = self.loss_fn(predict_Y, Y, loss_fn)
        loss.backward()
        optimizer.step()
        return loss.item()

    def loss_fn(self, predict_Y, Y, loss_fn):
        return loss_fn(predict_Y, Y)

    def cp(self, x):
        W = torch.from_numpy(get_window_matrix(
            delta_num=self.delta_num, sequence_length=x.size(0)))
        X = static2staticdelta(x, delta_num=self.delta_num, W=W)
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](X))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = X

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](z))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # bias-subnetwork
        if len(self.bias_fc) == 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2:
            h = self.softmax(self.bias_fc[0](z))
            bias = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-2]):
                h = self.activfn(fc(h))
            h = self.softmax(self.bias_fc[-2](h))
            bias = self.output_activfn(self.bias_fc[-1](h))

        # delta sub-network
        if len(self.delta_fc) == 1:
            delta = self.output_activfn(self.delta_fc[0](z))
        else:
            h = self.activfn(self.delta_fc[0](z))
            for i, fc in enumerate(self.delta_fc[1:-1]):
                h = self.activfn(fc(h))
            delta = self.output_activfn(self.delta_fc[-1](h))

        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))

        x = x.unsqueeze(2)
        delta = delta.unsqueeze(2)
        bias = bias.unsqueeze(2)

        # predict = torch.matmul(matrix, x) + bias
        A = matrix
        statics = torch.matmul(matrix, x) + bias
        dynamics = delta
        predict = torch.cat([statics, dynamics], dim=1)

        predict = predict.squeeze()

        return {"A": A, "b": bias, "delta": dynamics, "Y": predict}


class NNTVLTAlphawDBiasMLPG1(KNCNNModel):

    def __init__(self,
                 nb_shared_layers,
                 nb_shared_units,
                 nb_matrix_layers,
                 nb_matrix_units,
                 nb_bias_layers,
                 nb_bias_units,
                 nb_delta_layers,
                 nb_delta_units,
                 nb_vtln_layers,
                 nb_vtln_units,
                 activfn,
                 output_activfn,
                 input_dim,
                 output_dim,
                 train_dataset_path,
                 latent_dim=None,
                 nb_templates=16,
                 delta_num=1,
                 lim_scale=0.3,
                 only_alpha=False,
                 no_alpha=False,
                 no_softmax=False,
                 no_delta=False,
                 ):
        super(NNTVLTAlphawDBiasMLPG1, self).__init__()

        if nb_shared_layers > 0:
            shared_fc = [nn.Linear(nb_shared_units, nb_shared_units) if i > 0 else nn.Linear(
                input_dim*2, nb_shared_units) for i in range(nb_shared_layers - 1)]
            if latent_dim is None:
                latent_dim = nb_shared_units
            shared_fc.append(nn.Linear(nb_shared_units, latent_dim))
            self.shared_fc = nn.ModuleList(shared_fc)

            if nb_matrix_layers == 1:
                matrix_fc = [nn.Linear(latent_dim, output_dim**2)]
            else:
                matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                    latent_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]
                matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            if nb_bias_layers <= 1:
                raise(NotImplementedError())
                bias_fc = [nn.Linear(latent_dim, nb_templates)]
            elif nb_bias_layers > 2:
                bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                    latent_dim, nb_bias_units) for i in range(nb_bias_layers - 2)]
                bias_fc.append(nn.Linear(nb_bias_units, nb_templates))
                bias_fc.append(nn.Linear(nb_templates, output_dim))
                bias_fc.append(nn.Linear(nb_templates, output_dim))
            else:
                bias_fc = [nn.Linear(latent_dim, nb_templates)]
                bias_fc.append(nn.Linear(nb_templates, output_dim))
                bias_fc.append(nn.Linear(nb_templates, output_dim))

            vtln_fc = get_VTLNLayer(input_dim*2, nb_vtln_layers, nb_vtln_units)

            if nb_delta_layers == 1:
                raise(NotImplementedError())
                delta_fc = [nn.Linear(latent_dim, output_dim)]
            else:
                delta_fc = [nn.Linear(nb_delta_units, nb_delta_units) if i > 0 else nn.Linear(
                    latent_dim, nb_delta_units) for i in range(nb_delta_layers - 1)]
                delta_fc.append(nn.Linear(nb_delta_units, output_dim))

            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)
            self.delta_fc = nn.ModuleList(delta_fc)
            self.vtln_fc = nn.ModuleList(vtln_fc)


        else:
            if nb_matrix_layers == 1:
                matrix_fc = [nn.Linear(input_dim*2, output_dim**2)]
            else:
                matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                    input_dim*2, nb_matrix_units) for i in range(nb_matrix_layers - 1)]
                matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            if nb_bias_layers <= 1:
                raise(NotImplementedError())
                bias_fc = [nn.Linear(input_dim*2, nb_templates)]
            elif nb_bias_layers > 2:
                bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                    input_dim*2, nb_bias_units) for i in range(nb_bias_layers - 2)]
                bias_fc.append(nn.Linear(nb_bias_units, nb_templates))
                bias_fc.append(nn.Linear(nb_templates, output_dim))
                bias_fc.append(nn.Linear(nb_templates, output_dim))
            else:
                bias_fc = [nn.Linear(input_dim*2, nb_templates)]
                bias_fc.append(nn.Linear(nb_templates, output_dim))
                bias_fc.append(nn.Linear(nb_templates, output_dim))

            vtln_fc = get_VTLNLayer(input_dim*2, nb_vtln_layers, nb_vtln_units)

            if nb_delta_layers == 1:
                raise(NotImplementedError())
                delta_fc = [nn.Linear(input_dim*2, output_dim)]
            else:
                delta_fc = [nn.Linear(nb_delta_units, nb_delta_units) if i > 0 else nn.Linear(
                    input_dim*2, nb_delta_units) for i in range(nb_delta_layers - 1)]
                delta_fc.append(nn.Linear(nb_delta_units, output_dim))

            self.shared_fc = None
            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)
            self.vtln_fc = nn.ModuleList(vtln_fc)
            self.delta_fc = nn.ModuleList(delta_fc)

        self.activfn = MyActivations(activfn)
        self.output_activfn = MyActivations(output_activfn)

        self.input_dim = input_dim
        self.output_dim = output_dim
        self.latent_dim = latent_dim
        self.only_alpha = only_alpha
        self.no_alpha = no_alpha
        self.no_softmax = no_softmax
        self.no_delta = no_delta

        self.softmax = nn.Softmax(1)

        self.lim_scale = lim_scale

        self._prepare_A(self.output_dim)

        self.train_dataset_path = train_dataset_path
        self.var_Y = cmpt_var_Y(train_dataset_path)

        self.delta_num = delta_num


    def _prepare_A(self, dim):
        import math
        import numpy as np
        coeff_matrix = torch.Tensor(
            [[1. / math.factorial(j) for j in range(dim)] for i in range(dim)]).unsqueeze(2)

        A_base = np.zeros(shape=[dim, dim, dim+1])
        alpha_power1_matrix = np.zeros(shape=[dim, dim, dim+1])
        alpha_power2_matrix = np.zeros(shape=[dim, dim, dim+1])

        for i in range(dim):
            for j in range(dim):
                m0 = max([0, j-i])
                aij = 0.
                for m in range(m0, j+1):
                    first_term = math.factorial(
                        j+1) // (math.factorial(j+1 - m) * math.factorial(m)) if j+1 >= m else 0
                    second_term = math.factorial(m+i) / math.factorial(m+i-j)
                    each_elem = first_term * second_term
                    A_base[i][j][m] = each_elem
                    alpha_power1_matrix[i][j][m] = m + i - j
                    alpha_power2_matrix[i][j][m] = m
        A_base = torch.from_numpy(A_base).unsqueeze(3)
        alpha_power1_matrix = torch.from_numpy(
            alpha_power1_matrix).unsqueeze(3)
        alpha_power2_matrix = torch.from_numpy(
            alpha_power2_matrix).unsqueeze(3)

        self.params_for_calcA = nn.ParameterDict({'coeff': nn.Parameter(coeff_matrix, requires_grad=False),
                                                  'A_base': nn.Parameter(A_base, requires_grad=False),
                                                  'alpha_power1': nn.Parameter(alpha_power1_matrix, requires_grad=False),
                                                  'alpha_power2': nn.Parameter(alpha_power2_matrix, requires_grad=False)})

    def get_batchA(self, batch_alpha):
        dim = self.output_dim
        alpha_power1_matrix = self.params_for_calcA['alpha_power1']
        alpha_power2_matrix = self.params_for_calcA['alpha_power2']
        A_base = self.params_for_calcA['A_base']
        coeff_matrix = self.params_for_calcA['coeff']

        batch_alpha_like_A = batch_alpha.squeeze(1).repeat(dim, dim, dim+1, 1)

        third_term = (-1.0 * batch_alpha_like_A) ** alpha_power1_matrix * \
            batch_alpha_like_A ** alpha_power2_matrix

        element_wise_product = A_base * third_term

        no_scale_A = element_wise_product.sum(dim=2)

        A = coeff_matrix * no_scale_A

        A = A.transpose(1, 2).transpose(0, 1)

        return A

    def forward(self, x):
        W = torch.from_numpy(get_window_matrix(
            delta_num=self.delta_num, sequence_length=x.size(0)))
        X = static2staticdelta(x, delta_num=self.delta_num, W=W)
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](X))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            z = h
        else:
            z = X

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](z))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        if self.no_alpha:
            alpha = None
        else:
            # VTLN-subnetwork
            if len(self.vtln_fc) == 1:
                alpha = torch.tanh(self.vtln_fc[0](z))
            else:
                h = self.activfn(self.vtln_fc[0](z))
                for i, fc in enumerate(self.vtln_fc[1:-1]):
                    h = self.activfn(fc(h))
                alpha = torch.tanh(self.vtln_fc[-1](h))
            alpha = self.lim_scale * alpha

        # bias-subnetwork
        if len(self.bias_fc) <= 1 + 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2 + 1:
            if self.no_softmax:
                h = self.activfn(self.bias_fc[0](z))
            else:
                h = self.softmax(self.bias_fc[0](z))
            bias_src = self.output_activfn(self.bias_fc[-2](h))
            bias_tgt = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-3]):
                h = self.activfn(fc(h))
            if self.no_softmax:
                h = self.activfn(self.bias_fc[-3](h))
            else:
                h = self.softmax(self.bias_fc[-3](h))
            bias_src = self.output_activfn(self.bias_fc[-2](h))
            bias_tgt = self.output_activfn(self.bias_fc[-1](h))


        # delta sub-network
        if len(self.delta_fc) == 1:
            delta = self.output_activfn(self.delta_fc[0](z))
        else:
            h = self.activfn(self.delta_fc[0](z))
            for i, fc in enumerate(self.delta_fc[1:-1]):
                h = self.activfn(fc(h))
            delta = self.output_activfn(self.delta_fc[-1](h))

        if self.no_alpha:
            A_alpha = None
        else:
            A_alpha = self.get_batchA(alpha)

        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))

        x = x.unsqueeze(2)
        delta = delta.unsqueeze(2)
        bias_src = bias_src.unsqueeze(2)
        bias_tgt = bias_tgt.unsqueeze(2)


        if self.only_alpha:
            raise(NotImplementedError())
            A = A_alpha
            predict = torch.matmul(A_alpha, (x - bias_src)) + bias_tgt
        else:
            if self.no_alpha:
                A = matrix
            else:
                A = A_alpha+matrix
            statics = torch.matmul(A, (x - bias_src)) + bias_tgt
            if self.no_delta:
                return statics.squeeze()
            dynamics = delta
            predict = torch.cat([statics, dynamics], dim=1)

        predict = torch.cat([statics, dynamics], dim=1)

        predict = predict.squeeze()

        y = staticdelta2static(predict, self.var_Y,
                               delta_num=self.delta_num, W=W)

        return y

    def optimize(self, X, Y, optimizer, loss_fn):
        optimizer.zero_grad()
        predict_Y = self(X)
        loss = self.loss_fn(predict_Y, Y, loss_fn)
        loss.backward()
        optimizer.step()
        return loss.item()

    def loss_fn(self, predict_Y, Y, loss_fn):
        return loss_fn(predict_Y, Y)

    def cp(self, x):
        W = torch.from_numpy(get_window_matrix(
            delta_num=self.delta_num, sequence_length=x.size(0)))
        X = static2staticdelta(x, delta_num=self.delta_num, W=W)
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](X))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            z = h
        else:
            z = X

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](z))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        if self.no_alpha:
            alpha = None
        else:
            # VTLN-subnetwork
            if len(self.vtln_fc) == 1:
                alpha = torch.tanh(self.vtln_fc[0](z))
            else:
                h = self.activfn(self.vtln_fc[0](z))
                for i, fc in enumerate(self.vtln_fc[1:-1]):
                    h = self.activfn(fc(h))
                alpha = torch.tanh(self.vtln_fc[-1](h))
            alpha = self.lim_scale * alpha

        # bias-subnetwork
        if len(self.bias_fc) <= 1 + 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2 + 1:
            if self.no_softmax:
                h = self.activfn(self.bias_fc[0](z))
            else:
                h = self.softmax(self.bias_fc[0](z))
            bias_src = self.output_activfn(self.bias_fc[-2](h))
            bias_tgt = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-3]):
                h = self.activfn(fc(h))
            if self.no_softmax:
                h = self.activfn(self.bias_fc[-3](h))
            else:
                h = self.softmax(self.bias_fc[-3](h))
            bias_src = self.output_activfn(self.bias_fc[-2](h))
            bias_tgt = self.output_activfn(self.bias_fc[-1](h))

        # delta sub-network
        if len(self.delta_fc) == 1:
            delta = self.output_activfn(self.delta_fc[0](z))
        else:
            h = self.activfn(self.delta_fc[0](z))
            for i, fc in enumerate(self.delta_fc[1:-1]):
                h = self.activfn(fc(h))
            delta = self.output_activfn(self.delta_fc[-1](h))

        if self.no_alpha:
            A_alpha = None
        else:
            A_alpha = self.get_batchA(alpha)

        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))

        x = x.unsqueeze(2)
        delta = delta.unsqueeze(2)
        bias_src = bias_src.unsqueeze(2)
        bias_tgt = bias_tgt.unsqueeze(2)

        if self.only_alpha:
            A = A_alpha
            predict = torch.matmul(A_alpha, (x - bias_src)) + bias_tgt
        else:
            if self.no_alpha:
                A = matrix
            else:
                A = A_alpha+matrix
            statics = torch.matmul(A, (x - bias_src)) + bias_tgt
            dynamics = delta
            predict = torch.cat([statics, dynamics], dim=1)

        # statics = torch.matmul(matrix, (x - bias_src)) + bias_tgt
        # dynamics = delta
        predict = torch.cat([statics, dynamics], dim=1)

        predict = predict.squeeze()

        return {"A": A, "b_src": bias_src, "b_tgt": bias_tgt, "alpha": alpha, "A_alpha": A_alpha, "delta": dynamics, "Y": predict}


class NNTVLTAlphawTMPTMLPG4(KNCNNModel):

    def __init__(self,
                 nb_shared_layers,
                 nb_shared_units,
                 nb_matrix_layers,
                 nb_matrix_units,
                 nb_bias_layers,
                 nb_bias_units,
                 nb_vtln_layers,
                 nb_vtln_units,
                 nb_delta_layers,
                 nb_delta_units,
                 activfn,
                 output_activfn,
                 input_dim,
                 output_dim,
                 train_dataset_path,
                 latent_dim=None,
                 nb_templates=16,
                 lim_scale=0.3,
                 only_alpha=False,
                 delta_num=1
                 ):
        super(NNTVLTAlphawTMPTMLPG4, self).__init__()

        if nb_shared_layers > 0:
            shared_fc = [nn.Linear(nb_shared_units, nb_shared_units) if i > 0 else nn.Linear(
                input_dim*2, nb_shared_units) for i in range(nb_shared_layers - 1)]
            if latent_dim is None:
                latent_dim = nb_shared_units
            shared_fc.append(nn.Linear(nb_shared_units, latent_dim))
            self.shared_fc = nn.ModuleList(shared_fc)

            if nb_matrix_layers == 1:
                matrix_fc = [nn.Linear(latent_dim, output_dim**2)]
            else:
                matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                    latent_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]
                matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            if nb_bias_layers <= 1:
                raise(NotImplementedError())
                bias_fc = [nn.Linear(latent_dim, nb_templates)]
            elif nb_bias_layers > 2:
                bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                    latent_dim, nb_bias_units) for i in range(nb_bias_layers - 2)]
                bias_fc.append(nn.Linear(nb_bias_units, nb_templates))
                bias_fc.append(nn.Linear(nb_templates, output_dim))
            else:
                bias_fc = [nn.Linear(latent_dim, nb_templates)]
                bias_fc.append(nn.Linear(nb_templates, output_dim))

            vtln_fc = get_VTLNLayer(latent_dim, nb_vtln_layers, nb_vtln_units)

            if nb_delta_layers == 1:
                raise(NotImplementedError())
                delta_fc = [nn.Linear(input_dim*2, output_dim)]
            else:
                delta_fc = [nn.Linear(nb_delta_units, nb_delta_units) if i > 0 else nn.Linear(
                    input_dim*2, nb_delta_units) for i in range(nb_delta_layers - 1)]
                delta_fc.append(nn.Linear(nb_delta_units, output_dim))

            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)
            self.vtln_fc = nn.ModuleList(vtln_fc)
            self.delta_fc = nn.ModuleList(delta_fc)

        else:
            raise(NotImplementedError())
            self.shared_fc = None

            matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                input_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]

            matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                input_dim, nb_bias_units) for i in range(nb_bias_layers - 1)]
            bias_fc.append(nn.Linear(nb_bias_units, output_dim))

            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)

        self.activfn = MyActivations(activfn)
        self.output_activfn = MyActivations(output_activfn)

        self.input_dim = input_dim
        self.output_dim = output_dim
        self.latent_dim = latent_dim
        self.only_alpha = only_alpha

        self.softmax = nn.Softmax(1)

        self.lim_scale = lim_scale

        self._prepare_A(self.output_dim)

        self.train_dataset_path = train_dataset_path
        self.var_Y = cmpt_var_Y(train_dataset_path)

        self.delta_num = delta_num

    def _prepare_A(self, dim):
        import math
        import numpy as np
        coeff_matrix = torch.Tensor(
            [[1. / math.factorial(j) for j in range(dim)] for i in range(dim)]).unsqueeze(2)

        A_base = np.zeros(shape=[dim, dim, dim+1])
        alpha_power1_matrix = np.zeros(shape=[dim, dim, dim+1])
        alpha_power2_matrix = np.zeros(shape=[dim, dim, dim+1])

        for i in range(dim):
            for j in range(dim):
                m0 = max([0, j-i])
                aij = 0.
                for m in range(m0, j+1):
                    first_term = math.factorial(
                        j+1) // (math.factorial(j+1 - m) * math.factorial(m)) if j+1 >= m else 0
                    second_term = math.factorial(m+i) / math.factorial(m+i-j)
                    each_elem = first_term * second_term
                    A_base[i][j][m] = each_elem
                    alpha_power1_matrix[i][j][m] = m + i - j
                    alpha_power2_matrix[i][j][m] = m
        A_base = torch.from_numpy(A_base).unsqueeze(3)
        alpha_power1_matrix = torch.from_numpy(
            alpha_power1_matrix).unsqueeze(3)
        alpha_power2_matrix = torch.from_numpy(
            alpha_power2_matrix).unsqueeze(3)

        self.params_for_calcA = nn.ParameterDict({'coeff': nn.Parameter(coeff_matrix, requires_grad=False),
                                                  'A_base': nn.Parameter(A_base, requires_grad=False),
                                                  'alpha_power1': nn.Parameter(alpha_power1_matrix, requires_grad=False),
                                                  'alpha_power2': nn.Parameter(alpha_power2_matrix, requires_grad=False)})

    def get_batchA(self, batch_alpha):
        dim = self.output_dim
        alpha_power1_matrix = self.params_for_calcA['alpha_power1']
        alpha_power2_matrix = self.params_for_calcA['alpha_power2']
        A_base = self.params_for_calcA['A_base']
        coeff_matrix = self.params_for_calcA['coeff']

        batch_alpha_like_A = batch_alpha.squeeze(1).repeat(dim, dim, dim+1, 1)

        third_term = (-1.0 * batch_alpha_like_A) ** alpha_power1_matrix * \
            batch_alpha_like_A ** alpha_power2_matrix

        element_wise_product = A_base * third_term

        no_scale_A = element_wise_product.sum(dim=2)

        A = coeff_matrix * no_scale_A

        A = A.transpose(1, 2).transpose(0, 1)

        return A

    def forward(self, x):
        W = torch.from_numpy(get_window_matrix(
            delta_num=self.delta_num, sequence_length=x.size(0)))
        X = static2staticdelta(x, delta_num=self.delta_num, W=W)
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](X))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = X

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](z))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # VTLN-subnetwork
        if len(self.vtln_fc) == 1:
            alpha = torch.tanh(self.vtln_fc[0](z))
        else:
            h = self.activfn(self.vtln_fc[0](z))
            for i, fc in enumerate(self.vtln_fc[1:-1]):
                h = self.activfn(fc(h))
            alpha = torch.tanh(self.vtln_fc[-1](h))
        alpha = self.lim_scale * alpha

        # bias-subnetwork
        if len(self.bias_fc) == 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2:
            h = self.softmax(self.bias_fc[0](z))
            bias = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-2]):
                h = self.activfn(fc(h))
            h = self.softmax(self.bias_fc[-2](h))
            bias = self.output_activfn(self.bias_fc[-1](h))

        # delta sub-network
        if len(self.delta_fc) == 1:
            delta = self.output_activfn(self.delta_fc[0](X))
        else:
            h = self.activfn(self.delta_fc[0](X))
            for i, fc in enumerate(self.delta_fc[1:-1]):
                h = self.activfn(fc(h))
            delta = self.output_activfn(self.delta_fc[-1](h))

        A_alpha = self.get_batchA(alpha)

        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))

        x = x.unsqueeze(2)
        delta = delta.unsqueeze(2)
        bias = bias.unsqueeze(2)

        # predict = torch.matmul(matrix, x) + bias
        if self.only_alpha:
            raise(NotImplementedError())
            predict = torch.matmul(A_alpha, x) + bias
        else:
            statics = torch.matmul(A_alpha + matrix, x) + bias
            dynamics = delta
            predict = torch.cat([statics, dynamics], dim=1)

        predict = predict.squeeze()

        y = staticdelta2static(predict, self.var_Y,
                               delta_num=self.delta_num, W=W)

        return y

    def optimize(self, X, Y, optimizer, loss_fn):
        optimizer.zero_grad()
        predict_Y = self(X)
        loss = self.loss_fn(predict_Y, Y, loss_fn)
        loss.backward()
        optimizer.step()
        return loss.item()

    def loss_fn(self, predict_Y, Y, loss_fn):
        return loss_fn(predict_Y, Y)

    def cp(self, x):
        W = torch.from_numpy(get_window_matrix(
            delta_num=self.delta_num, sequence_length=x.size(0)))
        X = static2staticdelta(x, delta_num=self.delta_num, W=W)
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](X))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            # h = self.shared_layers_dropouts[len(self.shared_fc)-1](h)
            # z = self.activfn(self.shared_fc[-1](h))
            z = h
        else:
            z = X

        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](z))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        # VTLN-subnetwork
        if len(self.vtln_fc) == 1:
            alpha = torch.tanh(self.vtln_fc[0](z))
        else:
            h = self.activfn(self.vtln_fc[0](z))
            for i, fc in enumerate(self.vtln_fc[1:-1]):
                h = self.activfn(fc(h))
            alpha = torch.tanh(self.vtln_fc[-1](h))
        alpha = self.lim_scale * alpha

        # bias-subnetwork
        if len(self.bias_fc) == 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2:
            h = self.softmax(self.bias_fc[0](z))
            bias = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-2]):
                h = self.activfn(fc(h))
            h = self.softmax(self.bias_fc[-2](h))
            bias = self.output_activfn(self.bias_fc[-1](h))

        # delta sub-network
        if len(self.delta_fc) == 1:
            delta = self.output_activfn(self.delta_fc[0](X))
        else:
            h = self.activfn(self.delta_fc[0](X))
            for i, fc in enumerate(self.delta_fc[1:-1]):
                h = self.activfn(fc(h))
            delta = self.output_activfn(self.delta_fc[-1](h))

        A_alpha = self.get_batchA(alpha)

        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))

        x = x.unsqueeze(2)
        delta = delta.unsqueeze(2)
        bias = bias.unsqueeze(2)

        # predict = torch.matmul(matrix, x) + bias
        if self.only_alpha:
            A = A_alpha
            predict = torch.matmul(A_alpha, x) + bias
        else:
            A = A_alpha+matrix
            statics = torch.matmul(A_alpha + matrix, x) + bias
            dynamics = delta
            predict = torch.cat([statics, dynamics], dim=1)

        predict = predict.squeeze()

        return {"A": A, "A_alpha": A_alpha, "alpha": alpha, "A_res": matrix, "b": bias, "delta": dynamics, "Y": predict}


class NNTVLTAlphawDBiasMLPG1forBD(KNCNNModel):

    def __init__(self,
                 nb_shared_layers,
                 nb_shared_units,
                 nb_matrix_layers,
                 nb_matrix_units,
                 nb_bias_layers,
                 nb_bias_units,
                 nb_delta_layers,
                 nb_delta_units,
                 nb_vtln_layers,
                 nb_vtln_units,
                 activfn,
                 output_activfn,
                 input_dim,
                 output_dim,
                 train_dataset_path,
                 latent_dim=None,
                 nb_templates=16,
                 delta_num=1,
                 lim_scale=0.3,
                 only_alpha=False,
                 no_alpha=False,
                 no_softmax=False,
                 no_delta=False,
                 ):
        super(NNTVLTAlphawDBiasMLPG1forBD, self).__init__()

        if nb_shared_layers > 0:
            shared_fc = [nn.Linear(nb_shared_units, nb_shared_units) if i > 0 else nn.Linear(
                input_dim*2, nb_shared_units) for i in range(nb_shared_layers - 1)]
            if latent_dim is None:
                latent_dim = nb_shared_units
            shared_fc.append(nn.Linear(nb_shared_units, latent_dim))
            self.shared_fc = nn.ModuleList(shared_fc)

            if nb_matrix_layers == 1:
                matrix_fc = [nn.Linear(latent_dim, output_dim**2)]
            else:
                matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                    latent_dim, nb_matrix_units) for i in range(nb_matrix_layers - 1)]
                matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            if nb_bias_layers <= 1:
                raise(NotImplementedError())
                bias_fc = [nn.Linear(latent_dim, nb_templates)]
            elif nb_bias_layers > 2:
                bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                    latent_dim, nb_bias_units) for i in range(nb_bias_layers - 2)]
                bias_fc.append(nn.Linear(nb_bias_units, nb_templates))
                bias_fc.append(nn.Linear(nb_templates, output_dim))
                bias_fc.append(nn.Linear(nb_templates, output_dim))
            else:
                bias_fc = [nn.Linear(latent_dim, nb_templates)]
                bias_fc.append(nn.Linear(nb_templates, output_dim))
                bias_fc.append(nn.Linear(nb_templates, output_dim))

            vtln_fc = get_VTLNLayer(input_dim*2, nb_vtln_layers, nb_vtln_units)

            if nb_delta_layers == 1:
                raise(NotImplementedError())
                delta_fc = [nn.Linear(latent_dim, output_dim)]
            else:
                delta_fc = [nn.Linear(nb_delta_units, nb_delta_units) if i > 0 else nn.Linear(
                    latent_dim, nb_delta_units) for i in range(nb_delta_layers - 1)]
                delta_fc.append(nn.Linear(nb_delta_units, output_dim))

            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)
            self.delta_fc = nn.ModuleList(delta_fc)
            self.vtln_fc = nn.ModuleList(vtln_fc)

        else:
            if nb_matrix_layers == 1:
                matrix_fc = [nn.Linear(input_dim*2, output_dim**2)]
            else:
                matrix_fc = [nn.Linear(nb_matrix_units, nb_matrix_units) if i > 0 else nn.Linear(
                    input_dim*2, nb_matrix_units) for i in range(nb_matrix_layers - 1)]
                matrix_fc.append(nn.Linear(nb_matrix_units, output_dim**2))

            if nb_bias_layers <= 1:
                raise(NotImplementedError())
                bias_fc = [nn.Linear(input_dim*2, nb_templates)]
            elif nb_bias_layers > 2:
                bias_fc = [nn.Linear(nb_bias_units, nb_bias_units) if i > 0 else nn.Linear(
                    input_dim*2, nb_bias_units) for i in range(nb_bias_layers - 2)]
                bias_fc.append(nn.Linear(nb_bias_units, nb_templates))
                bias_fc.append(nn.Linear(nb_templates, output_dim))
                bias_fc.append(nn.Linear(nb_templates, output_dim))
            else:
                bias_fc = [nn.Linear(input_dim*2, nb_templates)]
                bias_fc.append(nn.Linear(nb_templates, output_dim))
                bias_fc.append(nn.Linear(nb_templates, output_dim))

            vtln_fc = get_VTLNLayer(input_dim*2, nb_vtln_layers, nb_vtln_units)

            if nb_delta_layers == 1:
                raise(NotImplementedError())
                delta_fc = [nn.Linear(input_dim*2, output_dim)]
            else:
                delta_fc = [nn.Linear(nb_delta_units, nb_delta_units) if i > 0 else nn.Linear(
                    input_dim*2, nb_delta_units) for i in range(nb_delta_layers - 1)]
                delta_fc.append(nn.Linear(nb_delta_units, output_dim))

            self.shared_fc = None
            self.matrix_fc = nn.ModuleList(matrix_fc)
            self.bias_fc = nn.ModuleList(bias_fc)
            self.vtln_fc = nn.ModuleList(vtln_fc)
            self.delta_fc = nn.ModuleList(delta_fc)

        self.activfn = MyActivations(activfn)
        self.output_activfn = MyActivations(output_activfn)

        self.input_dim = input_dim
        self.output_dim = output_dim
        self.latent_dim = latent_dim
        self.only_alpha = only_alpha
        self.no_alpha = no_alpha
        self.no_softmax = no_softmax
        self.no_delta = no_delta

        self.softmax = nn.Softmax(1)

        self.lim_scale = lim_scale

        if self.no_alpha:
            pass
        else:
            self._prepare_A(self.output_dim)

        self.train_dataset_path = train_dataset_path
        self.var_Y = cmpt_var_Y(train_dataset_path)

        self.delta_num = delta_num
        self.device = None

    def _prepare_A(self, dim):
        import math
        import numpy as np
        coeff_matrix = torch.Tensor(
            [[1. / math.factorial(j) for j in range(dim)] for i in range(dim)]).unsqueeze(2)

        A_base = np.zeros(shape=[dim, dim, dim+1])
        alpha_power1_matrix = np.zeros(shape=[dim, dim, dim+1])
        alpha_power2_matrix = np.zeros(shape=[dim, dim, dim+1])

        for i in range(dim):
            for j in range(dim):
                m0 = max([0, j-i])
                aij = 0.
                for m in range(m0, j+1):
                    first_term = math.factorial(
                        j+1) // (math.factorial(j+1 - m) * math.factorial(m)) if j+1 >= m else 0
                    second_term = math.factorial(m+i) / math.factorial(m+i-j)
                    each_elem = first_term * second_term
                    A_base[i][j][m] = each_elem
                    alpha_power1_matrix[i][j][m] = m + i - j
                    alpha_power2_matrix[i][j][m] = m
        A_base = torch.from_numpy(A_base).unsqueeze(3)
        alpha_power1_matrix = torch.from_numpy(
            alpha_power1_matrix).unsqueeze(3)
        alpha_power2_matrix = torch.from_numpy(
            alpha_power2_matrix).unsqueeze(3)

        self.params_for_calcA = nn.ParameterDict({'coeff': nn.Parameter(coeff_matrix, requires_grad=False),
                                                  'A_base': nn.Parameter(A_base, requires_grad=False),
                                                  'alpha_power1': nn.Parameter(alpha_power1_matrix, requires_grad=False),
                                                  'alpha_power2': nn.Parameter(alpha_power2_matrix, requires_grad=False)})

    def get_batchA(self, batch_alpha):
        dim = self.output_dim
        alpha_power1_matrix = self.params_for_calcA['alpha_power1']
        alpha_power2_matrix = self.params_for_calcA['alpha_power2']
        A_base = self.params_for_calcA['A_base']
        coeff_matrix = self.params_for_calcA['coeff']

        batch_alpha_like_A = batch_alpha.squeeze(1).repeat(dim, dim, dim+1, 1)

        third_term = (-1.0 * batch_alpha_like_A) ** alpha_power1_matrix * \
            batch_alpha_like_A ** alpha_power2_matrix

        element_wise_product = A_base * third_term

        no_scale_A = element_wise_product.sum(dim=2)

        A = coeff_matrix * no_scale_A

        A = A.transpose(1, 2).transpose(0, 1)

        return A

    def forward(self, x):
        if self.device is not None:
            x = x.to(self.device)
        W = torch.from_numpy(get_window_matrix(
            delta_num=self.delta_num, sequence_length=x.size(0)))
        X = static2staticdelta(x, delta_num=self.delta_num, W=W)
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](X))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            z = h
        else:
            z = X
        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](z))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        if self.no_alpha:
            alpha = None
        else:
            # VTLN-subnetwork
            if len(self.vtln_fc) == 1:
                alpha = torch.tanh(self.vtln_fc[0](z))
            else:
                h = self.activfn(self.vtln_fc[0](z))
                for i, fc in enumerate(self.vtln_fc[1:-1]):
                    h = self.activfn(fc(h))
                alpha = torch.tanh(self.vtln_fc[-1](h))
            alpha = self.lim_scale * alpha

        # bias-subnetwork
        if len(self.bias_fc) <= 1 + 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2 + 1:
            if self.no_softmax:
                h = self.activfn(self.bias_fc[0](z))
            else:
                h = self.softmax(self.bias_fc[0](z))
            bias_src = self.output_activfn(self.bias_fc[-2](h))
            bias_tgt = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-3]):
                h = self.activfn(fc(h))
            if self.no_softmax:
                h = self.activfn(self.bias_fc[-3](h))
            else:
                h = self.softmax(self.bias_fc[-3](h))
            bias_src = self.output_activfn(self.bias_fc[-2](h))
            bias_tgt = self.output_activfn(self.bias_fc[-1](h))

        # delta sub-network
        if len(self.delta_fc) == 1:
            delta = self.output_activfn(self.delta_fc[0](z))
        else:
            h = self.activfn(self.delta_fc[0](z))
            for i, fc in enumerate(self.delta_fc[1:-1]):
                h = self.activfn(fc(h))
            delta = self.output_activfn(self.delta_fc[-1](h))

        if self.no_alpha:
            A_alpha = None
        else:
            A_alpha = self.get_batchA(alpha)

        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))

        x = x.unsqueeze(2)
        delta = delta.unsqueeze(2)
        bias_src = bias_src.unsqueeze(2)
        bias_tgt = bias_tgt.unsqueeze(2)

        if self.only_alpha:
            raise(NotImplementedError())
            A = A_alpha
            predict = torch.matmul(A_alpha, (x - bias_src)) + bias_tgt
        else:
            if self.no_alpha:
                A = matrix
            else:
                A = A_alpha+matrix
            statics = torch.matmul(A, (x - bias_src)) + bias_tgt
            if self.no_delta:
                return statics.squeeze()
            dynamics = delta
            predict = torch.cat([statics, dynamics], dim=1)

        predict = torch.cat([statics, dynamics], dim=1)

        predict = predict.squeeze()

        y = staticdelta2static(predict, self.var_Y,
                               delta_num=self.delta_num, W=W)

        # return y
        # return {"y": y, "A": A, "b_src": bias_src, "b_tgt": bias_tgt, "alpha": alpha}
        return {"y": y, "A": A, "b_src": bias_src, "b_tgt": bias_tgt}

    def optimize(self, X, Y, optimizer, loss_fn):
        optimizer.zero_grad()
        predict_Y = self(X)
        loss = self.loss_fn(predict_Y, Y, loss_fn)
        loss.backward()
        optimizer.step()
        return loss.item()

    def loss_fn(self, predict_Y, Y, loss_fn):
        return loss_fn(predict_Y, Y)

    def cp(self, x):
        if self.device is not None:
            x = x.to(self.device)
        W = torch.from_numpy(get_window_matrix(
            delta_num=self.delta_num, sequence_length=x.size(0)))
        X = static2staticdelta(x, delta_num=self.delta_num, W=W)
        if self.shared_fc is not None:
            h = self.activfn(self.shared_fc[0](X))
            for i, fc in enumerate(self.shared_fc[1:]):
                h = self.activfn(fc(h))
            z = h
        else:
            z = X
        # matrix-subnetwork
        if len(self.matrix_fc) == 1:
            matrix_flatten = self.output_activfn(self.matrix_fc[0](z))
        else:
            h = self.activfn(self.matrix_fc[0](z))
            for i, fc in enumerate(self.matrix_fc[1:-1]):
                h = self.activfn(fc(h))
            matrix_flatten = self.output_activfn(self.matrix_fc[-1](h))

        if self.no_alpha:
            alpha = None
        else:
            # VTLN-subnetwork
            if len(self.vtln_fc) == 1:
                alpha = torch.tanh(self.vtln_fc[0](z))
            else:
                h = self.activfn(self.vtln_fc[0](z))
                for i, fc in enumerate(self.vtln_fc[1:-1]):
                    h = self.activfn(fc(h))
                alpha = torch.tanh(self.vtln_fc[-1](h))
            alpha = self.lim_scale * alpha

        # bias-subnetwork
        if len(self.bias_fc) <= 1 + 1:
            raise(NotImplementedError())
            bias = self.output_activfn(self.bias_fc[0](z))
        elif len(self.bias_fc) == 2 + 1:
            if self.no_softmax:
                h = self.activfn(self.bias_fc[0](z))
            else:
                h = self.softmax(self.bias_fc[0](z))
            bias_src = self.output_activfn(self.bias_fc[-2](h))
            bias_tgt = self.output_activfn(self.bias_fc[-1](h))
        else:
            h = self.activfn(self.bias_fc[0](z))
            for i, fc in enumerate(self.bias_fc[1:-3]):
                h = self.activfn(fc(h))
            if self.no_softmax:
                h = self.activfn(self.bias_fc[-3](h))
            else:
                h = self.softmax(self.bias_fc[-3](h))
            bias_src = self.output_activfn(self.bias_fc[-2](h))
            bias_tgt = self.output_activfn(self.bias_fc[-1](h))

        # delta sub-network
        if len(self.delta_fc) == 1:
            delta = self.output_activfn(self.delta_fc[0](z))
        else:
            h = self.activfn(self.delta_fc[0](z))
            for i, fc in enumerate(self.delta_fc[1:-1]):
                h = self.activfn(fc(h))
            delta = self.output_activfn(self.delta_fc[-1](h))

        if self.no_alpha:
            A_alpha = None
        else:
            A_alpha = self.get_batchA(alpha)

        matrix = torch.reshape(
            matrix_flatten, (matrix_flatten.shape[0], self.output_dim, self.output_dim))

        x = x.unsqueeze(2)
        delta = delta.unsqueeze(2)
        bias_src = bias_src.unsqueeze(2)
        bias_tgt = bias_tgt.unsqueeze(2)

        if self.only_alpha:
            raise(NotImplementedError())
            A = A_alpha
            predict = torch.matmul(A_alpha, (x - bias_src)) + bias_tgt
        else:
            if self.no_alpha:
                A = matrix
            else:
                A = A_alpha+matrix
            statics = torch.matmul(A, (x - bias_src)) + bias_tgt
            if self.no_delta:
                return statics.squeeze()
            dynamics = delta
            predict = torch.cat([statics, dynamics], dim=1)

        predict = torch.cat([statics, dynamics], dim=1)

        predict = predict.squeeze()

        return {"A": A, "b_src": bias_src, "b_tgt": bias_tgt, "alpha": alpha, "A_alpha": A_alpha, "delta": dynamics, "Y": predict}
