# -*- coding: utf-8 -*-
"""Overview:
    Getting stats(*.mean_X. *.std_X, *.cov_X, *.mean_Y, *.std_Y, *.cov_Y, *.A, *.b), which are calcurated from data in the scp file.   

Usage:
    getstats <scp_path> <output_path> [-u <vector_size>] [-f <feature_range>] [-z] [-l] [-d] [-v...]
    getstats -h | --help

Options:
    <scp_path>                          hoge.
    <output_path>                       hoge.
    -u --vector-size <vector_size>      hoge [default: 1].
    -f --feature-range <feature_range>  hoge [default: 0,1].
    -z --zero-retain                    hoge.
    -l --log-domain                     hoge.
    -d --add-delta                      hoge.
    -v --verbose                        Show verbose message
    -h --help                           Show this screen and exit.
"""
from docopt import docopt
import os

import numpy as np
from data.data.scp import SCP


from misc.util import set_level, logger
from misc.readwrite import write_fseq, load_fseq
from data.dataset.dataset import add_delta


def main(scp_path, output_path, vector_size, feature_range, zero_retain, log_domain, add_delta_flag, verbose):
    set_level(verbose)

    scp = SCP(filepath=scp_path)
    X = None
    Y = None
    # print(feature_range)
    for idx in range(scp.row_len):
        filepaths = scp.scp[idx]
        x = load_fseq(path=filepaths[0], vec_size=vector_size,
                        feature_range=range(feature_range[0], feature_range[1]))
        # logger.debug(x.shape)
        y = load_fseq(path=filepaths[1], vec_size=vector_size,
                      feature_range=range(feature_range[0], feature_range[1]))
        # X.append(x)
        # Y.append(y)
        if add_delta_flag:
            x = add_delta(x)
            y = add_delta(y)
        if X is None:
            X = x
        else:
            X = np.concatenate([X, x], axis=0)
        if Y is None:
            Y = y
        else:
            Y = np.concatenate([Y, y], axis=0)

    # X = np.asarray(X)
    # Y = np.asarray(Y)

    logger.debug(X.shape)
    logger.debug(Y.shape)
    if zero_retain:
        X = X[X != 0]
        Y = Y[Y != 0]
    if log_domain:
        X = np.log2(X)
        Y = np.log2(Y)

    mean_X = X.mean(axis=0)
    std_X = X.std(axis=0)
    cov_X = np.cov(X, rowvar=False)
    logger.debug(mean_X.shape)
    logger.debug(std_X.shape)
    logger.debug(cov_X.shape)

    mean_Y = Y.mean(axis=0)
    std_Y = Y.std(axis=0)
    cov_Y = np.cov(Y, rowvar=False)
    logger.debug(mean_Y.shape)
    logger.debug(std_Y.shape)
    logger.debug(cov_Y.shape)

    A = std_Y / std_X
    b = mean_Y - (std_Y / std_X) * mean_X
    # print(A)
    # print(b)


    write_fseq(path=output_path + ".mean_X", fseq=mean_X)
    write_fseq(path=output_path + ".std_X", fseq=std_X)
    write_fseq(path=output_path + ".cov_X", fseq=cov_X)
    write_fseq(path=output_path+ ".mean_Y", fseq=mean_Y)
    write_fseq(path=output_path + ".std_Y", fseq=std_Y)
    write_fseq(path=output_path + ".cov_Y", fseq=cov_Y)
    write_fseq(path=output_path+ ".A", fseq=A)
    write_fseq(path=output_path+ ".b", fseq=b)

if __name__ == "__main__":
    args = docopt(__doc__)
    main(scp_path=os.path.abspath(args['<scp_path>']),
         output_path=os.path.abspath(args['<output_path>']),
        #  mean_output_path=os.path.abspath(
            #  args['--mean-output-path']) if args['--mean-output-path'] is not None else None,
        #  std_output_path=os.path.abspath(
            #  args['--std-output-path']) if args['--std-output-path'] is not None else None,
            zero_retain=args['--zero-retain'],
            log_domain=args['--log-domain'],
            add_delta_flag=args['--add-delta'],
         vector_size=int(args['--vector-size']),
         feature_range=[int(f)
                          for f in args['--feature-range'].split(',')],
         verbose=int(args['--verbose']))
