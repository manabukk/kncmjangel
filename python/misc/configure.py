# -*- coding: utf-8 -*-

import json
import os


class MyDict(dict):
    __getattr__ = dict.__getitem__

with open("{}/configure.json".format(os.path.dirname(os.path.abspath(__file__)) + "/../.."), 'r') as f:
    CONF = json.load(f)

try:
    cur_dir = os.getcwd()
    with open("{}/configure.json".format(cur_dir), 'r') as f:
       _CONF = json.load(f)
    CONF = dict(CONF, **_CONF)
except Exception as e:
    # print("Default configuration")
    pass

CONF = MyDict(CONF)
    
