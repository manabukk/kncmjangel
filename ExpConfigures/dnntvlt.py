# -*- coding=utf-8 -*-

from base import BaseExpConfig


class ExpConfig(BaseExpConfig):
    """docstring for ExpConfig."""
    ##########################
    # Experimental configure #
    ##########################
    project_name = "20200127"

    def get_exp_name(self, experiment):
        exp_name = "TVLT_{}:{}:{}_{}:{}:{}_{}".format(
            experiment.get_parameter("nb_shared_layers"),
            experiment.get_parameter("nb_matrix_layers"),
            experiment.get_parameter("nb_bias_layers"),
            experiment.get_parameter("nb_shared_units"),
            experiment.get_parameter("nb_matrix_units"),
            experiment.get_parameter("nb_bias_units"),
            experiment.get_parameter("learning_rate"))
        return exp_name

    def get_exp_tags(self, experiment):
        return [
            'DNN-TVLT'
        ]

    ###################
    # Model configure #
    ###################
    model_class_path = "model.tvlt.NNTVLT"

    model_configure = {
        "algorithm": "grid",
        "spec": {
            "metric": "best_valid_mse",
            "objective": "minimize"
        },
        "parameters": {
            "learning_rate": {
                "type": "discrete",
                # "values": [0.01, 0.005, 0.002, 0.001]
                "values": [0.001]
            },
            "nb_shared_layers": {
                "type": "integer",
                # "min": 0, "max": 5
                "min": 4, "max": 4
            },
            "nb_matrix_layers": {
                "type": "integer",
                "min": 2, "max": 2
            },
            "nb_bias_layers": {
                "type": "integer",
                "min": 1, "max": 1
            },
            "nb_shared_units": {
                "type": "discrete",
                # "values": [32, 64, 128, 256]
                "values": [128]
            },
            "nb_matrix_units": {
                "type": "discrete",
                "values": [128]
            },
            "nb_bias_units": {
                "type": "discrete",
                "values": [128]
            },
            "active_fn": {
                "type": "categorical",
                "values": ['ReLU']
            },
            "input_dim": {
                "type": "discrete",
                "values": [24]
            },
            "output_dim": {
                "type": "discrete",
                "values": [24]
            },
            "output_activfn": {
                "type": "categorical",
                "values": ['linear']
            },
        }
    }

    def get_model(self, experiment):
        model_class = self.get_model_class(self.model_class_path)
        return model_class(
            nb_shared_layers=experiment.get_parameter("nb_shared_layers"),
            nb_matrix_layers=experiment.get_parameter("nb_matrix_layers"),
            nb_bias_layers=experiment.get_parameter("nb_bias_layers"),
            nb_shared_units=experiment.get_parameter("nb_shared_units"),
            nb_matrix_units=experiment.get_parameter("nb_matrix_units"),
            nb_bias_units=experiment.get_parameter("nb_bias_units"),
            activfn=experiment.get_parameter(
                "active_fn"),
            output_activfn=experiment.get_parameter(
                "output_activfn"),
            input_dim=experiment.get_parameter(
                "input_dim"),
            output_dim=experiment.get_parameter(
                "output_dim"),
        )

    save_model_path = "./model/dnn_tvlt"

    #######################
    # Dataset description #
    #######################
    normalize_flag = False

    train_dataset_path = "/work/kotani/tvlt-vc/20200115/dataset/mht-fks-train.dataset"
    valid_dataset_path = "/work/kotani/tvlt-vc/20200115/dataset/mht-fks-valid.dataset"
    batch_size = 1

    nb_bins = 513
    sampling_rate = 16000
    frame_shift = 5.0
    sample_id = {'train': ['a01', 'a02', 'a03'],
                 'valid': ['i01', 'i02', 'i03']}

    if normalize_flag:
        norm_funcs_x, norm_funcs_y = get_norm_funcs(
            stats_path=stats_path, dim=24)
        etc['norm_funcs'] = {'x': norm_funcs_x, 'y': norm_funcs_y}

    patience = 50
