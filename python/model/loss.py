# -*- coding: utf-8 -*-

import numpy as np
from misc.util import logger
import torch


def b_inv(b_mat):
    eye = b_mat.new_ones(b_mat.size(-1)).diag().expand_as(b_mat)
    b_inv, _ = torch.solve(eye, b_mat)
    return b_inv


def get_window_matrix(delta_num, sequence_length, dim=1): # TODO:
    if delta_num == 0:
        window_matrix = np.eye(dim*sequence_length)

    elif delta_num == 1:
        sub_window_static = np.eye(dim)
        sub_window_delta1 = np.eye(dim) * 0.5
        sub_window_delta2 = - np.eye(dim) * 0.5

        _window_matrix = np.zeros(shape=[dim*sequence_length, (delta_num+1)*dim*sequence_length])
        for t in range(sequence_length):
            _window_matrix[t*dim:t*dim+dim, (delta_num+1)*t*dim:(delta_num+1)*t*dim + dim] = sub_window_static
        for t in range(1, sequence_length):
            _window_matrix[t*dim:t*dim+dim,
                           (delta_num+1)*t*dim - dim:(delta_num+1)*t*dim] = sub_window_delta1
        for t in range(2, sequence_length+1):
            _window_matrix[t*dim - (delta_num+1) * dim:t*dim+dim - (delta_num+1) * dim,
                           (delta_num+1)*t*dim - dim:(delta_num+1)*t*dim] = sub_window_delta2
        window_matrix = _window_matrix.T
    else:
        raise NotImplementedError()

    return window_matrix


# 共分散行列に対角を仮定かつ時不変を仮定 [参考：https://www.isca-speech.org/archive/interspeech_2014/i14_2283.html]
class MLPG(object):
    def __init__(self, train_dataset, device=None): # TODO: 事前に mean と cov を計算しておいた場合の実装もしような
        super(MLPG, self).__init__()
        self.train_dataset = train_dataset
        self.device = device
        self.mean_Y = None
        self.cov_Y = None

        self._calc_stats()


    def _calc_stats(self):
        logger.debug("[MLPG] Calculating stats...")
        global_Y = None
        for idx in range(len(self.train_dataset)):
            utter = self.train_dataset[idx]
            if global_Y is None:
                global_Y = utter['y']
            else:
                global_Y = np.concatenate([global_Y, utter['y']], axis=0)

        mean_Y = global_Y.mean(axis=0)
        # cov_Y = np.cov(global_Y, bias=True, rowvar=False)
        sd_Y = np.sqrt(np.var(global_Y, axis=0))
        cov_Y = np.diag(sd_Y)
        inv_cov_Y = np.diag(1.0 / sd_Y)
        
        logger.debug("Global Y shape: {}".format(global_Y.shape))
        logger.debug("Global Y mean shape: {}".format(mean_Y.shape))
        logger.debug("Global Y covariance shape: {}".format(cov_Y.shape))

        self.mean_Y = torch.from_numpy(mean_Y)
        self.cov_Y = torch.from_numpy(cov_Y)
        self.inv_cov_Y = torch.from_numpy(inv_cov_Y)
        self.sd_Y = torch.from_numpy(sd_Y)

        logger.debug("[MLPG] Finished calculation")

    def __call__(self, Y):
        device = Y.device
        dim = len(self.train_dataset.yf)  # delta なしの特徴量次元
        delta_num = self.train_dataset.delta_dim
        sequence_length = Y.size(0)
        _W = get_window_matrix(delta_num=delta_num, sequence_length=sequence_length, dim=1)
        W = torch.from_numpy(_W).to(device)

        Y = (Y - self.mean_Y.to(device)) / self.sd_Y.to(device)  # 共分散行列に対角時不変を仮定しているので、正規化してしまえば、以下 U^{-1} = I と計算省略できる

        MtM = torch.matmul(W.transpose(1,0), W)

        inv_MtM = torch.inverse(MtM)

        y = []
        for d in range(dim):
            mini_Y = Y[:, [d + dim*i for i in range(delta_num+1)]]
            mini_Y_flatten = mini_Y.flatten().unsqueeze(1)
            prod = torch.matmul(W.transpose(1,0), mini_Y_flatten)
            prod = torch.matmul(inv_MtM, prod)
            y.append(prod)

        y = torch.cat(y, dim=1)
        y = self.sd_Y[:dim].to(device) * y + self.mean_Y[:dim].to(device)

        return y
