# -*- coding=utf-8 -*-
from misc.util import dynamic_import
# from norm import get_norm_funcs
from misc.cdist import compute_melcd
import ignite
from torch import optim, nn
import os
import sys

class BaseExpConfig(object):
    """docstring for BaseExpConfig."""

    def __init__(self):
        self.nb_iters = 0

    # as is
    etc = {}

    ##########################
    # Experimental configure #
    ##########################
    project_name = "sand box"

    def get_exp_name(self, experiment):
        exp_name = "{}_{}_{}".format(
            experiment.get_parameter("nb_layers"),
            experiment.get_parameter("nb_units"),
            experiment.get_parameter("learning_rate"))
        return exp_name

    def get_exp_tags(self, experiment):
        return [
            'Base one'
        ]
    ###################
    # Model configure #
    ###################
    model_class_path = "model.simple.SimpleBN"

    def get_model_class(self, model_class_path):
        model_class = dynamic_import(model_class_path)
        return model_class

    model_configure = {
        "algorithm": "grid",
        "spec": {
            "metric": "valid_loss_after_training",
            "objective": "minimize"
        },
        "parameters": {
            "learning_rate": {
                "type": "discrete",
                "values": [0.01, 0.001]
            },
            "nb_layers": {
                "type": "integer",
                "min": 3, "max": 5
            },
            "nb_units": {
                "type": "discrete",
                "values": [256, 512, 1024]
            },
            "active_fn": {
                "type": "categorical",
                "values": ['ReLU']
            },
            "input_dim": {
                "type": "discrete",
                "values": [59]
            },
            "output_dim": {
                "type": "discrete",
                "values": [59]
            },
            "output_activfn": {
                "type": "categorical",
                "values": ['linear']
            },
        }
    }

    def get_model(self, experiment):
        model_class = self.get_model_class(self.model_class_path)
        return model_class(nb_layers=experiment.get_parameter("nb_layers"),
                           nb_units=experiment.get_parameter(
            "nb_units"),
            activfn=experiment.get_parameter(
            "active_fn"),
            output_activfn=experiment.get_parameter(
            "output_activfn"),
            input_dim=experiment.get_parameter(
            "input_dim"),
            output_dim=experiment.get_parameter(
            "output_dim"),
        )

    pretrained_model_path = None
    def get_pretrained_model_path(self, experiment):
        return self.pretrained_model_path

    ########################
    # Optimizing configure #
    ########################
    def get_prepare_batch(self):
        def prepare_batch(batch, device, non_blocking):
            # with torch.no_grad():
            _X = batch['x'][0]
            _Y = batch['y'][0]
            if self.normalize_flag:
                X = self.etc['norm_funcs']['x'].normalize(_X)
                Y = self.etc['norm_funcs']['y'].normalize(_Y)
            else:
                X = _X
                Y = _Y
            X = X.to(device)
            Y = Y.to(device)
            return X, Y
        return prepare_batch

    def get_loss_fn(self,experiment):
        loss_fn = nn.MSELoss()
        return loss_fn

    @staticmethod
    def optimizer(params, lr):
        return optim.Adam(params, lr, amsgrad=True)

    def get_scheduler_func(self):

        def scheduler_func(step):
            return 1.0

        return scheduler_func

    #######################
    # Dataset description #
    #######################
    # train_dataset_path = "/work/kotani/tvlt-vc/dataset/mht-mmy/train_200.dataset"
    # valid_dataset_path = "/work/kotani/tvlt-vc/dataset/mht-mmy/valid.dataset"
    train_dataset_path = None
    valid_dataset_path = None
    def get_dataset_path(self, experiment):
        return self.train_dataset_path, self.valid_dataset_path
    
    batch_size = 1
    def get_batch_size(self, experiment):
        return self.batch_size

    normalize_flag = False
    # stats_path = "/work/kotani/tvlt-vc/dataset/mht-mmy/train_200_stats.{}"

    # if normalize_flag:
    #     norm_funcs_x, norm_funcs_y = get_norm_funcs(
    #         stats_path=stats_path, dim=59)
    #     etc['norm_funcs'] = {'x': norm_funcs_x, 'y': norm_funcs_y}

    # nb_bins = 513
    # sampling_rate = 16000
    # frame_shift = 1.0
    # sample_id = {'train': ['a01', 'a02', 'a03'],
    #              'valid': ['i01', 'i02', 'i03']}

    #####################
    # Logging configure #
    #####################
    # Callable

    log_n_iters = 10
    def get_log_n_iters(self, experiment):
        return self.log_n_iters

    save_model_path = None
    def get_save_model_path(self, experiment):
        return self.save_model_path

    def get_metrics(self, experiment):
        def eval_transform(output):
            return output['y_pred'], output['y']

        if self.normalize_flag:
            mcd_metric = ignite.metrics.Loss(lambda pred_y, y: compute_melcd([self.etc['norm_funcs']['y'].denormalize(
                pred_y.cpu()).numpy(), self.etc['norm_funcs']['y'].denormalize(y.cpu()).numpy()]), output_transform=eval_transform)
        else:
            mcd_metric = ignite.metrics.Loss(lambda pred_y, y: compute_melcd([
                pred_y.cpu().numpy(), y.cpu().numpy()]), output_transform=eval_transform)

        metrics = {
            'mse': ignite.metrics.MeanSquaredError(output_transform=eval_transform),
            'mcd': mcd_metric
        }

        return metrics

    def get_watching_metric(self):
        watching_metric = 'mcd'
        watching_minimum_step = 1000
        return watching_metric, watching_minimum_step

    ############################
    # Early stopping configure #
    ############################
    patience = 30
    def get_patience(self, experiment):
        return self.patience

    def get_score_function(self):
        def score_function(engine):
            val_loss = engine.state.metrics['mse']
            return -val_loss
        return score_function
