# -*- coding: utf-8 -*-
"""Overview:
    Processing VAD on mcepfile.

Usage:
    vad_onmcep <mcep_file> <output_path> [-t <threshold_value>] [-d <vector_size>] [-f <power_index>] [-v...]
    vad_onmcep -h

Options:
    <mcep_file>                               hoge.
    <output_path>                             Path to output idx file.
    -t --threshold-value <threshold_value>    Threshold value [default: -8.0].
    -d --vector-size <vector_size>            Vector size [default: 25].
    -f --feature-range <power_index>          Feature range [default: 0].
    -v --verbose                              Show in detail.
    -h --help                                 Show this message.
"""

from docopt import docopt
import sys
import os

from misc.readwrite import load_fseq, write_alignment
from misc.util import logger, set_level

import numpy as np


def get_idxs_applied_VAD(power_seq, threshold=-8.0):
    return np.where(power_seq[:,0] > threshold)[0]

def main(mfp, ofp, t, d, frange, verbose):
    set_level(verbose)

    logger.debug("Input mcep file path: {}".format(mfp))
    X = load_fseq(path=mfp, vec_size=d, feature_range=frange)
    logger.debug("Input mcep (only power) shape: {}".format(X.shape))

    logger.debug("Threshold value: {}".format(t))
    idxs = get_idxs_applied_VAD(X, t)
    logger.debug("Size of idxs: {}".format(len(idxs)))

    logger.debug("Path to the new alignment: {}".format(ofp))
    write_alignment(path=ofp, alignment=idxs)
    


if __name__ == '__main__':
    args = docopt(__doc__)

    main(
         mfp=os.path.abspath(args['<mcep_file>']),
         ofp=os.path.abspath(args['<output_path>']),
         d=int(args['--vector-size']),
         t=float(args['--threshold-value']),
         frange=[int(f) for f in args['--feature-range'].split(',')],
         verbose=int(args['--verbose']))
