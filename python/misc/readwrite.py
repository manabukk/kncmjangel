# -*- coding: utf-8 -*-
""" Doc string """
import os
import struct

import numpy as np

from misc.util import mkdir_p
from misc import pdata


def load_alignment(path, b=4):
    """
    docstring here
        :param path:
        :param b=4:
    """
    with open(path, "rb") as f:
        binary = f.read()
        data_size = os.path.getsize(path)
        alignment = list(struct.unpack(str(int(data_size / b)) + "i", binary))
        return [a if i == 0 else a - 1 for i, a in enumerate(alignment)]


def load_fseq(path, vec_size, feature_range, b=4):
    """
    docstring here
        :param path:
        :param vec_size:
        :param feature_range:
        :param b=4:
    """
    assert min(feature_range) >= 0 and max(
        feature_range) < vec_size, '# ERROR\nget_mcep_data\ndimension error'
    feature_indexes = np.array(feature_range)
    with open(path, "rb") as f:
        binary = f.read()
        data_size = os.path.getsize(path)
        f_in_a_vec = np.array(
            list(struct.unpack(str(int(data_size / b)) + "f", binary)))
        fseq = np.asarray([f_in_a_vec[frame * vec_size + feature_indexes]
                           for frame in range(int(data_size / (b * vec_size)))])
        assert data_size % (
            b * vec_size) == 0, "data size (%d) must be multiples of b * vec_size (%d * %d)" % (data_size, b, vec_size)
    return fseq


def write_fseq(path, fseq):
    """
    docstring here
        :param path:
        :param fseq:
    """
    if len(np.asarray(fseq).shape) > 2:
        print(np.asarray(fseq).shape)
        raise NotImplementedError()
    elif len(np.asarray(fseq).shape) == 1:
        fseq = np.asarray([[f] for f in fseq])
    elif len(np.asarray(fseq).shape) == 0:
        fseq = np.asarray([[fseq]])
    else:
        pass
    try:
        dir_path = os.path.dirname(path)
        if dir_path not in ('', '.'):
            mkdir_p(os.path.dirname(path))
        with open(path, "wb") as f:
            for feature_at_a_frame in fseq:
                for v in feature_at_a_frame:
                    f.write(struct.pack("f", v))
    except Exception as e:
        print(e)
        raise


def write_alignment(path, alignment):
    """
    docstring here
        :param path:
        :param alignment:
    """
    try:
        mkdir_p(os.path.dirname(path))
        with open(path, "wb") as f:
            for i, index in enumerate(alignment):
                if i == 0:
                    f.write(struct.pack("i", index))
                else:
                    f.write(struct.pack("i", index + 1))
    except Exception as e:
        print(e)
        raise

def load_lseq(path, in_fshift, out_fshift, label_list=None):
    """
    docstring here
        :param path:
        :param in_fshift:
        :param out_fshift:
        :param label_list=None:
    """
    # fshift described in ms
    lseq = []
    with open(path, "r") as f:
        def sep(line):
            """
            docstring here
                :param line:
            """
            columns = line.split()
            bframe = int(columns[0]) * in_fshift // out_fshift
            eframe = int(columns[1]) * in_fshift // out_fshift
            label = columns[2]
            return int(bframe), int(eframe), label

        for line in f.readlines():
            bframe, eframe, label = sep(line)
            if label_list is None or label in label_list:
                lseq.extend([label for _ in range(eframe - bframe)])
    return lseq


def write_matrix(path, A):
    """
    docstring here
        :param path:
        :param A:
    """
    write_fseq(path, A)


def write_vector(path, b):
    """
    docstring here
        :param path:
        :param b:
    """
    try:
        dir_path = os.path.dirname(path)
        if dir_path not in ('', '.'):
            mkdir_p(os.path.dirname(path))
        with open(path, "wb") as f:
            for v in b:
                f.write(struct.pack("f", v))
    except Exception as e:
        print(e)
        raise


def load_matrix(path, dim, b=4):
    """
    docstring here
        :param path:
        :param dim:
        :param b=4:
    """
    return load_fseq(path, vec_size=dim, feature_range=range(dim), b=b)


def load_vector(path, b=4):
    """
    docstring here
        :param path:
        :param b=4:
    """
    with open(path, "rb") as f:
        binary = f.read()
        data_size = os.path.getsize(path)
        vector = np.array(
            list(struct.unpack(str(int(data_size / b)) + "f", binary)))
    return vector


def load_matrices(path, dim):
    """
    docstring here
        :param path:
        :param dim:
    """
    matrices = load_fseq(path, vec_size=dim * dim,
                         feature_range=range(dim * dim))
    return matrices.reshape(-1, dim, dim)


def write_matrices(path, matrices):
    """
    docstring here
        :param path:
        :param matrices:
    """
    matrices = matrices.reshape(matrices.shape[0], -1)
    write_fseq(path, matrices)


def get_pdata_from_files(fpath_list, apath, vec_size=25, feature_range=range(1, 25), f_b=4, a_b=4):
    """
    docstring here
        :param fpath_list:
        :param apath:
        :param vec_size=25:
        :param feature_range=range(1:
        :param 25:
    """
    fseq_list = [load_fseq(path=fpath, vec_size=vec_size,
                           feature_range=feature_range, b=f_b) for fpath in fpath_list]
    alignment = load_alignment_index(
        path=apath, b=a_b) if apath is not None else None
    parallel_data, _ = pdata.get_parallel_data(
        fseq_list=fseq_list, alignment=alignment, num_seq=len(fseq_list))
    return parallel_data


def load_matfile(path):
    from scipy import io
    matdata = io.loadmat(path)
    return matdata
